// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef AREA_H
#define AREA_H

#include "blackguard/logic/AreaModel.h"

class Area : public BG::AreaModel {
  public:
    Area(std::string tag, std::string name);
    virtual ~Area();

    virtual Entity* getCopy() const override;

    virtual std::vector<const BG::CellModel*> getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const override;
    virtual bool cellBlocksLineOfSight(const BG::CellModel* cell) const override;
    virtual bool cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const override;
    virtual int getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const override;
    virtual int estimateDistanceBetweenCells(const BG::CellModel* startCell, const BG::CellModel* goalCell) const override;
};

#endif // AREA_H

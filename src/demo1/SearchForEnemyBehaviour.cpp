// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "SearchForEnemyBehaviour.h"
#include "NPC.h"
#include "BlackguardGM.h"
#include "../external/easylogging++.h"

SearchForEnemyBehaviour::SearchForEnemyBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor)
: Behaviour(tag, name, gm, actor) {}

SearchForEnemyBehaviour::~SearchForEnemyBehaviour() {
}

BG::Entity* SearchForEnemyBehaviour::getCopy() const {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto npc = dynamic_cast<NPC*>(actor);
  auto copy = new SearchForEnemyBehaviour(tag, name, bgGM, npc);
  copy->id = id;
  return copy;
}

void SearchForEnemyBehaviour::execute() {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto attacker = dynamic_cast<BaseActor*>(actor);
  auto target = dynamic_cast<const BG::Actor*>(actor->getMemory()->recall("targetEnemy").get_value_or(nullptr));

  if (target) {
    auto targetEnemy = bgGM->getActor(target->getTag());
    if (targetEnemy && targetEnemy.get()->isAlive()) {
      if (bgGM->actorIsWithinLOS(attacker, targetEnemy.get())) {
        bgGM->logMessage(BG::TextLogEntry(attacker->getName() + " shouts as it spots " + target->getName() + " again!"));
        attacker->getMemory()->memorise("targetEnemyPosition", &targetEnemy.get()->getPosition().get());
        attacker->setBehaviour("attackEnemy");
        attacker->startTurn();
      } else {
        auto lastKnownPosition = dynamic_cast<const BG::Position*>(attacker->getMemory()->recall("targetEnemyPosition").get_value_or(nullptr));
        if (lastKnownPosition) {
          if (*attacker->getPosition() != *lastKnownPosition) {
            bgGM->logMessage(BG::TextLogEntry(name + " moves towards your its target's last known position."));
            bgGM->moveActorTowards(attacker, *lastKnownPosition);
          } else {
            bgGM->logMessage(BG::TextLogEntry(attacker->getName() + " gives up the search."));
            actor->getMemory()->forget("targetEnemy");
            actor->getMemory()->forget("targetEnemyPosition");
            actor->setBehaviour("idle");
            actor->startTurn();
          }
        } else {
          LOG(ERROR) << "SearchForEnemyBehaviour::execute(): failed to cast memory entity to Position";
        }
      }
    }
  } else {
    LOG(ERROR) << "SearchForEnemyBehaviour::execute(): failed to cast memory entity to Actor";
  }

}

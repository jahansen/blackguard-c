// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef BLACKGUARDGM_H
#define BLACKGUARDGM_H

#include <boost/optional.hpp>
#include "blackguard/core/GameMaster.h"
#include "blackguard/logic/TextLogEntry.h"
#include "blackguard/logic/Direction.h"
#include "blackguard/logic/Position.h"

namespace BG {
  class EntityManager;
  class AreaManager;
  class TextLog;
  class AreaModel;
  class PhysicalEntity;
  class CellModel;
  class Item;
}

class BaseActor;

class BlackguardGM : public BG::GameMaster {
  public:
    BlackguardGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window);

    // Game inspectors
    boost::optional<BaseActor*> getActor(std::string tag) const;
    std::vector<BG::Actor*> getEnemiesInArea(BaseActor* actor) const;
    std::vector<BG::Item*> getItemsAtPosition(BG::Position position, const BG::AreaModel* area) const;
    bool actorIsWithinLOS(BG::Actor* source, BG::Actor* target) const;
    bool actorIsWithinAttackRange(BG::Actor* attacker, BG::Actor* target) const;

    // Game mutators
    void moveActor(BaseActor* actor, const BG::AreaModel* area, BG::Direction direction);
    void moveActorToCell(BaseActor* actor, const BG::CellModel* cell);
    void moveActorTowards(BaseActor* actor, BG::Position targetDirection);
    void executeAttack(BaseActor* attacker, BaseActor* defender);
    void damageActor(BaseActor* aggressor, BaseActor* victim, int amount);
    void giveItem(BaseActor* receiver, BG::Item* item);

    // Interface mutators
    void scrollLogUp();
    void scrollLogDown();
    void logMessage(BG::TextLogEntry message);

    void openInventory(BaseActor* player);
    void closeInventory();

  private:
    static const std::string INVENTORY_DIALOG;
};

#endif // BLACKGUARDGM_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include <algorithm>
#include <sstream>
#include "BlackguardGM.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "blackguard/logic/manager/AreaManager.h"
#include "blackguard/core/FontManager.h"
#include "blackguard/logic/TextLog.h"
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/Faction.h"
#include "blackguard/logic/Item.h"
#include "blackguard/core/Window.h"
#include "BaseActor.h"
#include "InventoryDialog.h"
#include "PC.h"
#include "../external/easylogging++.h"

const std::string BlackguardGM::INVENTORY_DIALOG = "inventory-dialog";

BlackguardGM::BlackguardGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window)
: BG::GameMaster(entityManager, world, fontManager, log, window) {}

void BlackguardGM::moveActor(BaseActor* actor, const BG::AreaModel* area, BG::Direction direction) {
  auto targetCell = area->getAdjacentCell(*actor->getPosition(), direction);
  if (targetCell) {
    moveActorToCell(actor, targetCell.get());
  } else {
    LOG(ERROR) << "BlackguardGM::moveActor(): Failed to get find target cell";
  }
}

void BlackguardGM::moveActorToCell(BaseActor* actor, const BG::CellModel* cell) {
  if (cell->getProperty("passable") == "true") {
    auto actorInTargetCell = getActorInCell(cell, actor->getArea().get());
    if (actorInTargetCell) {
      actor->bumpActor(actorInTargetCell.get());
    } else {
      actor->setPosition(BG::Position(cell->getX(), cell->getY()));
      actor->moveDone();
    }
  } else {
    actor->bumpWall();
  }
}

void BlackguardGM::moveActorTowards(BaseActor* actor, BG::Position targetPosition) {
  if (*actor->getPosition() != targetPosition) {
    auto area = actor->getArea().get();
    auto currentCell = area->getCell(actor->getPosition().get());
    auto targetCell = area->getCell(targetPosition);
    if (currentCell && targetCell) {
      auto actorsInArea = entityManager->getActorsInArea(area);
      auto nextCell = pathfinder.getNextStepTowardsGoal(area, currentCell.get(), targetCell.get(), actorsInArea);
      if (nextCell) {
        moveActorToCell(actor, nextCell.get());
      }
      else {
        LOG(WARNING) << "BlackguardGM::moveActorTowards(): Failed to find path for " << actor->getName();
      }
    } else {
      LOG(WARNING) << "BlackguardGM::moveActorTowards(): Error finding current or target cells";
    }
  }
}

void BlackguardGM::executeAttack(BaseActor* attacker, BaseActor* defender) {
  std::stringstream msg;
  msg << attacker->getName() << " hits " + defender->getName() + " for " << attacker->getAttack() << " damage.";
  logMessage(BG::TextLogEntry(msg.str()));

  damageActor(attacker, defender, attacker->getAttack());

  attacker->attackDone();
}

void BlackguardGM::damageActor(BaseActor* aggressor, BaseActor* victim, int amount) {
  victim->damage(amount, aggressor);

  std::stringstream msg;
  auto hpLeft = victim->getHP();
  if (hpLeft <= 0) {
    victim->die();

    msg << victim->getName() << " dies...";
    logMessage(BG::TextLogEntry(msg.str()));

    removeEntity(victim);
  } else {
    msg << victim->getName() << " has " << victim->getHP() << " HP left.";
    logMessage(BG::TextLogEntry(msg.str()));
  }
}

void BlackguardGM::giveItem(BaseActor* receiver, BG::Item* item) {
  receiver->getInventory()->addItem(item);
}

void BlackguardGM::scrollLogUp() {
  log->scrollUp();
}

void BlackguardGM::scrollLogDown() {
  log->scrollDown();
}

void BlackguardGM::logMessage(BG::TextLogEntry message) {
  log->addEntry(message);
}

boost::optional<BaseActor*> BlackguardGM::getActor(std::string tag) const {
  boost::optional<BaseActor*> option;
  auto entity = entityManager->getEntity(tag);
  if (auto actor = dynamic_cast<BaseActor*>(entity.get_value_or(nullptr))) {
    option = {actor};
  }
  return option;
}

std::vector<BG::Actor*> BlackguardGM::getEnemiesInArea(BaseActor* actor) const {
  std::vector<BG::Actor*> entities = entityManager->getActorsInArea(actor->getArea().get());
  std::vector<BG::Actor*> enemiesInArea(entities.size());
  auto iter = std::copy_if(entities.begin(), entities.end(), enemiesInArea.begin(),
                           [actor](BG::Actor* a){return actor->getFaction()->isHostileTo(a->getFaction());});
  enemiesInArea.resize(std::distance(enemiesInArea.begin(), iter));
  return enemiesInArea;
}

std::vector<BG::Item*> BlackguardGM::getItemsAtPosition(BG::Position position, const BG::AreaModel* area) const {
  return entityManager->getItemsAtPosition(position, area);
}

bool BlackguardGM::actorIsWithinLOS(BG::Actor* source, BG::Actor* target) const {
  return source->getArea().get()->lineOfSightExists(source->getPosition().get(), target->getPosition().get());
}

bool BlackguardGM::actorIsWithinAttackRange(BG::Actor* attacker, BG::Actor* target) const {
  auto attackerCell = attacker->getArea().get()->getCell(*attacker->getPosition());
  auto targetCell = target->getArea().get()->getCell(*target->getPosition());
  if (attackerCell && targetCell) {
    return attacker->getArea().get()->cellsAreAdjacent(attackerCell.get(), targetCell.get());
  } else {
    LOG(ERROR) << "BlackguardGM::actorIsWithinAttackRange(): Failed to locate target cell";
    return false;
  }
}

void BlackguardGM::openInventory(BaseActor* player) {
  setContext("inventory");
  auto dialogFont = fontManager->getFont("basic");
  if (dialogFont) {
    window->getMasterPanel()->addChild(new InventoryDialog(INVENTORY_DIALOG, this, dialogFont.get(), player, window->getMasterPanel()));
  } else {
    LOG(ERROR) << "BlackguardGM::openInventory(): failed to find font 'basic'";
  }

}

void BlackguardGM::closeInventory() {
  setContext("field");
  window->getMasterPanel()->removeChild(INVENTORY_DIALOG);
}

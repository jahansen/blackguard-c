// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef BASEACTOR_H
#define BASEACTOR_H

#include <libintl.h>
#include <boost/optional.hpp>
#include <SDL2/SDL.h>
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/Position.h"
#include "blackguard/logic/Direction.h"
#include "blackguard/logic/EquipmentSlot.h"

#define i18n(STRING) gettext(STRING)

namespace BG {
  class AreaModel;
  class Tile;
  class Faction;
  class Equipment;
}

class BlackguardGM;

enum Slot{Head, Body, RightHand, LeftHand};

class BaseActor : public BG::Actor {
  public:
    BaseActor(std::string tag, std::string name, bool alive, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, int hp, int attack, BlackguardGM* gm);

    inline int getHP() const {return hp;}
    inline int getAttack() const {return attack;}

    void damage(int amount, BaseActor* aggressor);

    virtual void inspectInventory();
    virtual void dropItem();
    virtual void equipItem(const BG::Equipment* item);

  protected:
    BlackguardGM* gm;

    std::string displayName;
    int hp;
    int attack;

    void equipToSlot(BG::EquipmentSlot& slot, const BG::Equipment* item);
};

#endif // BASEACTOR_H

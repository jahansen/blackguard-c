// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "PC.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/Item.h"
#include "BlackguardGM.h"
#include "../external/easylogging++.h"

PC::PC(const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, BlackguardGM* gm)
: BaseActor("player", "Player", true, area, position, faction, tile, 100, 3, gm) {}

BG::Entity* PC::getCopy() const {
  auto copy = new PC(area.get(), position.get(), faction, tile, gm);
  copy->id = id;
  return copy;
}

void PC::move(const BG::Direction& direction) {
  gm->moveActor(this, area.get(), direction);
}

void PC::pickUp() {
  auto itemsAtFeet = gm->getItemsAtPosition(position.get(), area.get());
  if (!itemsAtFeet.empty()) {
    auto item = itemsAtFeet.at(0);
    gm->giveItem(this, item);
    gm->logMessage(BG::TextLogEntry("You pick up a " + item->getName()));
    moveDone();
  }
}

void PC::wait() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(1));
}

void PC::moveDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeMillisecondsFromNow(500));
}

void PC::attackDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(3));
}

void PC::bumpActor(BG::Actor* actor) {
  if (auto baseActor = dynamic_cast<BaseActor*>(actor)) {
    gm->executeAttack(this, baseActor);
  } else {
    LOG(ERROR) << "PC::bumpActor(): failed to cast Actor to BaseActor";
  }
}

void PC::bumpWall() {
  gm->logMessage(BG::TextLogEntry(i18n("You can't go there")));
}

void PC::onDeath() {
  gm->quit();
}

void PC::startTurn() {
  gm->logMessage(BG::TextLogEntry("Player's turn"));
  myTurn = true;
  gm->waitForInput();
}


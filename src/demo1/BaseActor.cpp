// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "BaseActor.h"
#include "blackguard/logic/AreaModel.h"
#include "BlackguardGM.h"
#include "blackguard/logic/Item.h"
#include "blackguard/logic/Equipment.h"

BaseActor::BaseActor(std::string tag, std::string name, bool alive, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, int hp, int attack, BlackguardGM* gm)
: Actor(tag, name, alive, area, position, faction, tile),
  gm(gm),
  hp(hp),
  attack(attack) {
  addEquipmentSlot(BG::EquipmentSlot(Slot::Head));
  addEquipmentSlot(BG::EquipmentSlot(Slot::Body));
  addEquipmentSlot(BG::EquipmentSlot(Slot::RightHand));
  addEquipmentSlot(BG::EquipmentSlot(Slot::LeftHand));
}

void BaseActor::damage(int amount, BaseActor* aggressor) {
  hp -= amount;
}

void BaseActor::inspectInventory() {

}

void BaseActor::dropItem() {
  if (!inventory.getItems().empty()) {
    auto itemToDrop = inventory.getItems().at(0);
    inventory.removeItem(itemToDrop);

    itemToDrop->setArea(area.get());
    itemToDrop->setPosition(position.get());
  }
}

void BaseActor::equipItem(const BG::Equipment* item) {
  auto slot = std::find_if(equipmentSlots.begin(), equipmentSlots.end(), [item](BG::EquipmentSlot eqSlot)->bool{
    return eqSlot.getSlotType() == item->getEquipmentType();
  });

  if (slot != equipmentSlots.end()) {
    equipToSlot(*slot, item);
  }
}

void BaseActor::equipToSlot(BG::EquipmentSlot& slot, const BG::Equipment* item) {
  auto equipped = slot.getEquipment();
  if (equipped && equipped.get()->getTag() == item->getTag()) {
    slot.unequip();
    gm->logMessage(BG::TextLogEntry(name + " unequips the " + equipped.get()->getName()));
  } else {
    slot.equip(item);
    gm->logMessage(BG::TextLogEntry(name + " equips the " + item->getName()));
  }
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "IdleBehaviour.h"
#include "BlackguardGM.h"
#include "NPC.h"

IdleBehaviour::IdleBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor)
: Behaviour(tag, name, gm, actor) {}

BG::Entity* IdleBehaviour::getCopy() const {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto npc = dynamic_cast<NPC*>(actor);
  auto copy = new IdleBehaviour(tag, name, bgGM, npc);
  copy->id = id;
  return copy;
}

void IdleBehaviour::execute() {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto npc = dynamic_cast<NPC*>(actor);

  auto enemiesInArea = bgGM->getEnemiesInArea(npc);
  if (enemiesInArea.empty()) {
    npc->idle();
  } else {
    auto target = enemiesInArea.at(0);
    if (bgGM->actorIsWithinLOS(npc, target)) {
      bgGM->logMessage(BG::TextLogEntry(npc->getName() + " shouts as it sees " + target->getName()));

      npc->setBehaviour("attackEnemy");
      npc->getMemory()->memorise("targetEnemy", target);
      npc->getMemory()->memorise("targetEnemyPosition", &target->getPosition().get());
      npc->startTurn();
    } else {
      npc->idle();
    }
  }
}

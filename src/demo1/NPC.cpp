// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "NPC.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/Behaviour.h"
#include "BlackguardGM.h"
#include "IdleBehaviour.h"
#include "AttackEnemyBehaviour.h"
#include "SearchForEnemyBehaviour.h"
#include "blackguard/logic/Item.h"
#include "../external/easylogging++.h"

NPC::NPC(std::string name, std::string tag, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, BlackguardGM* gm)
: BaseActor(tag, name, true, area, position, faction, tile, 5, 1, gm) {
  behaviours.push_back(std::unique_ptr<IdleBehaviour>(new IdleBehaviour("idle", "Idle", gm, this)));
  behaviours.push_back(std::unique_ptr<AttackEnemyBehaviour>(new AttackEnemyBehaviour("attackEnemy", "Attack Enemy", gm, this)));
  behaviours.push_back(std::unique_ptr<SearchForEnemyBehaviour>(new SearchForEnemyBehaviour("searchForEnemy", "Search for Enemy", gm, this)));
  setBehaviour("idle");
}

BG::Entity* NPC::getCopy() const {
  auto copy = new NPC(name, tag, area.get(), position.get(), faction, tile, gm);
  copy->id = id;
  return copy;
}

void NPC::move(const BG::Direction& direction) {
  gm->moveActor(this, area.get(), direction);
}

void NPC::pickUp() {

}

void NPC::wait() {
  gm->logMessage(BG::TextLogEntry(name + " stands around, confused."));
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(1));
}

void NPC::moveDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeMillisecondsFromNow(750));
}

void NPC::attackDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(3));
}

void NPC::bumpActor(BG::Actor* actor) {
  if (auto target = dynamic_cast<BaseActor*>(actor)) {
    gm->executeAttack(this, target);
  } else {
    LOG(ERROR) << "NPC::bumpActor(): failed to cast Actor to BaseActor";
  }
}

void NPC::bumpWall() {
  gm->logMessage(BG::TextLogEntry(name + " walks into the wall."));
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(3));
}

void NPC::onDeath() {
  while (!inventory.getItems().empty()) {
    dropItem();
  }

  gm->logMessage(BG::TextLogEntry(name + " curses you with its dying breath."));
}

void NPC::startTurn() {
  gm->logMessage(BG::TextLogEntry(name + "'s turn."));

  currentBehaviour->execute();
}

void NPC::idle() {
  auto itemsAtFeet = gm->getItemsAtPosition(position.get(), area.get());
  if (!itemsAtFeet.empty()) {
    auto item = itemsAtFeet.at(0);
    gm->giveItem(this, item);
    gm->logMessage(BG::TextLogEntry(name + " picks up a " + item->getName()));
    moveDone();
    return;
  }

  // Move in random direction
  auto dice = gm->getGenerator();
  auto directionRoll = dice->rolldX(9);
  switch (directionRoll) {
    case 1:
      move(BG::Direction::NORTH);
      break;

    case 2:
      move(BG::Direction::EAST);
      break;

    case 3:
      move(BG::Direction::SOUTH);
      break;

    case 4:
      move(BG::Direction::WEST);
      break;

    case 5:
      move(BG::Direction::NORTH_EAST);
      break;

    case 6:
      move(BG::Direction::SOUTH_EAST);
      break;

    case 7:
      move(BG::Direction::SOUTH_WEST);
      break;

    case 8:
      move(BG::Direction::NORTH_WEST);
      break;

    case 9:
      wait();
      break;
  }
}

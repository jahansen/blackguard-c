// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Blackguard.h"
#include "AreaPanel.h"
#include "PC.h"
#include "NPC.h"
#include "BlackguardGM.h"
#include "blackguard/logic/Equipment.h"
#include "blackguard/interface/TextPanel.h"
#include "blackguard/interface/TextLogPanel.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/logic/Faction.h"
#include "FieldContext.h"
#include "InventoryContext.h"
#include "Area.h"
#include "../external/easylogging++.h"

Blackguard::Blackguard(int argc, char** argv)
          : Application(BLACKGUARD_FPS, argc, argv) {
}

Blackguard::~Blackguard() {
}

bool Blackguard::initialise() {

  if (!window->initialise(BG::Application::DEFAULT_WIDTH, BG::Application::DEFAULT_HEIGHT, "Blackguard")) {
    LOG(ERROR) << SDL_GetError();
    return false;
  }

  return (loadResources() &&
          buildGame() &&
          buildScreen());
}

bool Blackguard::loadResources() {
  BG::ErrorCollection errors;

  auto area = std::unique_ptr<Area>(new Area("level01", "Goblin Cave"));
  area->loadAreaFromFileWithErrors("data/testArea.bga", errors);
  world.addArea(std::move(area));

  tilesetManager.createTilesetWithErrors("world", "World Tiles", "data/worldTileset.bgt", window->getRenderer(), errors);
  tilesetManager.createTilesetWithErrors("actors", "Actor Tiles", "data/actorTileset.bgt", window->getRenderer(), errors);
  tilesetManager.createTilesetWithErrors("items", "Item Tiles", "data/itemTileset.bgt", window->getRenderer(), errors);
  tilesetManager.createTilesetWithErrors("equipment", "Equipment Tiles", "data/equipmentTileset.bgt", window->getRenderer(), errors);

  fontManager.createFont("basic", "Base Font", "data/luximr.ttf", 16, errors);

  if (errors.isEmpty()) {
    return true;
  } else {
    errors.printToLog();
    return false;
  }
}

bool Blackguard::buildGame() {
  BG::ErrorCollection errors;
  auto actorTileset = tilesetManager.getTileset("actors");
  auto itemTileset = tilesetManager.getTileset("items");
  auto equipmentTileset = tilesetManager.getTileset("equipment");
  auto area = world.getArea("level01");
  if (!actorTileset) errors.addError("Blackguard::buildGame(): Unable to locate tileset 'actors'");
  if (!itemTileset) errors.addError("Blackguard::buildGame(): Unable to locate tileset 'items'");
  if (!equipmentTileset) errors.addError("Blackguard::buildGame(): Unable to locate tileset 'equipment'");
  if (!area) errors.addError("Blackguard::buildGame(): Unable to locate area 'level01'");

  if (errors.isEmpty()) {
    gameMaster = std::unique_ptr<BlackguardGM>(new BlackguardGM(&entityManager, &world, &fontManager, &mainLog, window.get()));
    auto bgGM = dynamic_cast<BlackguardGM*>(gameMaster.get());

    factionManager.addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("player", "Player")));
    factionManager.addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("enemy", "Enemy")));
    factionManager.addTwoWayRelationship("player", "enemy", -100);

    auto playerFaction = factionManager.getFaction("player").get();
    auto enemyFaction = factionManager.getFaction("enemy").get();

    auto playerTile = getTile(actorTileset.get(), "player", errors);
    auto goblinTile = getTile(actorTileset.get(), "goblin", errors);

    auto swordTile = getTile(itemTileset.get(), "sword", errors);
    auto potionTile = getTile(itemTileset.get(), "potion", errors);
    auto goldTile = getTile(itemTileset.get(), "gold", errors);
    auto armourTile = getTile(itemTileset.get(), "armour", errors);
    auto helmetTile = getTile(itemTileset.get(), "helmet", errors);
    auto shieldTile = getTile(itemTileset.get(), "shield", errors);
    auto longswordAvatarTile = getTile(equipmentTileset.get(), "longsword", errors);
    auto warswordAvatarTile = getTile(equipmentTileset.get(), "warsword", errors);
    auto fieldPlateAvatarTile = getTile(equipmentTileset.get(), "field_plate", errors);
    auto fullPlateAvatarTile = getTile(equipmentTileset.get(), "full_plate", errors);
    auto kiteShieldAvatarTile = getTile(equipmentTileset.get(), "kite_shield", errors);
    auto knightShieldAvatarTile = getTile(equipmentTileset.get(), "knight_shield", errors);
    auto ironHelmAvatarTile = getTile(equipmentTileset.get(), "iron_helm", errors);
    auto steelHelmAvatarTile = getTile(equipmentTileset.get(), "steel_helm", errors);

    if (errors.isEmpty()) {
      entityManager.addEntity(std::unique_ptr<PC>(new PC(area.get(), BG::Position(7, 7), playerFaction, playerTile.get(), bgGM)));
      entityManager.addEntity(std::unique_ptr<NPC>(new NPC("Ally", "goblinA", area.get(), BG::Position(4, 4), playerFaction, playerTile.get(), bgGM)));
      entityManager.addEntity(std::unique_ptr<NPC>(new NPC("Goblin B", "goblinB", area.get(), BG::Position(5, 6), enemyFaction, goblinTile.get(), bgGM)));
      entityManager.addEntity(std::unique_ptr<NPC>(new NPC("Goblin C", "goblinC", area.get(), BG::Position(2, 2), enemyFaction, goblinTile.get(), bgGM)));

      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("longsword", "Longsword", Slot::RightHand, area.get(), BG::Position(5, 5), swordTile.get(), longswordAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("warsword", "Warsword", Slot::RightHand, area.get(), BG::Position(5, 5), swordTile.get(), warswordAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("fullplate", "Full Plate Mail", Slot::Body, area.get(), BG::Position(7, 5), armourTile.get(), fullPlateAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("fieldplate", "Field Plate", Slot::Body, area.get(), BG::Position(7, 5), armourTile.get(), fieldPlateAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("ironhelm", "Iron Helmet", Slot::Head, area.get(), BG::Position(7, 6), helmetTile.get(), ironHelmAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("steelhelm", "Steel Helmet", Slot::Head, area.get(), BG::Position(7, 6), helmetTile.get(), steelHelmAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("kiteshield", "Kite Shield", Slot::LeftHand, area.get(), BG::Position(8, 6), shieldTile.get(), kiteShieldAvatarTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Equipment>(new BG::Equipment("knightshield", "Knight Shield", Slot::LeftHand, area.get(), BG::Position(8, 6), shieldTile.get(), knightShieldAvatarTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("potion", "Potion", area.get(), BG::Position(7, 5), potionTile.get())));
      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold2", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold3", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold4", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold5", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold6", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold7", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold8", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));
//      entityManager.addEntity(std::unique_ptr<BG::Item>(new BG::Item("gold9", "Gold", area.get(), BG::Position(3, 3), goldTile.get())));

      gameMaster->scheduleInitialTurns();

      auto player = dynamic_cast<BaseActor*>(entityManager.getEntity("player").get());
      gameMaster->addContext(std::unique_ptr<FieldContext>(new FieldContext(bgGM, player)));
      gameMaster->addContext(std::unique_ptr<InventoryContext>(new InventoryContext(bgGM, player)));
      gameMaster->setContext("field");
      return true;
    }
  }

  errors.printToLog();
  return false;
}

bool Blackguard::buildScreen() {
  BG::ErrorCollection errors;
  auto font = fontManager.getFont("basic");
  auto area = world.getArea("level01");
  if (!font) errors.addError("Blackguard::buildScreen(): Unable to load font 'basic'");
  if (!area) errors.addError("Blackguard::buildScreen(): Unable to load area 'level01'");

  if (errors.isEmpty()) {
    areaPanel = new AreaPanel("area-main", area.get(), BG::Coordinates(0, 0), 768, 640, window->getMasterPanel(), &fontManager, &tilesetManager, &entityManager);

    textPanel = new BG::TextPanel("title-text", "Blackguard", font.get(), BG::Coordinates(800, 10), 200, 600, window->getMasterPanel(), BG::Colour(BG::ColourId::WHITE));

    logPanel = new BG::TextLogPanel("log-main", font.get(), BG::Coordinates(0, 565), 1024, 200, window->getMasterPanel(), &mainLog);
    logPanel->addEntry(BG::TextLogEntry(i18n("Welcome to Blackguard!"), BG::Colour(BG::ColourId::GREEN)));
    logPanel->addEntry(BG::TextLogEntry(i18n("Version 0.1"), BG::Colour(BG::ColourId::GREEN)));

    window->getMasterPanel()->addChild(areaPanel);
    window->getMasterPanel()->addChild(textPanel);
    window->getMasterPanel()->addChild(logPanel);
    return true;
  } else {
    errors.printToLog();
    return false;
  }
}

void Blackguard::run() {
  while (!quit) {
    masterTimer->tick();
    gameMaster->updateWorld();
    gameMaster->handleInput(quit);
    updateFrame();
    waitForNextFrame();
  }
}

boost::optional<const BG::Tile*> Blackguard::getTile(const BG::Tileset* tileset, std::string tileTag, BG::ErrorCollection& errors) {
  auto tileOpt = tileset->getTile(tileTag);
  if (!tileOpt) errors.addError("Blackguard::getTile(): Unable to locate tile '" + tileTag + "'");
  return tileOpt;
}

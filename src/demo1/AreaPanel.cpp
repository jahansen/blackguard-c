// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AreaPanel.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/CellModel.h"
#include "blackguard/core/FontManager.h"
#include "blackguard/interface/manager/TilesetManager.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/Item.h"
#include "blackguard/core/ErrorCollection.h"

AreaPanel::AreaPanel(std::string tag, const BG::AreaModel* areaModel, BG::Coordinates offset, int width, int height, Panel* parent, BG::FontManager* fontManager, BG::TilesetManager* tilesetManager, BG::EntityManager* entityManager)
: BG::SubPanel(tag, offset, width, height, parent),
  areaModel(areaModel),
  fontManager(fontManager),
  tilesetManager(tilesetManager),
  entityManager(entityManager) {}

void AreaPanel::drawSelf() {
  BG::ErrorCollection errors;
  auto worldTileset = tilesetManager->getTileset("world");
  if (!worldTileset) errors.addError("AreaPanel::drawSelf(): Unable to locate tileset 'world'");

  if (errors.isEmpty()) {
    const BG::CellMatrix* areaMatrix = areaModel->getMatrix();
    for (auto row : *areaMatrix) {
      for (auto cell : row) {
        auto curTile = worldTileset->getTile(cell.getProperty("tile"));
        if (curTile) {
          SDL_Rect cellDim = {(short int)(cell.getColPos() * TILE_WIDTH), (short int)(cell.getRowPos() * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
          SDL_Rect tileClip = curTile->getClip();
          SDL_RenderCopy(parent->getRenderer(), curTile->getImage()->getTexture(), &tileClip, &cellDim);
        } else {
          errors.addError("AreaPanel::drawSelf(): Unable to locate tile '" + cell.getProperty("tile") + "'");
        }
      }
    }

    auto itemsInArea = entityManager->getItemsInArea(areaModel);
    for (int i = 0; i < (int)itemsInArea.size(); ++i) {
      auto entity = dynamic_cast<BG::DrawableEntity*>(itemsInArea.at(i));
      SDL_Rect target = {(short int)(entity->getPosition()->getCol() * TILE_WIDTH), (short int)(entity->getPosition()->getRow() * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
      SDL_Rect source = entity->getTile()->getClip();
      SDL_RenderCopy(parent->getRenderer(), entity->getTile()->getImage()->getTexture(), &source, &target);
    }

    auto actorsInArea = entityManager->getActorsInArea(areaModel);
    for (int i = 0; i < (int)actorsInArea.size(); ++i) {
      auto actor = actorsInArea.at(i);
      auto entity = dynamic_cast<BG::DrawableEntity*>(actor);
      SDL_Rect target = {(short int)(entity->getPosition()->getCol() * TILE_WIDTH), (short int)(entity->getPosition()->getRow() * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
      SDL_Rect source = entity->getTile()->getClip();
      SDL_RenderCopy(parent->getRenderer(), entity->getTile()->getImage()->getTexture(), &source, &target);

      auto equippedItems = actor->getEquippedItems();
      for (auto equipment : equippedItems) {
        SDL_Rect equipmentSource = equipment->getAvatarTile()->getClip();
        SDL_RenderCopy(parent->getRenderer(), equipment->getAvatarTile()->getImage()->getTexture(), &equipmentSource, &target);
      }
    }
  }
  errors.printToLog();
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef INVENTORYLISTITEM_H
#define INVENTORYLISTITEM_H

#include "blackguard/interface/ListPanelItem.h"

namespace BG {
  class Item;
  class Font;
}

class InventoryListItem : public BG::ListPanelItem {
  public:
    InventoryListItem(const BG::Item* item, const BG::Font* font, Panel* parent);
    virtual ~InventoryListItem();

    virtual void drawSelf() override;

    inline const BG::Item* getItem() const {return item;}

  private:
    const BG::Item* item;

    static const int WIDTH = 300;
    static const int HEIGHT = 50;
};

#endif // INVENTORYLISTITEM_H

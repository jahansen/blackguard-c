// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef SEARCHFORENEMYBEHAVIOUR_H
#define SEARCHFORENEMYBEHAVIOUR_H

#include "blackguard/logic/Behaviour.h"

class BlackguardGM;
class NPC;

namespace BG {
  class Actor;
}

class SearchForEnemyBehaviour : public BG::Behaviour {
  public:
    SearchForEnemyBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor);
    virtual ~SearchForEnemyBehaviour();

    virtual BG::Entity* getCopy() const override;

    virtual void execute() override;
};

#endif // SEARCHFORENEMYBEHAVIOUR_H

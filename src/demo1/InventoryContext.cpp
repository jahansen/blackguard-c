// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "InventoryContext.h"
#include "PC.h"
#include "BlackguardGM.h"
#include "InventoryDialog.h"
#include "blackguard/interface/MasterPanel.h"
#include "blackguard/logic/Equipment.h"
#include "../external/easylogging++.h"

InventoryContext::InventoryContext(BlackguardGM* gm, BaseActor* player)
: BaseContext("inventory", gm, player) {}

InventoryContext::~InventoryContext() {}

void InventoryContext::handleInput(const SDL_Event& event, bool& quit) {
  if (event.type == SDL_KEYDOWN) {
    switch (event.key.keysym.sym) {
      case SDLK_ESCAPE:
        closeDialog();
        break;

      case SDLK_KP_8:
        if (auto dialogOpt = getInventoryDialog()) {
          auto dialog = dialogOpt.get();
          dialog->previousItem();
        } else {
          closeDialog();
        }
        break;

      case SDLK_KP_2:
        if (auto dialogOpt = getInventoryDialog()) {
          auto dialog = dialogOpt.get();
          dialog->nextItem();
        } else {
          closeDialog();
        }
        break;

      case SDLK_e:
        if (auto dialogOpt = getInventoryDialog()) {
          auto dialog = dialogOpt.get();
          auto item = dialog->getSelectedItem();
          if (auto equipment = dynamic_cast<const BG::Equipment*>(item)) {
            player->equipItem(equipment);
            dialog->updateItemInfo();
          } else {
            gm->logMessage(BG::TextLogEntry("You can't equip that."));
          }
        } else {
          closeDialog();
        }
        break;

      case SDLK_q:
        quit = true;
        break;

      default:
        break;
    }
  }
}

// Lazy get for InventoryDialog
boost::optional<InventoryDialog*> InventoryContext::getInventoryDialog() {
  if (!inventoryDialog) {
      auto dialogOpt = gm->getMasterPanel()->getChild("inventory-dialog");
    if (auto dialog = dynamic_cast<InventoryDialog*>(dialogOpt.get_value_or(nullptr))) {
      inventoryDialog = dialog;
    } else {
      LOG(ERROR) << "InventoryContext(): unable to find or cast inventoryDialog in interface";
    }
  }
  return inventoryDialog;
}

void InventoryContext::closeDialog() {
  inventoryDialog = boost::none;
  gm->closeInventory();
}

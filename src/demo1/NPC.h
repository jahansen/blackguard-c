// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ENEMY_H
#define ENEMY_H

#include "BaseActor.h"

namespace BG {
  class AreaModel;
  class Tile;
  class Behaviour;
  class Faction;
}

class BlackguardGM;

class NPC : public BaseActor {
  public:
    NPC(std::string name, std::string tag, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, BlackguardGM* gm);

    virtual BG::Entity* getCopy() const override;
    inline virtual bool waitForInput() const override {return false;}

    virtual void move(const BG::Direction& direction) override;
    virtual void pickUp() override;
    virtual void wait() override;
    virtual void moveDone() override;
    virtual void attackDone() override;
    virtual void bumpActor(BG::Actor* actor) override;
    virtual void bumpWall() override;

    virtual void onDeath() override;

    virtual void startTurn() override;

    void idle();
};

#endif // ENEMY_H

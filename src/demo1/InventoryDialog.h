// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef INVENTORYDIALOG_H
#define INVENTORYDIALOG_H

#include "blackguard/interface/DialogPanel.h"

namespace BG {
  class Font;
  class MenuPanel;
  class ContainerPanel;
  class Item;
}

class BlackguardGM;
class BaseActor;

class InventoryDialog : public BG::DialogPanel {
  public:
    InventoryDialog(std::string tag, BlackguardGM* gm, const BG::Font* font, BaseActor* player, Panel* parent);
    virtual ~InventoryDialog();

    virtual void drawSelf() override;

    void nextItem();
    void previousItem();
    const BG::Item* getSelectedItem();

    void updateItemInfo();

  private:
    BlackguardGM* gm;
    const BG::Font* font;
    BaseActor* player;
    BG::MenuPanel* menu;
    BG::ContainerPanel* rightPanel;

    void buildDialog();

    static const int X = 100;
    static const int Y = 100;
    static const int WIDTH = 600;
    static const int HEIGHT = 400;
};

#endif // INVENTORYDIALOG_H

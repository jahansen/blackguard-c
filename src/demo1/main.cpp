// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include <libintl.h>
#include <locale.h>
#include "blackguard/core/Window.h"
#include "demo1/Blackguard.h"
#include "../external/easylogging++.h"
#include <memory>

_INITIALIZE_EASYLOGGINGPP

#define i18n(STRING) gettext(STRING)

void initI18N() {
  setlocale(LC_ALL, "");
  bindtextdomain("Blackguard", "/usr/share/locale");
  textdomain("Blackguard");
}

void initBlackguard(int argc, char** argv) {
  auto blackguard = std::unique_ptr<Blackguard>(new Blackguard(argc, argv));
  if (!blackguard->initialise()) {
    LOG(FATAL) << "Failed to successfully initialise Blackguard.";
    exit(EXIT_FAILURE);
  }

  blackguard->run();
}

int main(int argc, char** argv) {

  initI18N();

  initBlackguard(argc, argv);

  return EXIT_SUCCESS;
}



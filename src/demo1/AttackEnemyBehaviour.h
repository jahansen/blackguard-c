// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ATTACKENEMYBEHAVIOUR_H
#define ATTACKENEMYBEHAVIOUR_H

#include "blackguard/logic/Behaviour.h"

class BlackguardGM;
class NPC;

namespace BG {
  class Actor;
}

class BaseActor;

class AttackEnemyBehaviour : public BG::Behaviour {
  public:
    AttackEnemyBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor);
    virtual ~AttackEnemyBehaviour();

    virtual BG::Entity* getCopy() const override;
    virtual void execute() override;

  private:
    void attackEnemy(BaseActor* actor);
};

#endif // ATTACKENEMYBEHAVIOUR_H

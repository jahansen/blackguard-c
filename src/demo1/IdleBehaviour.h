// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef IDLEJOB_H
#define IDLEJOB_H

#include "blackguard/logic/Behaviour.h"

class BlackguardGM;
class NPC;

class IdleBehaviour : public BG::Behaviour {
  public:
    IdleBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor);

    virtual BG::Entity* getCopy() const override;

    virtual void execute() override;
};

#endif // IDLEJOB_H

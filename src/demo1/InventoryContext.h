// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef INVENTORYCONTEXT_H
#define INVENTORYCONTEXT_H

#include <boost/optional.hpp>
#include "BaseContext.h"

class InventoryDialog;

class InventoryContext : public BaseContext {
  public:
    InventoryContext(BlackguardGM* gm, BaseActor* player);
    virtual ~InventoryContext();

    virtual void handleInput(const SDL_Event& event, bool& quit) override;

  private:
    boost::optional<InventoryDialog*> inventoryDialog;

    boost::optional<InventoryDialog*> getInventoryDialog();
    void closeDialog();
};

#endif // INVENTORYCONTEXT_H

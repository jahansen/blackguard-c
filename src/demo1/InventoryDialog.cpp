// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "InventoryDialog.h"
#include "blackguard/interface/TextPanel.h"
#include "blackguard/logic/Item.h"
#include "blackguard/interface/MenuPanel.h"
#include "blackguard/interface/ContainerPanel.h"
#include "blackguard/logic/Tile.h"
#include "blackguard/interface/Image.h"
#include "InventoryListItem.h"
#include "BaseActor.h"
#include "../external/easylogging++.h"

InventoryDialog::InventoryDialog(std::string tag, BlackguardGM* gm, const BG::Font* font, BaseActor* player, Panel* parent)
: DialogPanel(tag, BG::Coordinates(X, Y), WIDTH, HEIGHT, parent),
  gm(gm),
  font(font),
  player(player) {
  buildDialog();
}

InventoryDialog::~InventoryDialog() {}

void InventoryDialog::buildDialog() {
  menu = new BG::MenuPanel("inventory-menu", BG::Coordinates(0, 0), WIDTH/2, HEIGHT, this);
  rightPanel = new BG::ContainerPanel("right-panel", BG::Coordinates(WIDTH/2, 0), WIDTH/2, HEIGHT, this);
  addChild(menu);
  addChild(rightPanel);

  auto items = player->getInventory()->getItems();
  for (int i = 0; i < (int)items.size(); ++i) {
    menu->addItem(std::unique_ptr<InventoryListItem>(new InventoryListItem(items.at(i), font, menu)));
  }

  auto itemHeader = new BG::TextPanel("item-header", "Item Details", font, BG::Coordinates(100, 5), 100, 100, rightPanel, BG::Colour(BG::ColourId::WHITE));
  rightPanel->addChild(itemHeader);

  nextItem();
}

void InventoryDialog::nextItem() {
  menu->selectNext();
  updateItemInfo();
}

void InventoryDialog::previousItem() {
  menu->selectPrevious();
  updateItemInfo();
}

const BG::Item* InventoryDialog::getSelectedItem() {
  auto selectedItem = menu->getSelectedItem();
  if (auto selectedItem = dynamic_cast<InventoryListItem*>(menu->getSelectedItem().get_value_or(nullptr))) {
    return selectedItem->getItem();
  } else {
    LOG(FATAL) << "InventoryDialog::getSelectedItem(): failed to cast ListItem* to InventoryListItem*";
    return nullptr;
  }
}

void InventoryDialog::updateItemInfo() {
  auto selectedItem = menu->getSelectedItem();
  if (auto selectedItem = dynamic_cast<InventoryListItem*>(menu->getSelectedItem().get_value_or(nullptr))) {
    auto item = selectedItem->getItem();

    rightPanel->removeChild("item-name");
    auto itemName = new BG::TextPanel("item-name", "Name: " + item->getName() + (player->hasItemEquipped((const BG::Equipment*)item) ? " (Equipped)" : ""), font, BG::Coordinates(10, 30), 100, 100, rightPanel, BG::Colour(BG::ColourId::WHITE));
    rightPanel->addChild(itemName);
  }
}

void InventoryDialog::drawSelf() {
  SDL_Rect absDimensions = getAbsoluteDimensions();
  SDL_RenderFillRect(getRenderer(), &absDimensions);
  SDL_SetRenderDrawColor(getRenderer(), 255, 0, 0, 0);
  SDL_RenderDrawRect(getRenderer(), &absDimensions);

  // TODO: user renderer/panel class to draw player
  auto absOffset = getAbsoluteOffset();
  SDL_Rect target = {(short int)absOffset.getX() + 410, (short int)absOffset.getY() + 290, 96, 96};
  SDL_Rect source = player->getTile()->getClip();
  SDL_RenderCopy(parent->getRenderer(), player->getTile()->getImage()->getTexture(), &source, &target);

  auto equippedItems = player->getEquippedItems();
  for (auto equipment : equippedItems) {
    SDL_Rect equipmentSource = equipment->getAvatarTile()->getClip();
    SDL_RenderCopy(parent->getRenderer(), equipment->getAvatarTile()->getImage()->getTexture(), &equipmentSource, &target);
  }

}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AttackEnemyBehaviour.h"
#include "BlackguardGM.h"
#include "NPC.h"
#include "../external/easylogging++.h"

AttackEnemyBehaviour::AttackEnemyBehaviour(std::string tag, std::string name, BlackguardGM* gm, NPC* actor)
: Behaviour(tag, name, gm, actor) {}

AttackEnemyBehaviour::~AttackEnemyBehaviour() {
}

BG::Entity* AttackEnemyBehaviour::getCopy() const {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto npc = dynamic_cast<NPC*>(actor);
  auto copy = new AttackEnemyBehaviour(tag, name, bgGM, npc);
  copy->id = id;
  return copy;
}

void AttackEnemyBehaviour::execute() {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto target = dynamic_cast<const BG::Actor*>(actor->getMemory()->recall("targetEnemy").get_value_or(nullptr));

  if (target) {
    auto targetEnemy = bgGM->getActor(target->getTag());
    if (targetEnemy && targetEnemy.get()->isAlive()) {
      attackEnemy(targetEnemy.get());
    } else {
      actor->getMemory()->forget("targetEnemy");
      actor->getMemory()->forget("targetEnemyPosition");
      actor->setBehaviour("idle");
      actor->startTurn();
    }
  } else {
    LOG(ERROR) << "AttackEnemyBehaviour::execute(): failed to cast memory entity to Actor";
  }
}

void AttackEnemyBehaviour::attackEnemy(BaseActor* target) {
  auto bgGM = dynamic_cast<BlackguardGM*>(gm);
  auto attacker = dynamic_cast<BaseActor*>(actor);

  if (bgGM->actorIsWithinLOS(attacker, target)) {
    if (bgGM->actorIsWithinAttackRange(attacker, target)) {
      bgGM->executeAttack(attacker, target);
    } else {
      bgGM->moveActorTowards(attacker, *target->getPosition());
      attacker->getMemory()->memorise("targetEnemyPosition", &target->getPosition().get());
    }
  } else {
    bgGM->logMessage(BG::TextLogEntry(attacker->getName() + " loses track of " + target->getName() + "..."));
    actor->setBehaviour("searchForEnemy");
    actor->startTurn();
  }
}

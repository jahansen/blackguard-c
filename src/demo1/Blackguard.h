// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef BLACKGUARD_H
#define BLACKGUARD_H

#include <libintl.h>
#include <memory>
#include <boost/optional.hpp>
#include "blackguard/core/Application.h"
#include "blackguard/core/Window.h"
#include "blackguard/logic/TextLog.h"

#define i18n(STRING) gettext(STRING)

namespace BG {
  class Tileset;
  class ErrorCollection;
  class TextPanel;
  class TextLogPanel;
  class Tile;
}

class PC;
class AreaPanel;

class Blackguard : public BG::Application {
  public:
    Blackguard(int argc, char** argv);
    virtual ~Blackguard();

    virtual bool initialise() override;
    virtual void run() override;

  private:
    static const int BLACKGUARD_FPS = 30;

    AreaPanel* areaPanel;
    BG::TextPanel* textPanel;
    BG::TextLogPanel* logPanel;
    BG::TextLog mainLog;

    bool loadResources();
    bool buildScreen();
    bool buildGame();
    boost::optional<const BG::Tile*> getTile(const BG::Tileset* tileset, std::string tileTag, BG::ErrorCollection& errors);
};

#endif // BLACKGUARD_H

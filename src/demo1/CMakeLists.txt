# Demo 1

add_subdirectory(data)

set(demo1_SRCS main.cpp Area.cpp AreaPanel.cpp InventoryDialog.cpp AttackEnemyBehaviour.cpp BaseActor.cpp Blackguard.cpp BlackguardGM.cpp BaseContext.cpp FieldContext.cpp InventoryContext.cpp IdleBehaviour.cpp NPC.cpp PC.cpp SearchForEnemyBehaviour.cpp InventoryListItem.cpp)

include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_SOURCE_DIR}/external)

add_executable(demo1 ${demo1_SRCS})
target_link_libraries(demo1 blackguard)

install(TARGETS demo1 DESTINATION ${CMAKE_INSTALL_PREFIX})

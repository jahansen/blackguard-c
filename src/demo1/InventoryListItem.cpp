// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "InventoryListItem.h"
#include "blackguard/interface/TextPanel.h"
#include "blackguard/logic/Item.h"

InventoryListItem::InventoryListItem(const BG::Item* item, const BG::Font* font, Panel* parent)
: ListPanelItem(WIDTH, HEIGHT, parent),
  item(item) {
  addChild(new BG::TextPanel("inventory-row", item->getName(), font, BG::Coordinates(10, 15), 100, 30, this, BG::Colour(BG::ColourId::GREEN)));
}

InventoryListItem::~InventoryListItem() {}

void InventoryListItem::drawSelf() {
  auto absDimensions = getAbsoluteDimensions();

  if (selected) {
    SDL_SetRenderDrawColor(getRenderer(), 255, 0, 0, 0);
  } else {
    SDL_SetRenderDrawColor(getRenderer(), 255, 255, 255, 0);
  }

  SDL_RenderDrawRect(getRenderer(), &absDimensions);
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "LegendArea.h"
#include "blackguard/logic/Actor.h"

LegendArea::LegendArea(std::string tag, std::string name)
: BG::AreaModel(tag, name) {}

LegendArea::~LegendArea() {}

BG::Entity* LegendArea::getCopy() const {
  auto area = new LegendArea(tag, name);
  area->id = id;
  area->matrix = matrix;
  return area;
}

std::vector<const BG::CellModel*> LegendArea::getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  auto northCell = getAdjacentCell(cell, BG::Direction::NORTH);
  auto eastCell = getAdjacentCell(cell, BG::Direction::EAST);
  auto southCell = getAdjacentCell(cell, BG::Direction::SOUTH);
  auto westCell = getAdjacentCell(cell, BG::Direction::WEST);
  auto neCell = getAdjacentCell(cell, BG::Direction::NORTH_EAST);
  auto seCell = getAdjacentCell(cell, BG::Direction::SOUTH_EAST);
  auto swCell = getAdjacentCell(cell, BG::Direction::SOUTH_WEST);
  auto nwCell = getAdjacentCell(cell, BG::Direction::NORTH_WEST);

  std::vector<const BG::CellModel*> passableCells;
  if (northCell && cellIsPassable(northCell.get(), goalCell, actorsInArea)) passableCells.push_back(northCell.get());
  if (eastCell && cellIsPassable(eastCell.get(), goalCell, actorsInArea)) passableCells.push_back(eastCell.get());
  if (southCell && cellIsPassable(southCell.get(), goalCell, actorsInArea)) passableCells.push_back(southCell.get());
  if (westCell && cellIsPassable(westCell.get(), goalCell, actorsInArea)) passableCells.push_back(westCell.get());
  if (neCell && cellIsPassable(neCell.get(), goalCell, actorsInArea)) passableCells.push_back(neCell.get());
  if (seCell && cellIsPassable(seCell.get(), goalCell, actorsInArea)) passableCells.push_back(seCell.get());
  if (swCell && cellIsPassable(swCell.get(), goalCell, actorsInArea)) passableCells.push_back(swCell.get());
  if (nwCell && cellIsPassable(nwCell.get(), goalCell, actorsInArea)) passableCells.push_back(nwCell.get());

  return passableCells;
}

bool LegendArea::cellBlocksLineOfSight(const BG::CellModel* cell) const {
  return cell->getProperty("passable") == "false";
}

bool LegendArea::cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  if (cell == goalCell) return true; // The goal cell is always considered passable (otherwise a path to it will never be found)
  if (cell->getProperty("passable") == "false") return false;

  for (auto actor : actorsInArea) {
    if (*actor->getPosition() == cell->getPosition()) return false;
  }
  return true;
}

int LegendArea::getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const {
  auto travelDirection = getDirection(cell->getPosition(), neighbourCell->getPosition());

  switch (travelDirection) {
    case BG::Direction::NORTH:
    case BG::Direction::EAST:
    case BG::Direction::SOUTH:
    case BG::Direction::WEST:
      return 10;

    case BG::Direction::NORTH_EAST:
    case BG::Direction::SOUTH_EAST:
    case BG::Direction::SOUTH_WEST:
    case BG::Direction::NORTH_WEST:
      return 14;

    default:
      return 10;
  }
}

int LegendArea::estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const {
  // Chebyshev distance with straight movement cost of 10, diagonal of 14
  const int DS = 10;
  const int DD = 14;
  int dx = abs(start->getX() - goal->getX());
  int dy = abs(start->getY() - goal->getY());
  return DS * (dx + dy) + (DD - 2 * DS) * std::min(dx, dy);
}

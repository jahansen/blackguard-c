// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef LEGENDGM_H
#define LEGENDGM_H

#include "blackguard/core/GameMaster.h"
#include "blackguard/logic/Direction.h"

namespace BG {
  class EntityManager;
  class AreaManager;
  class TextLog;
}

class BaseActor;

class LegendGM : public BG::GameMaster {
  public:
    LegendGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window);
    virtual ~LegendGM();

    // Game inspectors
    boost::optional<BaseActor*> getActorInCell(const BG::CellModel* cell, const BG::AreaModel* area) const;

    // Game mutators
    void moveActor(BaseActor* actor, const BG::AreaModel* area, BG::Direction direction);
    void moveActorToCell(BaseActor* actor, const BG::CellModel* cell);
};

#endif // LEGENDGM_H

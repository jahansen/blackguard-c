// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Legend.h"
#include "LegendGM.h"
#include "VillageArea.h"
#include "LegendAreaPanel.h"
#include "PC.h"
#include "FieldContext.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/logic/Faction.h"
#include "blackguard/logic/Tileset.h"
#include "../external/easylogging++.h"

Legend::Legend(int argc, char **arg)
: Application(LegendFPS, argc, argv) {}

Legend::~Legend() {}

bool Legend::initialise() {
  if (window->initialise(BG::Application::DEFAULT_WIDTH, BG::Application::DEFAULT_HEIGHT, "Legend")) {
    return (loadResources() &&
            buildGame() &&
            buildScreen());
  } else {
    LOG(ERROR) << SDL_GetError();
    return false;
  }
}

bool Legend::loadResources() {
  BG::ErrorCollection errors;

  tilesetManager.createTilesetWithErrors("world", "World Tiles", "data/worldTileset.bgt", window->getRenderer(), errors);
  tilesetManager.createTilesetWithErrors("actors", "Actor Tiles", "data/actorTileset.bgt", window->getRenderer(), errors);

  if (errors.isEmpty()) {
    return true;
  } else {
    errors.printToLog();
    return false;
  }
}

bool Legend::buildGame() {
  BG::ErrorCollection errors;
  auto actorTileset = tilesetManager.getTileset("actors");
  if (!actorTileset) errors.addError("Legend::buildGame(); Unable to locate tileset 'actors'");

  if (errors.isEmpty()) {
    gameMaster = std::unique_ptr<LegendGM>(new LegendGM(&entityManager, &world, &fontManager, &mainLog, window.get()));
    auto legendGM = dynamic_cast<LegendGM*>(gameMaster.get());

    factionManager.addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("player", "Player")));
    auto playerFaction = factionManager.getFaction("player").get();

    auto playerTile = getTile(actorTileset.get(), "player", errors);

    if (errors.isEmpty()) {
      auto area = std::unique_ptr<VillageArea>(new VillageArea("hamlet", "Hamlet", gameMaster->getGenerator(), 48, 64));
      world.addArea(std::move(area));
      auto villageArea = world.getArea("hamlet");

      entityManager.addEntity(std::unique_ptr<PC>(new PC(villageArea.get(), BG::Position(7, 7), playerFaction, playerTile.get(), legendGM)));

      gameMaster->scheduleInitialTurns();

      auto player = dynamic_cast<BaseActor*>(entityManager.getEntity("player").get());
      gameMaster->addContext(std::unique_ptr<FieldContext>(new FieldContext(legendGM, player)));
      gameMaster->setContext("field");
      return true;
    }
  }

  errors.printToLog();
  return false;
}

bool Legend::buildScreen() {
  auto areaOpt = world.getArea("hamlet");
  if (areaOpt) {
    areaPanel = new LegendAreaPanel("area-main", areaOpt.get(), BG::Coordinates(0, 0), 768, 640, window->getMasterPanel(), &fontManager, &tilesetManager, &entityManager);

    window->getMasterPanel()->addChild(areaPanel);

    return true;
  } else {
    LOG(ERROR) << "Legend::buildScreen(): failed to load area";
    return false;
  }
}

void Legend::run() {
  while (!quit) {
    masterTimer->tick();
    gameMaster->updateWorld();
    gameMaster->handleInput(quit);
    updateFrame();
    waitForNextFrame();
  }
}

boost::optional<const BG::Tile*> Legend::getTile(const BG::Tileset* tileset, std::string tileTag, BG::ErrorCollection& errors) {
  auto tileOpt = tileset->getTile(tileTag);
  if (!tileOpt) errors.addError("Legend::getTile(): Unable to locate tile '" + tileTag + "'");
  return tileOpt;
}

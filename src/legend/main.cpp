// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "blackguard/core/Window.h"
#include "legend/Legend.h"
#include "../external/easylogging++.h"
#include <memory>

_INITIALIZE_EASYLOGGINGPP

void initLegend(int argc, char** argv) {
  auto legend = std::unique_ptr<Legend>(new Legend(argc, argv));
  if (!legend->initialise()) {
      LOG(ERROR) << "Failed to successfully initialise Legend.";
      exit(EXIT_FAILURE);
  }

  legend->run();
}

int main(int argc, char** argv) {
  initLegend(argc, argv);

  return EXIT_SUCCESS;
}

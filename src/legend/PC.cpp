// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "PC.h"
#include "LegendGM.h"

PC::PC(const BG::AreaModel *area, BG::Position position, BG::Faction *faction, const BG::Tile *tile, LegendGM *gm)
: BaseActor("player", "Player", true, area, position, faction, tile, gm) {}

PC::~PC() {}

BG::Entity* PC::getCopy() const {
  auto copy = new PC(area.get(), position.get(), faction, tile, gm);
  copy->id = id;
  return copy;
}

void PC::move(const BG::Direction& direction) {
  gm->moveActor(this, area.get(), direction);
}

void PC::pickUp() {

}

void PC::wait() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(5));
}


void PC::moveDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeMillisecondsFromNow(500));
}

void PC::attackDone() {

}

void PC::bumpActor(BG::Actor* actor) {

}

void PC::bumpWall() {

}

void PC::onDeath() {

}

void PC::startTurn() {
  myTurn = true;
  gm->waitForInput();
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "BaseContext.h"

BaseContext::BaseContext(std::string id, LegendGM* gm, BaseActor* player)
: BG::InputContext(id),
  player(player),
  gm(gm) {}

BaseContext::~BaseContext() {}

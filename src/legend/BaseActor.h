// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef BASEACTOR_H
#define BASEACTOR_H

#include "blackguard/logic/Actor.h"

class LegendGM;

class BaseActor : public BG::Actor {
  public:
    BaseActor(std::string tag, std::string name, bool alive, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, LegendGM* gm);
    virtual ~BaseActor();

  protected:
    LegendGM* gm;

    std::string displayName;
};

#endif // BASEACTOR_H

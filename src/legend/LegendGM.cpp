// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "LegendGM.h"
#include "BaseActor.h"
#include "blackguard/logic/manager/AreaManager.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "../external/easylogging++.h"

LegendGM::LegendGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window)
: BG::GameMaster(entityManager, world, fontManager, log, window) {}

LegendGM::~LegendGM() {}

boost::optional<BaseActor*> LegendGM::getActorInCell(const BG::CellModel* cell, const BG::AreaModel* area) const {
  std::vector<BG::PhysicalEntity*> entities = entityManager->getEntitiesInArea(area);
  std::vector<BG::PhysicalEntity*> actorInCell(entities.size());
  boost::optional<BaseActor*> option;
  auto iter = std::copy_if(entities.begin(), entities.end(), actorInCell.begin(),
                           [cell](BG::PhysicalEntity* entity){return entity->getPosition()->getRow() == cell->getRowPos() &&
                                                             entity->getPosition()->getCol() == cell->getColPos() &&
                                                             entity->getType() == "actor";});
  actorInCell.resize(std::distance(actorInCell.begin(), iter));
  if (!actorInCell.empty()) {
    auto baseActor = dynamic_cast<BaseActor*>(actorInCell.at(0));
    if (baseActor) option = {baseActor};
  }
  return option;
}

void LegendGM::moveActor(BaseActor* actor, const BG::AreaModel* area, BG::Direction direction) {
  auto targetCell = area->getAdjacentCell(*actor->getPosition(), direction);
  if (targetCell) {
    moveActorToCell(actor, targetCell.get());
  } else {
    LOG(ERROR) << "BlackguardGM::moveActor(): Failed to get find target cell";
  }
}

void LegendGM::moveActorToCell(BaseActor* actor, const BG::CellModel* cell) {
  if (cell->getProperty("passable") == "true") {
    auto actorInTargetCell = getActorInCell(cell, actor->getArea().get());
    if (actorInTargetCell) {
      actor->bumpActor(actorInTargetCell.get());
    } else {
      actor->setPosition(BG::Position(cell->getX(), cell->getY()));
      actor->moveDone();
    }
  } else {
    actor->bumpWall();
  }
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef VILLAGEAREA_H
#define VILLAGEAREA_H

#include "LegendArea.h"

namespace BG {
  class RandomGenerator;
}

class VillageArea : public LegendArea {
  public:
    VillageArea(std::string tag, std::string name, BG::RandomGenerator* generator,  int rows, int columns);
    virtual ~VillageArea();

  private:
    void build(BG::RandomGenerator* generator, int rows, int columns);
    void fillWithGrass(int rows, int columns);

    void buildRiver(BG::RandomGenerator* generator);
    BG::Direction chooseUphillDirection(BG::RandomGenerator* generator) const;
    BG::Direction chooseWidthDirection(BG::RandomGenerator* generator, BG::Direction uphillDirection) const;
    BG::Position chooseStartingPosition(BG::RandomGenerator* generator, BG::Direction startingEdge) const;
    BG::Direction chooseRandomDownhillDirection(BG::RandomGenerator* generator, BG::Direction uphillDirection, BG::Direction currentDirection) const;
    BG::Direction getOppositeDirection(BG::Direction direction) const;
    void drawRiverSection(BG::Position position, BG::Direction widthDirection, int width);

    void buildForest(BG::RandomGenerator* generator, int treeDensity, int bushDensity, int mushroomDensity);
    void scatterTile(BG::RandomGenerator* generator, int density, std::string tileId, std::string passable, const std::function <bool (const BG::CellModel*)>& condition);
    bool isNearTree(BG::Position position) const;
    int calculateDensity(int densityFactor) const;
};

#endif // VILLAGEAREA_H

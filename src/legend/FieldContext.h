// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef FIELDCONTEXT_H
#define FIELDCONTEXT_H

#include "BaseContext.h"

class FieldContext : public BaseContext {
  public:
    FieldContext(LegendGM* gm, BaseActor* player);
    virtual ~FieldContext();

    virtual void handleInput(const SDL_Event &event, bool &quit) override;
};

#endif // FIELDCONTEXT_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef LEGENDAREAPANEL_H
#define LEGENDAREAPANEL_H

#include "blackguard/interface/SubPanel.h"

namespace BG {
  class AreaModel;
  class FontManager;
  class TilesetManager;
  class EntityManager;
}

class LegendAreaPanel : public BG::SubPanel {
  public:
    LegendAreaPanel(std::string tag, const BG::AreaModel* areaModel, BG::Coordinates offset, int width, int height, Panel* parent, BG::FontManager* fontManager, BG::TilesetManager* tilesetManager, BG::EntityManager* entityManager);

    virtual void drawSelf() override;

  private:
    static const int TILE_WIDTH = 16;
    static const int TILE_HEIGHT = 16;

    const BG::AreaModel* areaModel;
    BG::FontManager* fontManager;
    BG::TilesetManager* tilesetManager;
    BG::EntityManager* entityManager;
};

#endif // LEGENDAREAPANEL_H

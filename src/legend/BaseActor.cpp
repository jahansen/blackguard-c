// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "BaseActor.h"

BaseActor::BaseActor(std::string tag, std::string name, bool alive, const BG::AreaModel* area, BG::Position position, BG::Faction* faction, const BG::Tile* tile, LegendGM* gm)
: Actor(tag, name, alive, area, position, faction, tile),
  gm(gm) {}

BaseActor::~BaseActor() {}

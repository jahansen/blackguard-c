// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef LEGEND_H
#define LEGEND_H

#include <boost/optional.hpp>
#include "blackguard/core/GameMaster.h"
#include "blackguard/core/Application.h"
#include "blackguard/core/Window.h"
#include "blackguard/logic/TextLog.h"

namespace BG {
  class Tile;
}

class LegendAreaPanel;

class Legend : public BG::Application {
  public:
    Legend(int argc, char** arg);
    virtual ~Legend();

    virtual bool initialise() override;
    virtual void run() override;

  private:
    static const int LegendFPS = 30;

    LegendAreaPanel* areaPanel;
    BG::TextLog mainLog;

    bool loadResources();
    bool buildScreen();
    bool buildGame();

    boost::optional<const BG::Tile*> getTile(const BG::Tileset* tileset, std::string tileTag, BG::ErrorCollection& errors);
};

#endif // LEGEND_H

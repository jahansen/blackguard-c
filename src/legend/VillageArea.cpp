// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "VillageArea.h"
#include "blackguard/core/RandomGenerator.h"
#include "blackguard/logic/CellModel.h"
#include "blackguard/logic/Direction.h"
#include "../external/easylogging++.h"

VillageArea::VillageArea(std::string tag, std::string name, BG::RandomGenerator* generator, int rows, int columns)
: LegendArea(tag, name) {
  build(generator, rows, columns);
}

VillageArea::~VillageArea() {}

void VillageArea::build(BG::RandomGenerator* generator, int rows, int columns) {
  fillWithGrass(rows, columns);
  buildRiver(generator);
  buildForest(generator, 25, 10, 5);
}

void VillageArea::fillWithGrass(int rows, int columns) {
  for (int r = 0; r < rows; ++r) {
    matrix.push_back(BG::CellRow());
    for (int c = 0; c < columns; ++c) {

      auto currentCell = BG::CellModel(BG::Position(c, r));
      currentCell.addProperty("tile", "grass");
      currentCell.addProperty("passable", "true");

      matrix[r].push_back(currentCell);
    }
  }
}

void VillageArea::buildRiver(BG::RandomGenerator* generator) {
  auto uphillDirection = chooseUphillDirection(generator);
  auto widthDirection = chooseWidthDirection(generator, uphillDirection);
  auto currentCell = getCell(chooseStartingPosition(generator, uphillDirection));
  auto currentDirection = getOppositeDirection(uphillDirection);
  while (currentCell) {
    drawRiverSection(currentCell.get()->getPosition(), widthDirection, 6);
    currentDirection = chooseRandomDownhillDirection(generator, uphillDirection, currentDirection);
    currentCell = getAdjacentCell(currentCell.get()->getPosition(), currentDirection);
  }
}

BG::Direction VillageArea::chooseUphillDirection(BG::RandomGenerator* generator) const {
  auto roll = generator->rolldX(4);
  switch (roll) {
    case 1: return BG::Direction::NORTH;
    case 2: return BG::Direction::EAST;
    case 3: return BG::Direction::SOUTH;
    case 4: return BG::Direction::WEST;
    default: return BG::Direction::NEUTRAL;
  }
}

BG::Direction VillageArea::chooseWidthDirection(BG::RandomGenerator* generator, BG::Direction uphillDirection) const {
  auto roll = generator->rolldX(2);
  switch (uphillDirection) {
    case BG::Direction::NORTH:
    case BG::Direction::SOUTH:
      switch (roll) {
        case 1: return BG::Direction::WEST;
        case 2: return BG::Direction::EAST;
      }

    case BG::Direction::EAST:
    case BG::Direction::WEST:
      switch (roll) {
        case 1: return BG::Direction::NORTH;
        case 2: return BG::Direction::SOUTH;
      }

    default:
      LOG(WARNING) << "VillageArea::chooseWidthDirection(): received invalid uphillDirection";
      return BG::Direction::NEUTRAL;
  }
}

BG::Position VillageArea::chooseStartingPosition(BG::RandomGenerator* generator, BG::Direction startingEdge) const {
  switch (startingEdge) {
    case BG::Direction::NORTH: return BG::Position(generator->range(0 + getColCount()/4, getColCount()-1 - getColCount()/4), 0);
    case BG::Direction::EAST:  return BG::Position(getColCount()-1, generator->range(0 + getRowCount()/4, getRowCount()-1 - getRowCount()/4));
    case BG::Direction::SOUTH: return BG::Position(generator->range(0 + getColCount()/4, getColCount()-1 - getColCount()/4), getRowCount()-1);
    case BG::Direction::WEST:  return BG::Position(0, generator->range(0 + getRowCount()/4, getRowCount()-1 - getRowCount()/4));
    default:
      LOG(WARNING) << "VillageArea::chooseStartingPosition(): received non-cardinal direction";
      return BG::Position(0, 0);
  }
}

BG::Direction VillageArea::chooseRandomDownhillDirection(BG::RandomGenerator* generator, BG::Direction uphillDirection, BG::Direction currentDirection) const {

  // Weigh odds in favor of keeping the same direction
  if (generator->percentChance(50)) {
    return currentDirection;
  }

  auto roll = generator->rolldX(3);
  switch (uphillDirection) {
    case BG::Direction::NORTH:
      switch (roll) {
        case 1: return BG::Direction::SOUTH_WEST;
        case 2: return BG::Direction::SOUTH;
        case 3: return BG::Direction::SOUTH_EAST;
      }

    case BG::Direction::EAST:
      switch (roll) {
        case 1: return BG::Direction::NORTH_WEST;
        case 2: return BG::Direction::WEST;
        case 3: return BG::Direction::SOUTH_WEST;
      }

    case BG::Direction::SOUTH:
     switch (roll) {
       case 1: return BG::Direction::NORTH_WEST;
       case 2: return BG::Direction::NORTH;
       case 3: return BG::Direction::NORTH_EAST;
     }

    case BG::Direction::WEST:
     switch (roll) {
       case 1: return BG::Direction::NORTH_EAST;
       case 2: return BG::Direction::EAST;
       case 3: return BG::Direction::SOUTH_EAST;
     }

    default:
      LOG(WARNING) << "VillageArea::chooseRandomDownhillDirection(): received non-cardinal direction";
      return BG::Direction::NEUTRAL;
  }
}

BG::Direction VillageArea::getOppositeDirection(BG::Direction direction) const {
  switch (direction) {
    case BG::Direction::NORTH: return BG::Direction::SOUTH;
    case BG::Direction::EAST: return BG::Direction::WEST;
    case BG::Direction::SOUTH: return BG::Direction::NORTH;
    case BG::Direction::WEST: return BG::Direction::EAST;
    case BG::Direction::NORTH_EAST: return BG::Direction::SOUTH_WEST;
    case BG::Direction::SOUTH_EAST: return BG::Direction::NORTH_WEST;
    case BG::Direction::SOUTH_WEST: return BG::Direction::NORTH_EAST;
    case BG::Direction::NORTH_WEST: return BG::Direction::SOUTH_EAST;
    case BG::Direction::UP: return BG::Direction::UP;
    case BG::Direction::DOWN: return BG::Direction::UP;
    default: return BG::Direction::NEUTRAL;
  }
}

void VillageArea::drawRiverSection(BG::Position position, BG::Direction widthDirection, int width) {
  auto currentPosition = position;
  for (int i = 0; i < width; ++i) {
    modifyCellProperty(currentPosition, "tile", "water");
    modifyCellProperty(currentPosition, "passable", "false");

    auto nextCell = getAdjacentCell(currentPosition, widthDirection);
    if (nextCell) {
      currentPosition = nextCell.get()->getPosition();
    } else {
      break;
    }
  }
}

void VillageArea::buildForest(BG::RandomGenerator* generator, int treeDensity, int bushDensity, int mushroomDensity) {
  scatterTile(generator, treeDensity, "tree", "false", [](const BG::CellModel* cell){
    return cell->getProperty("tile") == "grass";
  });

  scatterTile(generator, bushDensity, "bush", "true", [this](const BG::CellModel* cell){
    return cell->getProperty("tile") == "grass" && isNearTree(cell->getPosition());
  });

  scatterTile(generator, mushroomDensity, "mushrooms", "true", [this](const BG::CellModel* cell){
    return cell->getProperty("tile") == "grass" && isNearTree(cell->getPosition());
  });
}

void VillageArea::scatterTile(BG::RandomGenerator* generator, int density, std::string tileId, std::string passable,
                              const std::function<bool (const BG::CellModel*)>& condition) {
  auto numberOfTiles = calculateDensity(density);

  for (int i = 0; i < numberOfTiles; ++i) {
    auto proposedCell = getRandomCell(generator);

    if (condition(proposedCell)) {
      modifyCellProperty(proposedCell->getPosition(), "tile", tileId);
      modifyCellProperty(proposedCell->getPosition(), "passable", passable);
    }
  }
}

bool VillageArea::isNearTree(BG::Position position) const {
  auto proposedStartRow = position.getRow()-2;
  auto proposedStartCol = position.getCol()-2;
  auto proposedEndRow = position.getRow()+2;
  auto proposedEndCol = position.getCol()+2;

  auto startRow = (proposedStartRow >= 0 ? proposedStartRow : 0);
  auto startCol = (proposedStartCol >= 0 ? proposedStartCol : 0);
  auto endRow = (proposedEndRow <= getRowCount()-1 ? proposedEndRow : getRowCount()-1);
  auto endCol = (proposedEndCol <= getColCount()-1 ? proposedEndCol : getColCount()-1);

  for (int r = startRow; r <= endRow; ++r) {
    for (int c = startCol; c <= endCol; ++c) {
      auto curCell = getCell(BG::Position(c, r));
      if (curCell && curCell.get()->getProperty("tile") == "tree") {
        return true;
      }
    }
  }

  return false;
}

int VillageArea::calculateDensity(int densityFactor) const {
  return getRowCount() * getColCount() * densityFactor / 100.0;
}

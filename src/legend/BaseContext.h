// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef BASECONTEXT_H
#define BASECONTEXT_H

#include <blackguard/core/InputContext.h>

class LegendGM;
class BaseActor;

class BaseContext : public BG::InputContext {
  public:
    BaseContext(std::string id, LegendGM* gm, BaseActor* player);
    virtual ~BaseContext();

  protected:
    BaseActor* player;
    LegendGM* gm;
};

#endif // BASECONTEXT_H

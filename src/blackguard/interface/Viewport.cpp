// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Viewport.h"
#include "../external/easylogging++.h"

namespace BG {

Viewport::Viewport(const AreaModel* areaModel, const Position centre, int width, int height)
: areaModel(areaModel),
  width(width),
  height(height) {

  if (width < 1) LOG(FATAL) << "Viewport width must be at least 1";
  if (height < 1) LOG(FATAL) << "Viewport height must be at least 1";

  // Find upper left corner (without considering area borders)
  origin = BG::Position(centre.getX() - width / 2, centre.getY() - height / 2);

  // Shift viewport corner over, taking into account area borders
  if (origin.getRow() < 0) origin.setY(0);
  else if ((origin.getRow() + height) > areaModel->getRowCount()) origin.setY(areaModel->getRowCount() - height);
  if (origin.getCol() < 0) origin.setX(0);
  else if ((origin.getCol() + width) > areaModel->getColCount()) origin.setX(areaModel->getColCount() - width);
}

Position Viewport::getOffset() const {
  return origin;
}

bool Viewport::positionIsWithinViewport(Position position) const {
  return position.isBetweenInclusive(origin, Position(origin.getX() + (width-1), origin.getY() + (height-1)));
}

void Viewport::forEachVisibleCell(std::function<void (int, int, Position)> f) {
  for (int r = origin.getRow(); r < (origin.getRow() + height); ++r) {
    for (int c = origin.getCol(); c < (origin.getCol() + width); ++c) {
      if (areaModel->cellExists(Position(r, c))) {
        f(r, c, origin);
      }
    }
  }
}


} // BG

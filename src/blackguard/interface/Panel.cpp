// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Panel.h"
#include "blackguard/interface/SubPanel.h"

namespace BG {

Panel::Panel(std::string tag, int width, int height)
: tag(tag),
  dimensions(SDL_Rect{0, 0, width, height}) {}

Panel::~Panel() {
  for (auto child : children) {
    delete child;
  }
  children.clear();
}

boost::optional<Panel*> Panel::getChild(std::string panelTag) const {
  boost::optional<Panel*> option;
  auto p = std::find_if(children.begin(), children.end(), [panelTag](SubPanel* child)->bool{
    return child->getTag() == panelTag;
  });

  if (p != children.end()) {
    option = {*p};
  }
  return option;
}

void Panel::removeChild(std::string panelTag) {
  for (int i = 0; i < (int)children.size(); ++i) {
    auto child = children.at(i);
    if (child->getTag() == panelTag) {
      delete child;
      children.erase(children.begin()+i);
      return;
    }
  }
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TEXTLOGPANEL_H
#define TEXTLOGPANEL_H

#include <SDL2/SDL_ttf.h>
#include "blackguard/interface/SubPanel.h"
#include "blackguard/logic/TextLog.h"

namespace BG {

class Font;
class TextLog;

class TextLogPanel : public BG::SubPanel {
  public:
    TextLogPanel(std::string tag, const Font* font, Coordinates offset, int width, int height, Panel* parent, TextLog* log);
    virtual ~TextLogPanel();

    virtual void drawSelf() override;
    void drawText(int x, int y, TextLogEntry entry);
    void addEntry(TextLogEntry entry) {log->addEntry(entry);}
    void scrollUp() {log->scrollUp();}
    void scrollDown() {log->scrollDown();}
    inline int getScrollPosition() const {return log->getScrollPosition();}

  private:
    const Font* font;
    TextLog* log;
    int lineHeight;

    std::vector<TextLogEntry> getDisplayLines(std::vector<TextLogEntry> lines) const;
    int getLineWidth(std::string text) const;
    std::vector<TextLogEntry> splitMultilineEntry(TextLogEntry entry, int maxWidth) const;
    std::string getOneLine(std::vector<std::string> words, int maxWidth) const;
    std::vector<std::string> splitStringIntoWords(std::string line) const;
};

} // BG

#endif // TEXTLOGPANEL_H

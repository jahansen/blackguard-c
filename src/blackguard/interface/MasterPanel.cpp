// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "MasterPanel.h"
#include "blackguard/interface/SubPanel.h"

namespace BG {

MasterPanel::MasterPanel(std::string tag, int width, int height, SDL_Window* screen, SDL_Renderer* renderer)
: Panel(tag, width, height),
  screen(screen),
  renderer(renderer) {}

MasterPanel::~MasterPanel() {
}

void MasterPanel::addChild(SubPanel* childPanel) {
  children.push_back(childPanel);
}

void MasterPanel::draw() {
  for (SubPanel* child : children) {
    child->draw();
  }
}

} // BG

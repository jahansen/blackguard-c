// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include <algorithm>
#include <sstream>
#include "../external/easylogging++.h"
#include "TextLogPanel.h"
#include "blackguard/interface/Font.h"
#include "blackguard/interface/TextPanel.h"

namespace BG {

TextLogPanel::TextLogPanel(std::string tag, const Font* font, Coordinates offset, int width, int height, Panel* parent, TextLog* log)
: SubPanel(tag, offset, width, height, parent),
  font(font),
  log(log),
  lineHeight(TTF_FontLineSkip(const_cast<Font*>(font)->getFont())) {}

TextLogPanel::~TextLogPanel() {
}

void TextLogPanel::drawSelf() {
  int linesToDisplay = dimensions.h / lineHeight;
  std::vector<TextLogEntry> lines = log->getLatest(linesToDisplay + (getScrollPosition() - 1));
  std::reverse(lines.begin(), lines.end());
  std::vector<TextLogEntry> displayLines = getDisplayLines(lines);

  int firstLineY = dimensions.y + dimensions.h - lineHeight;
  displayLines = std::vector<TextLogEntry>(displayLines.begin()+getScrollPosition()-1, displayLines.end());
  int finalLineCount = linesToDisplay < (int)displayLines.size() ? linesToDisplay : displayLines.size();
  for (int i = 1; i <= finalLineCount; ++i) {
    drawText(dimensions.x, firstLineY - i * lineHeight, displayLines.at(i-1));
  }
}

void TextLogPanel::drawText(int x, int y, TextLogEntry entry) {
  TextPanel textPanel = TextPanel("text", entry.getText(), font, Coordinates(x, y), x, lineHeight, this, entry.getColour());
  textPanel.drawSelf();
}

std::vector<TextLogEntry> TextLogPanel::getDisplayLines(std::vector<TextLogEntry> lines) const {

  int maxWidth = dimensions.w;
  std::vector<TextLogEntry> displayLines;

  for (auto line : lines) {
    if (getLineWidth(line.getText()) > maxWidth) {
      std::vector<TextLogEntry> multilineEntry = splitMultilineEntry(line, maxWidth);
      std::reverse(multilineEntry.begin(), multilineEntry.end());
      for (auto singleLine : multilineEntry) {
        displayLines.push_back(singleLine);
      }
    } else {
      displayLines.push_back(line);
    }
  }

  return displayLines;
}

int TextLogPanel::getLineWidth(std::string text) const {
  int textWidth, textHeight;
  TTF_SizeUTF8(const_cast<Font*>(font)->getFont(), text.c_str(), &textWidth, &textHeight);
  return textWidth;
}

std::vector<TextLogEntry> TextLogPanel::splitMultilineEntry(TextLogEntry entry, int maxWidth) const {
  std::vector<TextLogEntry> multilineEntry;
  std::string remainingEntry = entry.getText();
  std::string entrySoFar = "";
  while (entrySoFar.length() < entry.getText().length()) {
    std::vector<std::string> words = splitStringIntoWords(remainingEntry);
    std::string nextLine = getOneLine(words, maxWidth);
    entrySoFar += nextLine;
    remainingEntry = remainingEntry.erase(0, nextLine.length());
    multilineEntry.push_back(TextLogEntry(nextLine, entry.getColour()));
  }
  return multilineEntry;
}

std::string TextLogPanel::getOneLine(std::vector<std::string> words, int maxWidth) const {
  std::string line;
  for (auto word : words) {
    if (getLineWidth(line + word) > maxWidth) {
      return line;
    } else {
      line += (word + " ");
    }
  }
  return line;
}

std::vector<std::string> TextLogPanel::splitStringIntoWords(std::string line) const {
  std::vector<std::string> words;
  std::stringstream stream(line);
  std::string word;
  while (std::getline(stream, word, ' ')) {
    words.push_back(word);
  }
  return words;
}

} // BG

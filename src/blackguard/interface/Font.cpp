// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Font.h"
#include "blackguard/core/File.h"
#include "blackguard/core/ErrorCollection.h"
#include "../external/easylogging++.h"

namespace BG {

Font::Font(std::string descriptorFileName)
: Entity(),
  descriptorFileName(descriptorFileName) {}

Font::~Font() {
}

Font* Font::getCopy() const {
  auto font = new Font(fileName);
  ErrorCollection errors;
  font->load(errors);
  return font;
}

bool Font::load(ErrorCollection& errors) {

  File fontFile(descriptorFileName);
  if (!fontFile.open()) {
    errors.addError(("Unable to open font descriptor file '" + descriptorFileName + "'."));
    return false;
  }

  rapidjson::Document json;
  std::string fileData = fontFile.getAll();
  json.Parse<0>(fileData.data());
  fontFile.close();

  errors = validateFontJSON(json);
  if (!errors.isEmpty()) return false;

  tag = json["tag"].GetString();
  name = json["name"].GetString(); fileName = json["filename"].GetString();
  fontSize = json["fontSize"].GetInt();

  TTF_Font* fontPtr = TTF_OpenFont(fileName.data(), fontSize);
  if (fontPtr == nullptr) {
    errors.addError("Cannot open font file '" + fileName + "', " + TTF_GetError());
    return false;
  }
  font = std::unique_ptr<TTF_Font, FontDeleter>(fontPtr);
  return true;
}

ErrorCollection Font::validateFontJSON(rapidjson::Document& json) const {
  ErrorCollection errors;
  if (!json.IsObject()) {
    errors.addError("Unable to parse JSON in file: " + descriptorFileName);
    return errors;
  }
  if (!json.HasMember("tag") || !json["tag"].IsString()) {
    errors.addError(buildJSONErrorString("tag"));
  }
  if (!json.HasMember("name") || !json["name"].IsString()) {
    errors.addError(buildJSONErrorString("name"));
  }
  if (!json.HasMember("filename") || !json["filename"].IsString()) {
    errors.addError(buildJSONErrorString("filename"));
  }
  if (!json.HasMember("fontSize") || !json["fontSize"].IsInt()) {
    errors.addError(buildJSONErrorString("fontSize"));
  }

  return errors;
}

std::string Font::buildJSONErrorString(std::string property) const {
  return "Font file '" + descriptorFileName + "' does not contain a valid '" + property + "' property.";
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "MenuPanel.h"
#include "../external/easylogging++.h"

namespace BG {

MenuPanel::MenuPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent)
: ListPanel(tag, offset, width, height, parent),
  selectedIndex(-1) {}

MenuPanel::~MenuPanel() {}

boost::optional<ListPanelItem*> MenuPanel::getSelectedItem() const {
  boost::optional<ListPanelItem*> option;
  if (indexIsValid(selectedIndex)) option = items.at(selectedIndex).get();
  return option;
}

void MenuPanel::selectNext() {
  if ((int)items.size() == 0) {
    selectedIndex = -1;
  } else {
    auto newIndex = (selectedIndex + 1) >= (int)items.size() ? 0 : selectedIndex + 1;
    selectIndex(newIndex);
  }

  updateScrollPosition();
}

void MenuPanel::selectPrevious() {
  if ((int)items.size() <= 1) {
    selectedIndex = 0;
  } else {
    auto newIndex = selectedIndex <= 0 ? (int)items.size() - 1 : selectedIndex - 1;
    selectIndex(newIndex);
  }

  updateScrollPosition();
}

void MenuPanel::selectIndex(int index) {
  if (indexIsValid(index)) {
    if (indexIsValid(selectedIndex)) {
      items.at(selectedIndex)->unselect();
    }
    selectedIndex = index;
    items.at(selectedIndex)->select();
  }
}

bool MenuPanel::indexIsValid(int index) const {
  return (0 <= index && index < (int)items.size());
}

void MenuPanel::updateScrollPosition() {
  auto maxItems = getMaxViewableItems();
  auto maxScrollPosition = (int)items.size() - maxItems;

  scrollPosition = (selectedIndex + 1) - maxItems;
  if (scrollPosition < 0) {
    scrollPosition = 0;
  } else if (scrollPosition > maxScrollPosition) {
    scrollPosition = maxScrollPosition;
  }
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef IMAGE_H
#define IMAGE_H

#include <SDL2/SDL.h>
#include <string>

namespace BG {

class Image {
  public:
    Image(std::string filename);
    virtual ~Image();

    bool load(SDL_Renderer* renderer);
    inline bool isLoaded() const {return image == nullptr ? false : true;}
    inline SDL_Texture* getTexture() const {return image;}

  private:
    std::string filename;
    SDL_Texture* image;
};

} // BG

#endif // IMAGE_H

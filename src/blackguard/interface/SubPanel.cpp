// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "SubPanel.h"

namespace BG {

SubPanel::SubPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent)
: Panel(tag, width, height),
  parent(parent),
  offset(offset) {}

SubPanel::~SubPanel() {}

void SubPanel::addChild(SubPanel* childPanel) {
  children.push_back(childPanel);
}

void SubPanel::draw() {
  drawSelf();

  for (SubPanel* child : children) {
    child->draw();
  }
}

SDL_Rect SubPanel::getAbsoluteDimensions() const {
  auto absOffset = getAbsoluteOffset();
  return SDL_Rect{absOffset.getX(), absOffset.getY(), getWidth(), getHeight()};
}

} // BG

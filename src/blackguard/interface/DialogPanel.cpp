// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "DialogPanel.h"

namespace BG {

DialogPanel::DialogPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent)
: SubPanel(tag, offset, width, height, parent) {}

DialogPanel::~DialogPanel() {}

void DialogPanel::drawSelf() {
  auto absDimensions = getAbsoluteDimensions();
  SDL_RenderFillRect(getRenderer(), &absDimensions);
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef CONTAINERPANEL_H
#define CONTAINERPANEL_H

#include "blackguard/interface/SubPanel.h"

namespace BG {

class ContainerPanel : public SubPanel {
  public:
    ContainerPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent);
    virtual ~ContainerPanel();

    virtual void drawSelf() override;
};

} // BG

#endif // CONTAINERPANEL_H

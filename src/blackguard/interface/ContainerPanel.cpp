// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ContainerPanel.h"

namespace BG {

ContainerPanel::ContainerPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent)
: SubPanel(tag, offset, width, height, parent) {}

ContainerPanel::~ContainerPanel() {}

void ContainerPanel::drawSelf() {
  auto absDimensions = getAbsoluteDimensions();
  SDL_SetRenderDrawColor(getRenderer(), 255, 255, 255, 0);
  SDL_RenderDrawRect(getRenderer(), &absDimensions);
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Image.h"
#include <SDL2/SDL_image.h>
#include "../external/easylogging++.h"

namespace BG {

Image::Image(std::string filename)
: filename(filename),
  image(nullptr) {}

Image::~Image() {
  SDL_DestroyTexture(image);
  image = nullptr;
}

bool Image::load(SDL_Renderer* renderer) {
  image = IMG_LoadTexture(renderer, filename.data());
  if (image != nullptr) {
    return true;
  } else {
    LOG(ERROR) << "Failed to load image from file: " << filename << " - " << IMG_GetError();
    return false;
  }
}

} // BG

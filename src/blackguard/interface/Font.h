// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef FONT_H
#define FONT_H

#include <SDL2/SDL_ttf.h>
#include <string>
#include <memory>
#include "blackguard/logic/Entity.h"
#include "rapidjson/document.h"
namespace BG {

struct FontDeleter {
  void operator()(TTF_Font* ptr) {
    TTF_CloseFont(ptr);
  }
};

class ErrorCollection;

class Font : public Entity {
  public:
    Font(std::string descriptorFileName);
    virtual ~Font();

    inline virtual std::string getType() const {return "font";}
    virtual Font* getCopy() const;

    bool load(ErrorCollection& errors);
    inline TTF_Font* getFont() const {return font.get();}

  protected:
    std::string id;
    std::string descriptorFileName;
    std::string fileName;
    int fontSize;
    std::unique_ptr<TTF_Font, FontDeleter> font;

    ErrorCollection validateFontJSON(rapidjson::Document& json) const;
    std::string buildJSONErrorString(std::string property) const;
};

} // BG

#endif // FONT_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "TilesetManager.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/logic/Tileset.h"
#include "../external/easylogging++.h"

namespace BG {

TilesetManager::TilesetManager() {
}

TilesetManager::~TilesetManager() {
}

void TilesetManager::createTileset(std::string filename, SDL_Renderer* renderer) {
  ErrorCollection errors;
  createTilesetWithErrors(filename, renderer, errors);
  if (!errors.isEmpty()) {
    errors.printToLog();
    LOG(FATAL) << "Failed to create tileset from file: '" << filename << "'";
  }
}

void TilesetManager::createTilesetWithErrors(std::string filename, SDL_Renderer* renderer, ErrorCollection& errors) {
  auto tileset = std::unique_ptr<Tileset>(new Tileset(filename));
  if (tileset->loadWithErrors(renderer, errors)) {
    add(std::move(tileset));
  }
}

const Tileset* TilesetManager::getTileset(std::string tag) const {
  auto tilesetOpt = getTilesetOpt(tag);
  if (!tilesetOpt) {
    LOG(FATAL) << "Cannot find tileset with tag '" << tag << "'";
    return nullptr;
  } else {
    return tilesetOpt.get();
  }
}

boost::optional<const Tileset*> TilesetManager::getTilesetOpt(std::string tag) const {
  boost::optional<const Tileset*> option;
  if (auto tileset = dynamic_cast<const Tileset*>(get(tag))) {
    option = tileset;
  }
  return option;
}

} // BG

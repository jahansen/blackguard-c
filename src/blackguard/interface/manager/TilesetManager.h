// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TILESETMANAGER_H
#define TILESETMANAGER_H

#include <SDL2/SDL.h>
#include <boost/optional.hpp>
#include "blackguard/core/AbstractManager.h"

namespace BG {

class ErrorCollection;
class Tileset;

class TilesetManager : public AbstractManager {
  public:
    TilesetManager();
    virtual ~TilesetManager();

    void createTileset(std::string fileName, SDL_Renderer* renderer);
    void createTilesetWithErrors(std::string filename, SDL_Renderer* renderer, ErrorCollection& errors);

    const Tileset* getTileset(std::string tag) const;
    boost::optional<const Tileset*> getTilesetOpt(std::string tag) const;
};

} // BG

#endif // TILESETMANAGER_H

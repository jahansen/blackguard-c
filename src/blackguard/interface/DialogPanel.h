// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef DIALOGPANEL_H
#define DIALOGPANEL_H

#include <SDL2/SDL.h>
#include "blackguard/interface/SubPanel.h"

namespace BG {

class DialogPanel : public BG::SubPanel {
  public:
    DialogPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent);
    virtual ~DialogPanel();

    virtual void drawSelf() override;

  private:

};

} // BG

#endif // DIALOGPANEL_H

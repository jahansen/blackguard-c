// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef SUBPANEL_H
#define SUBPANEL_H

#include <SDL2/SDL.h>
#include "blackguard/interface/Panel.h"
#include "blackguard/logic/Coordinates.h"

namespace BG {

class SubPanel : public Panel {
  public:
    SubPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent);
    virtual ~SubPanel();

    virtual void draw() override;
    inline virtual SDL_Window* getScreen() const override {return parent->getScreen();}
    inline virtual SDL_Renderer* getRenderer() const override {return parent->getRenderer();}
    inline virtual Coordinates getAbsoluteOffset() const override {return parent->getAbsoluteOffset() + offset;}
    SDL_Rect getAbsoluteDimensions() const;

    virtual void addChild(SubPanel* childPanel) override;
    inline void setXOffset(int x) {offset.setX(x);}
    inline void setYOffset(int y) {offset.setY(y);}

  protected:
    Panel* parent;
    Coordinates offset;

    virtual void drawSelf() =0;
};

} // BG

#endif // SUBPANEL_H

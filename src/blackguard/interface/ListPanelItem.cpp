// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ListPanelItem.h"

namespace BG {

ListPanelItem::ListPanelItem(int width, int height, Panel* parent)
: SubPanel("list-panel-item", Coordinates(0, 0), width, height, parent),
  selected(false) {}

ListPanelItem::~ListPanelItem() {}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "TextPanel.h"
#include "blackguard/interface/Font.h"

namespace BG {

TextPanel::TextPanel(std::string tag, std::string text, const Font* font, Coordinates offset, int width, int height, Panel* parent, Colour colour)
: SubPanel(tag, offset, width, height, parent),
  text(text),
  font(font),
  colour(colour) {}

TextPanel::~TextPanel() {
}

void TextPanel::drawSelf() {
  SDL_Surface* textSurface = TTF_RenderUTF8_Blended(const_cast<Font*>(font)->getFont(), text.c_str(), colour.getSDLColour());
  SDL_Texture* textTexture = SDL_CreateTextureFromSurface(parent->getRenderer(), textSurface);
  SDL_FreeSurface(textSurface);

  SDL_Rect textDimensions = getTextDimensions();

  SDL_RenderCopy(parent->getRenderer(), textTexture, nullptr, &textDimensions);

  SDL_DestroyTexture(textTexture);
  textTexture = nullptr;
}

SDL_Rect TextPanel::getTextDimensions() const {
  int textWidth, textHeight;
  TTF_SizeUTF8(const_cast<Font*>(font)->getFont(), text.c_str(), &textWidth, &textHeight);

  auto absOffset = getAbsoluteOffset();
  SDL_Rect textDimensions = {absOffset.getX(), absOffset.getY(), textWidth, textHeight};
  return textDimensions;
}

}

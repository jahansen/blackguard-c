// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef VIEWPORT_H
#define VIEWPORT_H

#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/Position.h"
#include <functional>

namespace BG {

class Viewport {
  public:
    Viewport(const AreaModel* areaModel, const Position centre, int width, int height);

    Position getOffset() const;
    bool positionIsWithinViewport(Position position) const;

    void forEachVisibleCell(std::function<void (int, int, Position)> f);

  private:
    const AreaModel* areaModel;
    const int width;
    const int height;

    Position origin;
};

} // BG

#endif // VIEWPORT_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PANEL_H
#define PANEL_H

#include <SDL2/SDL.h>
#include <vector>
#include <string>
#include <boost/optional.hpp>
#include "blackguard/logic/Coordinates.h"

namespace BG {

class SubPanel;

class Panel {
  public:
    Panel(std::string tag, int width, int height);
    virtual ~Panel();

    inline std::string getTag() const {return tag;}
    inline int getXPos() const {return dimensions.x;}
    inline int getYPos() const {return dimensions.y;}
    inline int getWidth() const {return dimensions.w;}
    inline int getHeight() const {return dimensions.h;}
    inline virtual Coordinates getAbsoluteOffset() const {return Coordinates(0, 0);}
    boost::optional<Panel*> getChild(std::string panelTag) const;
    virtual SDL_Window* getScreen() const =0;
    virtual SDL_Renderer* getRenderer() const =0;

    virtual void draw() =0;
    virtual void addChild(SubPanel* childPanel) =0;
    inline void setXPos(int x) {dimensions.x = x;}
    inline void setYPos(int y) {dimensions.y = y;}
    void removeChild(std::string panelTag);

  protected:
    std::string tag;
    SDL_Rect dimensions;
    std::vector<SubPanel*> children;
};

} // BG

#endif // PANEL_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TEXTPANEL_H
#define TEXTPANEL_H

#include <SDL2/SDL.h>
#include <string>
#include "blackguard/interface/SubPanel.h"
#include "blackguard/interface/Colour.h"

namespace BG {

class Font;

class TextPanel : public BG::SubPanel {
  public:
    TextPanel(std::string tag, std::string text, const Font* font, Coordinates offset, int width, int height, Panel* parent, Colour colour);
    virtual ~TextPanel();

    virtual void drawSelf() override;

  private:
    std::string text;
    const Font* font;
    Colour colour;

    SDL_Rect getTextDimensions() const;
};

} //BG

#endif // TEXTPANEL_H

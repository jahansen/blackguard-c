// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef MASTERPANEL_H
#define MASTERPANEL_H

#include <vector>
#include <SDL2/SDL.h>
#include "blackguard/interface/Panel.h"

namespace BG {

class SubPanel;

class MasterPanel : public Panel {
  public:
    MasterPanel(std::string tag, int width, int height, SDL_Window* screen, SDL_Renderer* renderer);
    virtual ~MasterPanel();

    virtual void draw() override;
    inline virtual SDL_Window* getScreen() const override {return screen;}
    inline virtual SDL_Renderer* getRenderer() const override {return renderer;}
    virtual void addChild(SubPanel* childPanel) override;

  protected:
    SDL_Window* screen;
    SDL_Renderer* renderer;
};

} // BG

#endif // MASTERPANEL_H

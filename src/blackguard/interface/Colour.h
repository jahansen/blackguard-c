// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef COLOUR_H
#define COLOUR_H

#include <SDL2/SDL.h>

namespace BG {

enum ColourId {RED, BLUE, GREEN, WHITE, BLACK, YELLOW, MAGENTA, CYAN};

class Colour
{
  public:
    Colour();
    Colour(ColourId colour);
    virtual ~Colour();

    inline SDL_Colour getSDLColour() const {return sdlColour;}

  private:
    SDL_Color sdlColour;
};

bool operator==(const Colour& lhs, const Colour& rhs);

} // BG

#endif // COLOUR_H

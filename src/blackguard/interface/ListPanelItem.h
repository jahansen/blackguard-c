// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef LISTPANELITEM_H
#define LISTPANELITEM_H

#include "blackguard/interface/SubPanel.h"

namespace BG {

class ListPanelItem : public BG::SubPanel {
  public:
    ListPanelItem(int width, int height, Panel* parent);
    virtual ~ListPanelItem();

    inline void select() {selected = true;}
    inline void unselect() {selected = false;}

  protected:
    bool selected;
};

} // BG

#endif // LISTPANELITEM_H

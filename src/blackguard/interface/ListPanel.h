// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef LISTPANEL_H
#define LISTPANEL_H

#include <vector>
#include <memory>
#include <algorithm>
#include "blackguard/interface/SubPanel.h"
#include "blackguard/interface/ListPanelItem.h"

namespace BG {

class ListPanel : public BG::SubPanel {
  public:
    ListPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent);
    virtual ~ListPanel();

    virtual void drawSelf() override;

    void addItem(std::unique_ptr<ListPanelItem> item);
    inline void scrollDown() {++scrollPosition;}
    inline void scrollUp() {if (scrollPosition > 0) --scrollPosition;}

    std::vector<const ListPanelItem*> getItemPanels() const;

  protected:
    std::vector<std::unique_ptr<ListPanelItem>> items;
    int scrollPosition;

    int getMaxViewableItems() const;
};

} // BG

#endif // LISTPANEL_H

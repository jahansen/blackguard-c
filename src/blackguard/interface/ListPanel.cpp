// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ListPanel.h"

namespace BG {

ListPanel::ListPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent)
: SubPanel(tag, offset, width, height, parent),
  scrollPosition(0) {}

ListPanel::~ListPanel() {}

void ListPanel::drawSelf() {
  auto itemLimit = std::min((int)items.size(), getMaxViewableItems());
  for (int i = scrollPosition; i < itemLimit + scrollPosition; ++i) {
    auto item = items.at(i).get();
    item->setYOffset((i - scrollPosition) * item->getHeight());
    items.at(i)->draw();
  }
}

void ListPanel::addItem(std::unique_ptr<ListPanelItem> item) {
  items.push_back(std::move(item));
}

std::vector<const ListPanelItem*> ListPanel::getItemPanels() const {
  std::vector<const ListPanelItem*> itemPanels;
  for (int i = 0; i < (int)items.size(); ++i) {
    itemPanels.push_back(items.at(i).get());
  }
  return itemPanels;
}

int ListPanel::getMaxViewableItems() const {
  if (items.empty()) {
    return 0;
  } else {
    return getHeight() / items.at(0)->getHeight();
  }
}

} // BG

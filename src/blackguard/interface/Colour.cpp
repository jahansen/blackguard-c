// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Colour.h"

namespace BG {

Colour::Colour() {
  sdlColour = {255, 255, 255, 0};
}

Colour::Colour(ColourId colour) {
  switch (colour) {
    case RED:
      sdlColour = {255, 0, 0, 0};
      break;

    case GREEN:
      sdlColour = {0, 255, 0, 0};
      break;

    case BLUE:
      sdlColour = {0, 0, 255, 0};
      break;

    case WHITE:
      sdlColour = {255, 255, 255, 0};
      break;

    case BLACK:
      sdlColour = {0, 0, 0, 0};
      break;

    case YELLOW:
      sdlColour = {255, 255, 0, 0};
      break;

    case MAGENTA:
      sdlColour = {255, 0, 255, 0};
      break;

    case CYAN:
      sdlColour = {0, 255, 255, 0};
      break;

    default:
      sdlColour = {255, 255, 255, 0};
  }
}

Colour::~Colour() {
}

bool operator==(const Colour& lhs, const Colour& rhs) {
  SDL_Colour leftColour = lhs.getSDLColour();
  SDL_Colour rightColour = rhs.getSDLColour();
  return (leftColour.r == rightColour.r &&
          leftColour.g == rightColour.g &&
          leftColour.b == rightColour.b &&
          leftColour.a == rightColour.a);
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef MENUPANEL_H
#define MENUPANEL_H

#include <boost/optional.hpp>
#include "blackguard/interface/ListPanel.h"

namespace BG {

class MenuPanel : public BG::ListPanel {
  public:
    MenuPanel(std::string tag, Coordinates offset, int width, int height, Panel* parent);
    virtual ~MenuPanel();

    inline int getSelectedIndex() const {return selectedIndex;}

    boost::optional<ListPanelItem*> getSelectedItem() const;

    void selectNext();
    void selectPrevious();
    void selectIndex(int index);

  private:
    int selectedIndex;

    bool indexIsValid(int index) const;
    void updateScrollPosition();
};

} // BG

#endif // MENUPANEL_H

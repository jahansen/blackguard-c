// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef RESOURCEPOOL_H
#define RESOURCEPOOL_H

#include <SDL2/SDL.h>
#include <boost/optional.hpp>
#include "blackguard/core/AbstractManager.h"

namespace BG {

class ErrorCollection;
class Font;

class FontManager : public AbstractManager {
  public:
    FontManager();
    virtual ~FontManager();

    void createFont(std::string filename);
    void createFontWithErrors(std::string filename, ErrorCollection& errors);
    boost::optional<const Font*> getFont(int id) const;
    boost::optional<const Font*> getFont(std::string tag) const;
};

} // BG

#endif // RESOURCEPOOL_H

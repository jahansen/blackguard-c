// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ERRORCOLLECTION_H
#define ERRORCOLLECTION_H

#include <vector>
#include <string>

namespace BG {

class ErrorCollection
{
  public:
    ErrorCollection();
    virtual ~ErrorCollection();

    void addError(std::string error) {errors.push_back(error);}
    bool isEmpty() const {return errors.empty();}
    void printToLog() const;

  private:
    std::vector<std::string> errors;
};

} // BG

#endif // ERRORCOLLECTION_H

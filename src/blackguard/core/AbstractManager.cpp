// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AbstractManager.h"
#include "../external/easylogging++.h"

namespace BG {

AbstractManager::AbstractManager()
: nextId(0) {}

AbstractManager::~AbstractManager() {
}

void AbstractManager::add(std::unique_ptr<Entity> entity) {
  if (get(entity->getTag()) == nullptr) {
    entity->setId(nextId);
    entities.push_back(std::move(entity));
    ++nextId;
  } else {
    LOG(ERROR) << "AbstractManager::add(): cannot add entity with duplicate tag '" << entity->getTag() << "'";
  }
}

Entity* AbstractManager::get(int id) const {
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (entities.at(i)->getId() == id) {
      return entities.at(i).get();
    }
  }
  return nullptr;
}

Entity* AbstractManager::get(std::string tag) const {
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (entities.at(i)->getTag() == tag) {
      return entities.at(i).get();
    }
  }
  return nullptr;
}

void AbstractManager::erase(Entity* entity) {
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (entities.at(i)->getId() == entity->getId()) {
      entities.erase(entities.begin()+i);
      return;
    }
  }
  LOG(ERROR) << "AbstractManager::erase(): Unable to locate entity '" << entity->getId() << ":" << entity->getTag() << "'";
}

}

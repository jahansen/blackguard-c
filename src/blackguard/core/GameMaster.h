// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef GAMEMASTER_H
#define GAMEMASTER_H

#include <vector>
#include <memory>
#include <boost/optional.hpp>
#include "SDL2/SDL.h"
#include "blackguard/core/InputContext.h"
#include "blackguard/logic/ActionQueue.h"
#include "blackguard/logic/Calendar.h"
#include "blackguard/core/RandomGenerator.h"
#include "blackguard/logic/Pathfinder.h"
#include "blackguard/logic/Direction.h"


namespace BG {

class EntityManager;
class AreaManager;
class FontManager;
class TextLog;
class PhysicalEntity;
class Window;
class Panel;

class GameMaster {
  public:
    GameMaster(EntityManager* entityManager, AreaManager* world, FontManager* fontManager, TextLog* log, Window* window);

    Calendar* getWorldCalendar() {return &worldCalendar;}
    RandomGenerator* getGenerator() {return &generator;}
    Panel* getMasterPanel() const;

    void addContext(std::unique_ptr<InputContext> context);
    void setContext(std::string contextId);
    virtual void handleInput(bool& quit);
    virtual void updateWorld();
    virtual void scheduleInitialTurns();
    virtual void scheduleNextTurn(Actor* actor, long time);
    virtual bool cellIsPassable(const CellModel* cell) const =0;
    inline void quit() {receivedQuitCommand = true;}

    void removeEntity(PhysicalEntity* entity);

    void waitForInput() {waitingForInput = true;}
    void stopWaitingForInput() {waitingForInput = false;}

    // Game inspectors
    virtual boost::optional<Actor*> getActorInCell(const CellModel* cell, const AreaModel* area) const;

    // Game mutators
    virtual void moveActor(Actor* actor, const AreaModel* area, Direction direction);
    virtual void moveActorToCell(Actor* actor, const CellModel* cell);
    virtual void moveActorToArea(Actor* actor, const AreaModel* area, const CellModel* cell);

  protected:
    SDL_Event event;
    EntityManager* entityManager;
    AreaManager* world;
    FontManager* fontManager;
    TextLog* log;
    Window* window;
    std::vector<std::unique_ptr<InputContext>> contexts;
    boost::optional<InputContext*> currentContext;
    ActionQueue actionQueue;
    Calendar worldCalendar;
    RandomGenerator generator;
    Pathfinder pathfinder;

    bool waitingForInput;
    bool receivedQuitCommand;
};

} // BG

#endif // GAMEMASTER_H

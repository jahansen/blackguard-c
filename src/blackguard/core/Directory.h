// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <string>
#include <vector>
#include <regex>

namespace BG {

class Directory {
  public:
    Directory();
    virtual ~Directory();

    /**
     * @brief getFileNames returns a list of the file names in the given directory, not including subdirectories.
     * @param directoryPath The directory to list the files of.
     * @return A vector<string> containing the file names in the directory.
     */
    static std::vector<std::string> getFileNames(std::string directoryPath);

    /**
     * @brief getFileNames returns a list of the file names in the given directory that match the given regex, not including subdirectories.
     * @param directoryPath The directory to list the files of.
     * @param regex The regex to match file names on.
     * @return A vector<string> containing the file names in the directory matching the regex.
     */
    static std::vector<std::string> getFileNames(std::string directoryPath, std::regex regex);
};

}

#endif // DIRECTORY_H

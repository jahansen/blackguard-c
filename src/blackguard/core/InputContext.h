// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef INPUTCONTEXT_H
#define INPUTCONTEXT_H

#include "SDL2/SDL.h"
#include <string>

namespace BG {

class GameMaster;

class InputContext {
  public:
    InputContext(std::string id);

    inline std::string getId() const {return id;}

    virtual void handleInput(const SDL_Event& event, bool& quit) =0;

  protected:
    std::string id;
};

} // BG

#endif // INPUTCONTEXT_H

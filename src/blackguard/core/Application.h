// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef APPLICATION_H
#define APPLICATION_H

#include <SDL2/SDL.h>
#include <memory>
#include "blackguard/core/Timer.h"
#include "blackguard/logic/manager/AreaManager.h"
#include "blackguard/core/FontManager.h"
#include "blackguard/interface/manager/TilesetManager.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "blackguard/logic/manager/FactionManager.h"

namespace BG {

class Window;
class GameMaster;

class Application {
  public:
    Application();
    Application(int fps, int argc, char* argv[]);

    virtual bool initialise(const int width, const int height, const std::string name);
    virtual void draw();
    virtual void run();

    static const int DEFAULT_FRAMERATE = 30;
    static const int DEFAULT_WIDTH = 1024;
    static const int DEFAULT_HEIGHT = 768;

  protected:
    int framerate;
    int argc;
    char** argv;
    bool quit;

    std::unique_ptr<Window> window;
    std::unique_ptr<Timer> masterTimer;

    std::unique_ptr<GameMaster> gameMaster;
    AreaManager world;
    EntityManager entityManager;
    FontManager fontManager;
    TilesetManager tilesetManager;
    FactionManager factionManager;

    virtual void updateFrame();
    void waitForNextFrame();

    virtual void loadResources();
    virtual void loadAreas() =0;
    virtual void loadTilesets();
    virtual void loadFonts();
    virtual void buildGame() =0;
    virtual void buildScreen() =0;

    static const int MS_IN_SECOND = 1000;

    virtual const std::string getAreaDirectory() const;
    virtual const std::string getTilesetDirectory() const;
    virtual const std::string getFontDirectory() const;
};

} // BG

#endif // APPLICATION_H

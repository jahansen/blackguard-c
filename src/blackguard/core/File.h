// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef FILE_H
#define FILE_H

#include <string>
#include <fstream>

namespace BG {

class File {
  public:
    File();
    File(std::string fileName);
    virtual ~File();

    bool open();
    bool close();
    inline bool isOpen() {return stream.is_open();}
    int getNextInt() throw(std::exception);
    std::string getAll() throw(std::exception);

  protected:
  private:
    std::string fileName;
    std::ifstream stream;
};

}

#endif // FILE_H

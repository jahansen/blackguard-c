// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "RandomGenerator.h"
#include <chrono>

namespace BG {

RandomGenerator::RandomGenerator() :
  generator(std::chrono::system_clock::now().time_since_epoch().count()) {}

RandomGenerator::RandomGenerator(int seed) :
  generator(seed) {}

int RandomGenerator::range(int min, int max) {
  // TODO: handle negatives and overflowed values
  return generator() % (max - min) + min;
}

int RandomGenerator::rolldX(int x) {
  return generator() % x + 1;
}

int RandomGenerator::rollXdY(int x, int y) {
  int sum = 0;
  for (int i = 0; i < x; ++i) {
    sum += rolldX(y);
  }
  return sum;
}

int RandomGenerator::rollXdYplusZ(int x, int y, int z) {
  return rollXdY(x, y) + z;
}

bool RandomGenerator::percentChance(int chance) {
  return rolldX(100) <= chance;
}

bool RandomGenerator::flipCoin() {
  return percentChance(50);
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TIMER_H
#define TIMER_H

#include "SDL2/SDL.h"

namespace BG {

class Timer
{
  public:
    Timer();

    Uint32 getTicks() const;
    void start();
    void stop();
    void tick();
    void reset();

  protected:
    Uint32 timeStarted;
    Uint32 pausedTicks;
    bool active;

  private:
};

} // BG

#endif // TIMER_H

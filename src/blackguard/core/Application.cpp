// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Application.h"
#include "blackguard/core/Window.h"
#include "blackguard/core/Directory.h"
#include "blackguard/interface/MasterPanel.h"
#include "blackguard/core/GameMaster.h"
#include "../external/easylogging++.h"

namespace BG {

Application::Application()
: framerate(DEFAULT_FRAMERATE),
  quit(false),
  window(std::unique_ptr<Window>(new Window())),
  masterTimer(std::unique_ptr<Timer>(new Timer())) {}

Application::Application(int fps, int argc, char** argv)
: framerate(fps),
  argc(argc),
  argv(argv),
  window(std::unique_ptr<Window>(new Window())),
  masterTimer(std::unique_ptr<Timer>(new Timer())) {}

bool Application::initialise(const int width, const int height, const std::string name) {
  if (window->initialise(width, height, name)) {

    loadResources();
    buildGame();
    buildScreen();

    return true;
  } else {
    LOG(ERROR) << SDL_GetError();
    return false;
  }
}

void Application::loadResources() {
  loadAreas();
  loadTilesets();
  loadFonts();
}

void Application::loadTilesets() {
  auto tilesetFiles = Directory::getFileNames(getTilesetDirectory(), std::regex("(.*)[.](json)"));

  for (std::string tilesetFile : tilesetFiles) {
    tilesetManager.createTileset(getTilesetDirectory() + "/" + tilesetFile, window->getRenderer());
  }
}

void Application::loadFonts() {
  auto fontFiles = Directory::getFileNames(getFontDirectory(), std::regex("(.*)[.](json)"));

  for (std::string fontFile : fontFiles) {
    fontManager.createFont(getFontDirectory() + "/" + fontFile);
  }
}

void Application::run() {
  while (!quit) {
    masterTimer->tick();
    gameMaster->updateWorld();
    gameMaster->handleInput(quit);
    updateFrame();
    waitForNextFrame();
  }
}

void Application::updateFrame() {
  window->clear();
  draw();

  window->redraw();
}

void Application::draw() {
  window->getMasterPanel()->draw();
}

void Application::waitForNextFrame() {
  if ((int)masterTimer->getTicks() < MS_IN_SECOND / framerate) {
    SDL_Delay((MS_IN_SECOND / framerate) - masterTimer->getTicks());
  }
}

const std::string Application::getAreaDirectory() const {
  return "data/areas";
}

const std::string Application::getTilesetDirectory() const {
  return "data/tilesets";
}

const std::string Application::getFontDirectory() const {
  return "data/fonts";
}

} // BG

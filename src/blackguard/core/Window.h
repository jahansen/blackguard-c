// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <string>
#include <memory>

namespace BG {

struct WindowDeleter {
  void operator()(SDL_Window* ptr) {
    SDL_DestroyWindow(ptr);
  }
};

struct RendererDeleter {
  void operator()(SDL_Renderer* ptr) {
    SDL_DestroyRenderer(ptr);
  }
};

class Panel;

class Window {
  public:
    Window();
    virtual ~Window();

    bool initialise(Uint16 width, Uint16 height, std::string title);

    void setCaption(std::string caption);

    inline std::string getCaption() const {return std::string(SDL_GetWindowTitle(screen.get()));}
    inline int getHeight() const {int w; int h; SDL_GetWindowSize(screen.get(), &w, &h); return h;}
    inline int getWidth() const {int w; int h; SDL_GetWindowSize(screen.get(), &w, &h); return w;}
    inline int getBitsPerPixel() const {return SDL_GetWindowPixelFormat(screen.get());}
    inline SDL_Window* getScreen() const {return screen.get();}
    inline SDL_Renderer* getRenderer() const {return renderer.get();}
    inline Panel* getMasterPanel() const {return masterPanel.get();}

    void clear();
    void redraw();

	private:
    std::string title;
    std::unique_ptr<SDL_Window, WindowDeleter> screen;
    std::unique_ptr<SDL_Renderer, RendererDeleter> renderer;
    std::unique_ptr<Panel> masterPanel;
};

} // BG

#endif // WINDOW_H

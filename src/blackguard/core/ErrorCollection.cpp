// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ErrorCollection.h"
#include "../external/easylogging++.h"

namespace BG {

ErrorCollection::ErrorCollection() {
}

ErrorCollection::~ErrorCollection() {
}

void ErrorCollection::printToLog() const {
  for (auto error : errors) {
    LOG(ERROR) << error;
  }
}

} // BG

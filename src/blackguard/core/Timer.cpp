// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Timer.h"

namespace BG{

Timer::Timer() {
  active = false;
  timeStarted = 0;
  pausedTicks = 0;
}

Uint32 Timer::getTicks() const {
  if (active) {
    return SDL_GetTicks() - timeStarted;
  } else {
    return pausedTicks;
  }
}

void Timer::start() {
  active = true;
  timeStarted = SDL_GetTicks() - pausedTicks;
  pausedTicks = 0;
}

void Timer::stop() {
  active = false;
  pausedTicks = SDL_GetTicks() - timeStarted;
}

void Timer::tick() {
  if (!active) {
    start();
  }

  timeStarted = SDL_GetTicks();
}

void Timer::reset() {
  active = false;
  timeStarted = 0;
  pausedTicks = 0;
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "FontManager.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/interface/Font.h"
#include "../external/easylogging++.h"

namespace BG {

FontManager::FontManager() {
}

FontManager::~FontManager() {
}

void FontManager::createFont(std::string filename) {
  ErrorCollection errors;
  createFontWithErrors(filename, errors);
  if (!errors.isEmpty()) {
    errors.printToLog();
    LOG(FATAL) << "Failed to create font from file: '" << filename << "'";
  }
}

void FontManager::createFontWithErrors(std::string filename, ErrorCollection& errors) {
  auto font = std::unique_ptr<Font>(new Font(filename));
  if (font->load(errors)) {
    add(std::move(font));
  }
}

boost::optional<const Font*> FontManager::getFont(int id) const {
  boost::optional<const Font*> option;
  if (auto font = dynamic_cast<const Font*>(get(id))) {
    option = {font};
  }
  return option;
}

boost::optional<const Font*> FontManager::getFont(std::string tag) const {
  boost::optional<const Font*> option;
  if (auto font = dynamic_cast<const Font*>(get(tag))) {
    option = {font};
  }
  return option;
}

} // BG

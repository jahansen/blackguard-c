// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Window.h"
#include "blackguard/interface/MasterPanel.h"

namespace BG{

Window::Window() {
}

Window::~Window() {
  Mix_CloseAudio();
  TTF_Quit();
  SDL_Quit();
}

bool Window::initialise(Uint16 width, Uint16 height, std::string title="") {
	//Initialise all SDL subsystems
	if (SDL_Init(SDL_INIT_TIMER|SDL_INIT_AUDIO|SDL_INIT_VIDEO) == -1) return false;

	//Initialise SDL_ttf
	if (TTF_Init() == -1) return false;

	//Initialise SDL_mixer
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1) return false;

  //Initialise screen and renderer
  SDL_Window* screenPtr = nullptr;
  SDL_Renderer* rendererPtr = nullptr;
  SDL_CreateWindowAndRenderer(
    width,
    height,
    SDL_WINDOW_SHOWN,
    &screenPtr,
    &rendererPtr
  );

  if (screenPtr == nullptr || rendererPtr == nullptr) return false;

  screen = std::unique_ptr<SDL_Window, WindowDeleter>(screenPtr);
  renderer = std::unique_ptr<SDL_Renderer, RendererDeleter>(rendererPtr);

  setCaption(title);

	masterPanel = std::unique_ptr<MasterPanel>(new MasterPanel("screen", width, height, screen.get(), renderer.get()));

	return true;
}

void Window::setCaption(std::string caption) {
  SDL_SetWindowTitle(screen.get(), caption.data());
}

void Window::clear() {
  SDL_SetRenderDrawColor(renderer.get(), 0, 0, 0, 255);
  SDL_RenderClear(renderer.get());
}

void Window::redraw() {
	SDL_RenderPresent(renderer.get());
}

} // BG

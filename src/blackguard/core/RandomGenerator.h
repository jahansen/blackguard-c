// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#include <random>

namespace BG {

class RandomGenerator
{
  public:
    RandomGenerator();
    RandomGenerator(int seed);

  int range(int min, int max);
  int rolldX(int x);
  int rollXdY(int x, int y);
  int rollXdYplusZ(int x, int y, int z);
  bool flipCoin();
  bool percentChance(int chance);

  private:
    std::mt19937 generator;
};

} // BG

#endif // RANDOMGENERATOR_H

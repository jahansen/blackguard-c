#ifndef ABSTRACTMANAGER_H
#define ABSTRACTMANAGER_H

#include <vector>
#include <memory>
#include "blackguard/logic/Entity.h"

namespace BG {

class AbstractManager {
  public:
    AbstractManager();
    virtual ~AbstractManager();

    inline int getCount() const {return entities.size();}

  protected:
    int nextId;
    std::vector<std::unique_ptr<Entity>> entities;

    void add(std::unique_ptr<Entity> entity);
    Entity* get(int id) const;
    Entity* get(std::string tag) const;
    void erase(Entity* entity);
};

} // BG

#endif // ABSTRACTMANAGER_H

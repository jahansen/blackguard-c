// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "File.h"
#include "../external/easylogging++.h"

namespace BG {

File::File() {
  fileName = "";
}

File::File(std::string fileName)
: fileName(fileName) {}

File::~File() {
  if (isOpen())
    close();
}

bool File::open() {
  stream.open(fileName.data());
  if (!isOpen()) {
    stream.clear();
    return false;
  } else {
    return true;
  }
}

bool File::close() {
  stream.close();
  return !isOpen();
}

int File::getNextInt() throw(std::exception) {
  stream.exceptions(std::ios::failbit);
  int intValue;
  stream >> intValue;
  return intValue;
}

std::string File::getAll() throw(std::exception) {
  stream.exceptions(std::ios::failbit);

  std::string fileContents;
  stream.seekg(0, std::ios::end);
  fileContents.resize(stream.tellg());
  stream.seekg(0, std::ios::beg);
  stream.read(&fileContents[0], fileContents.size());
  return fileContents;
}

}

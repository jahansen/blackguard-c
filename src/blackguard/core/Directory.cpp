// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Directory.h"
#include "../external/tinydir.h"
#include "../external/easylogging++.h"

namespace BG {

Directory::Directory() {}

Directory::~Directory() {}

std::vector<std::string> Directory::getFileNames(std::string directoryPath) {

  tinydir_dir directory;
  auto openResult = tinydir_open(&directory, directoryPath.data());

  std::vector<std::string> fileNames;

  if (openResult != 0) {
    LOG(ERROR) << "Could not find directory with path: " << directoryPath;
  }

  while (directory.has_next) {
    tinydir_file file;
    tinydir_readfile(&directory, &file);

    if (!file.is_dir) {
      fileNames.push_back(file.name);
    }

    tinydir_next(&directory);
  }

  tinydir_close(&directory);

  return fileNames;
}

std::vector<std::string> Directory::getFileNames(std::string directoryPath, std::regex regex) {
  auto allFileNames = getFileNames(directoryPath);
  std::vector<std::string> matchingFileNames(allFileNames.size());

  auto iter = std::copy_if(allFileNames.begin(), allFileNames.end(), matchingFileNames.begin(), [regex](std::string fileName) {
      return std::regex_match(fileName, regex);
  });

  matchingFileNames.resize(std::distance(matchingFileNames.begin(), iter));

  return matchingFileNames;
}

}

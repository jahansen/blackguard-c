// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "GameMaster.h"
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/CellModel.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "blackguard/core/Window.h"
#include "../external/easylogging++.h"

namespace BG {

GameMaster::GameMaster(EntityManager* entityManager, AreaManager* world, FontManager* fontManager, TextLog* log, Window* window)
: entityManager(entityManager),
  world(world),
  fontManager(fontManager),
  log(log),
  window(window),
  waitingForInput(false),
  receivedQuitCommand(false) {}

Panel* GameMaster::getMasterPanel() const {
  return window->getMasterPanel();
}

void GameMaster::addContext(std::unique_ptr<InputContext> context) {
  contexts.push_back(std::move(context));
}

void GameMaster::setContext(std::string contextId) {
  for (int i = 0; i < (int)contexts.size(); ++i) {
    InputContext* c = contexts.at(i).get();
    if (c->getId() == contextId) {
      currentContext = {c};
      return;
    }
  }
  LOG(ERROR) << "Unable to locate context '" << contextId << "'";
}

void GameMaster::handleInput(bool& quit) {
  while (SDL_PollEvent(&event)) {

    if (currentContext) {
      currentContext.get()->handleInput(event, quit);
    }

    // If [X] is clicked, exit the game
    if (event.type == SDL_QUIT)
      quit = true;
  }

  if (receivedQuitCommand) quit = receivedQuitCommand;
}

void GameMaster::updateWorld() {
  if (actionQueue.empty() || waitingForInput) return;

  worldCalendar.setAbsoluteTime(actionQueue.next().getTime());
  auto currentActor = actionQueue.next().getActor();
  actionQueue.pop();
  currentActor->startTurn();
}

void GameMaster::scheduleInitialTurns() {
  auto entities = entityManager->getActors();
  for (auto entity : entities) {
    if (auto actor = dynamic_cast<BG::Actor*>(entity)) {
      scheduleNextTurn(actor, worldCalendar.getTimeSecondsFromNow(1));
    } else {
      LOG(ERROR) << "GameMaster::scheduleInitialTurns(): failed to cast entity '" << entity->getTag() << "' into Actor";
    }
  }
}

void GameMaster::scheduleNextTurn(Actor* actor, long time) {
  actor->endTurn();
  stopWaitingForInput();
  actionQueue.push(actor, time);
}

void GameMaster::removeEntity(BG::PhysicalEntity* entity) {
  actionQueue.cancelActionsForEntity(entity);
  entityManager->deleteEntity(entity);
}

boost::optional<Actor*> GameMaster::getActorInCell(const CellModel* cell, const AreaModel* area) const {
  std::vector<PhysicalEntity*> entities = entityManager->getEntitiesInArea(area);
  std::vector<PhysicalEntity*> actorInCell(entities.size());
  boost::optional<Actor*> option;
  auto iter = std::copy_if(entities.begin(), entities.end(), actorInCell.begin(),
                           [cell](PhysicalEntity* entity){return entity->getPosition()->getRow() == cell->getRowPos() &&
                                                             entity->getPosition()->getCol() == cell->getColPos() &&
                                                             entity->getType() == "actor";});
  actorInCell.resize(std::distance(actorInCell.begin(), iter));
  if (!actorInCell.empty()) {
    auto actor = dynamic_cast<Actor*>(actorInCell.at(0));
    if (actor) option = {actor};
  }
  return option;
}

void GameMaster::moveActor(Actor* actor, const AreaModel* area, Direction direction) {
  auto targetCell = area->getAdjacentCell(*actor->getPosition(), direction);
  if (targetCell) {
    moveActorToCell(actor, targetCell.get());
  } else {
    LOG(ERROR) << "BlackguardGM::moveActor(): Failed to get find target cell";
  }
}

void GameMaster::moveActorToCell(Actor* actor, const CellModel* cell) {
  if (cellIsPassable(cell)) {
    auto actorInTargetCell = getActorInCell(cell, actor->getArea().get());
    if (actorInTargetCell) {
      actor->bumpActor(actorInTargetCell.get());
    } else {
      actor->setPosition(Position(cell->getX(), cell->getY()));
      actor->moveDone();
    }
  } else {
    actor->bumpWall();
  }
}

void GameMaster::moveActorToArea(Actor* actor, const AreaModel* area, const CellModel* cell) {
  actor->setArea(area);
  actor->setPosition(cell->getPosition());
}


} // BG

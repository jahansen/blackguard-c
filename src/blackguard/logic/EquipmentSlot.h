// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef EQUIPMENTSLOT_H
#define EQUIPMENTSLOT_H

#include <boost/optional.hpp>
#include "blackguard/logic/Equipment.h"

namespace BG {

class EquipmentSlot {
  public:
    EquipmentSlot(int slotType);
    virtual ~EquipmentSlot();

    inline int getSlotType() const {return slotType;}
    inline boost::optional<const Equipment*> getEquipment() const {return slot;}

    inline void equip(const Equipment* equipment) {slot = equipment;}
    inline void unequip() {slot = boost::none;}

  private:
    boost::optional<const Equipment*> slot;
    int slotType;
};

} // BG

#endif // EQUIPMENTSLOT_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef CALENDAR_H
#define CALENDAR_H

namespace BG {

class Calendar {
  public:
    Calendar();

    long getAbsoluteTime() const {return time;}
    int getMillisecond() const;
    int getSecond() const;
    int getMinute() const;
    int getHour() const;
    int getDay() const;
    int getMonth() const;
    int getYear() const;

    long getTimeMillisecondsFromNow(int ms) const;
    long getTimeSecondsFromNow(int seconds) const;

    void setAbsoluteTime(long time) {this->time = time;}
    void setMillisecond(int millisecond);
    void setSecond(int second);
    void setMinute(int minute);
    void setHour(int hour);
    void setDay(int date);
    void setMonth(int month);
    void setYear(int year);

    void setTime(int hour, int minute, int second);
    void setFullTime(int hour, int minute, int second, int millisecond);
    void setDate(int day, int month, int year);
    void setDateTime(int hour, int minute, int second, int day, int month, int year);
    void setFullDateTime(int hour, int minute, int second, int millisecond, int day, int month, int year);

    void addMilliseconds(int milliseconds);
    void addSeconds(int seconds);
    void addMinutes(int minutes);
    void addHours(int hours);
    void addDays(int days);
    void addMonths(int months);
    void addYears(int years);

  protected:
    long time;

    virtual int secondsPerMinute() const;
    virtual int minutesPerHour() const;
    virtual int hoursPerDay() const;
    virtual int daysPerYear() const;

  private:
    long timeAsSeconds() const;

    long getSecondsPerHour() const;
    long getSecondsPerDay() const;
    long getSecondsPerMonth() const;
    long getSecondsPerYear() const;

    long getSecondsForMinute(int minute) const;
    long getSecondsForHour(int hour) const;
    long getSecondsForDay(int day) const;
    long getSecondsForMonth(int month) const;
    long getSecondsForYear(int year) const;

    static const int MS = 1000;
};

} // BG

#endif // CALENDAR_H


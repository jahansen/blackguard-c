// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include <algorithm>
#include "Pathfinder.h"
#include "blackguard/logic/PathNodeQueue.h"
#include "blackguard/logic/AreaModel.h"
#include "../external/easylogging++.h"

namespace BG {

Pathfinder::Pathfinder() {
}

Pathfinder::~Pathfinder() {
}

std::vector<const CellModel*> Pathfinder::getShortestPath(const AreaModel* area, const CellModel* start, const CellModel* goal, const std::vector<Actor*>& actorsInArea) const {
  PathNodeQueue openList;
  PathNodeQueue closedList;
  openList.push(PathNodePtr(new PathNode(start, 0, area->estimateDistanceBetweenCells(start, goal))));

  PathNodePtr currentNode;
  while (!openList.empty()) {
    currentNode = openList.pop();
    if (currentNode->getCell() == goal) {
      return constructFinalPath(currentNode.get());
    } else {
      auto currentNodeRef = closedList.push(std::move(currentNode));
      auto neighbours = area->getPassableNeighbours(currentNodeRef->getCell(), goal, actorsInArea);

      for (auto neighbour : neighbours) {
        int tentativeDistanceFromStart = currentNodeRef->getDistanceFromStart() + area->getMovementCost(currentNodeRef->getCell(), neighbour);

        auto neighbourInClosedList = closedList.find(neighbour);
        auto neighbourInOpenList = openList.find(neighbour);

        if (neighbourInClosedList && tentativeDistanceFromStart < neighbourInClosedList.get()->getDistanceFromStart()) {
          updateNodeParent(neighbourInClosedList.get(), currentNodeRef, tentativeDistanceFromStart, area->estimateDistanceBetweenCells(neighbour, goal));
        } else if (neighbourInOpenList && tentativeDistanceFromStart < neighbourInOpenList.get()->getDistanceFromStart()) {
          updateNodeParent(neighbourInOpenList.get(), currentNodeRef, tentativeDistanceFromStart, area->estimateDistanceBetweenCells(neighbour, goal));
        } else if (!neighbourInOpenList && !neighbourInClosedList) {
          openList.push(PathNodePtr(new PathNode(neighbour, currentNodeRef, tentativeDistanceFromStart, area->estimateDistanceBetweenCells(neighbour, goal))));
        }
      }
    }
  }

  return std::vector<const CellModel*>();
}

boost::optional<const CellModel*> Pathfinder::getNextStepTowardsGoal(const AreaModel* area, const CellModel* start, const CellModel* goal, const std::vector<Actor*>& actorsInArea) const {
  boost::optional<const CellModel*> option;
  auto path = getShortestPath(area, start, goal, actorsInArea);
  if (path.size() >= 2) {
    option = {path.at(1)};
  }
  return option;
}

void Pathfinder::updateNodeParent(PathNode* childNode, const PathNode* parentNode, int distanceFromStart, int distanceToGoal) const {
  childNode->setParent(parentNode);
  childNode->setDistanceFromStart(distanceFromStart);
  childNode->setDistanceToGoal(distanceToGoal);
}

std::vector<const CellModel*> Pathfinder::constructFinalPath(const PathNode* goalNode) const {
  auto finalPath = std::vector<const CellModel*>();

  auto currentNode = goalNode;
  finalPath.push_back(currentNode->getCell());

  while (currentNode->getParent()) {
    currentNode = currentNode->getParent().get();
    finalPath.push_back(currentNode->getCell());
  }

  std::reverse(finalPath.begin(), finalPath.end());
  return finalPath;
}

} // BG

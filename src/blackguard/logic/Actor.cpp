// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Actor.h"
#include "blackguard/logic/Behaviour.h"
#include "../external/easylogging++.h"

namespace BG {

Actor::Actor(std::string tag, std::string name, bool alive, const AreaModel* area, Position position, const Faction* faction, const Tile* tile)
: DrawableEntity(tag, name, position, area, tile),
  alive(alive),
  myTurn(false),
  faction(faction) {}

Actor::~Actor() {
}

void Actor::die() {
  alive = false;
  onDeath();
}

void Actor::setBehaviour(std::string behaviourTag) {
  for (int i = 0; i < (int)behaviours.size(); ++i) {
    if (behaviours.at(i)->getTag() == behaviourTag) {
      currentBehaviour = behaviours.at(i).get();
      return;
    }
  }
  LOG(WARNING) << "Actor::setBehaviour() failed to find behaviour '" << behaviourTag;
}

bool Actor::hasItemEquipped(const BG::Equipment* item) const {
  auto slot = std::find_if(equipmentSlots.begin(), equipmentSlots.end(), [item](BG::EquipmentSlot eqSlot)->bool{
    return eqSlot.getSlotType() == item->getEquipmentType();
  });

  return (slot != equipmentSlots.end() && slot->getEquipment() && slot->getEquipment().get()->getTag() == item->getTag());
}

std::vector<const Equipment*> Actor::getEquippedItems() const {

  std::vector<const Equipment*> equippedItems;
  for (auto slot : equipmentSlots) {
    if (auto equipment = slot.getEquipment()) {
      equippedItems.push_back(equipment.get());
    }
  }
  return equippedItems;
}

} // BG

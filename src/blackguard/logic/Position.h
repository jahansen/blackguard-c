// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef POSITION_H
#define POSITION_H

#include "blackguard/logic/AbstractEntity.h"

namespace BG {

class Position : public AbstractEntity {
  public:
    Position(int x=0, int y=0, int z=0);
    virtual ~Position();

    virtual Position* getCopy() const;
    virtual std::string getType() const override {return "position";}

    inline int getX() const {return x;}
    inline int getY() const {return y;}
    inline int getZ() const {return z;}
    inline int getRow() const {return y;}
    inline int getCol() const {return x;}
    inline int getLevel() const {return z;}

    inline void setX(int x) {this->x = x;}
    inline void setY(int y) {this->y = y;}
    inline void setZ(int z) {this->z = z;}
    inline void setPosition(int x, int y, int z=0) {this->x = x; this->y = y; this->z = z;}

    bool isBetweenInclusive(Position p1, Position p2) const;
    bool isBetweenExclusive(Position p1, Position p2) const;

  protected:
    int x;
    int y;
    int z;
};

bool operator==(const Position& lhs, const Position& rhs);
bool operator!=(const Position& lhs, const Position& rhs);

} // BG

#endif // POSITION_H

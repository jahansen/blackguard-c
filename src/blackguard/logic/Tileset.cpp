// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Tileset.h"
#include "blackguard/core/File.h"
#include "blackguard/core/ErrorCollection.h"
#include "../external/easylogging++.h"
#include <algorithm>

namespace BG {

Tileset::Tileset(std::string filename)
: Entity(),
  filename(filename) {}

Tileset::~Tileset() {
  unload();
}

Tileset* Tileset::getCopy() const {
  auto tileset = new Tileset(filename);
  tileset->tag = tag;
  tileset->name = name;
  tileset->imageFilename = imageFilename;
  tileset->tiles = tiles;
  tileset->tileImage = {tileImage};
  return tileset;
}

void Tileset::load(SDL_Renderer* renderer) {
  ErrorCollection errors;
  if (!loadWithErrors(renderer, errors)) {
    errors.printToLog();
    LOG(FATAL) << "Failed to load required tilset.";
  }
}

bool Tileset::loadWithErrors(SDL_Renderer* renderer, ErrorCollection& errors) {
  unload();

  File tilesetFile(filename);
  if (!tilesetFile.open()) {
    errors.addError("Unable to open tileset file '" + filename + "'.");
    return false;
  }

  rapidjson::Document json;
  std::string fileData = tilesetFile.getAll();
  json.Parse<0>(fileData.data());
  tilesetFile.close();

  errors = validateTilesetJSON(json, filename);
  if (!errors.isEmpty()) return false;

  tag = json["tag"].GetString();
  name = json["name"].GetString();
  imageFilename = json["filename"].GetString();
  int tileHeight = json["tileHeight"].GetInt();
  int tileWidth = json["tileWidth"].GetInt();
  const rapidjson::Value& tileRows = json["tiles"];

  tileImage = BG::Image(imageFilename);
  if (!tileImage.get().load(renderer)) {
    errors.addError("Unable to load tileset image: " + imageFilename);
    return false;
  }

  for (rapidjson::SizeType r = 0; r < tileRows.Size(); ++r) {
    const rapidjson::Value& tileRow = tileRows[r];
    if (!tileRow.IsArray()) {
      errors.addError(buildJSONErrorString(filename, "tiles"));
      return false;
    }
    for (rapidjson::SizeType c = 0; c < tileRow.Size(); ++c) {
      if (tileRow[c].IsObject()) {
        const rapidjson::Value& tile = tileRow[c];
        if (tile.HasMember("id")) {
          this->tiles.push_back(Tile(tile["id"].GetString(), c * tileWidth, r * tileHeight, tileWidth, tileHeight, &tileImage.get()));
        }
      }
    }
  }
  return true;
}

ErrorCollection Tileset::validateTilesetJSON(rapidjson::Document& json, std::string filename) const {
  ErrorCollection errors;
  if (!json.IsObject()) {
    errors.addError("Unable to parse JSON in file: " + filename);
    return errors;
  }
  if (!json.HasMember("tag") || !json["tag"].IsString()) {
    errors.addError(buildJSONErrorString(filename, "tag"));
  }
  if (!json.HasMember("name") || !json["name"].IsString()) {
    errors.addError(buildJSONErrorString(filename, "name"));
  }
  if (!json.HasMember("filename") || !json["filename"].IsString()) {
    errors.addError(buildJSONErrorString(filename, "filename"));
  }
  if (!json.HasMember("tileHeight") || !json["tileHeight"].IsInt()) {
    errors.addError(buildJSONErrorString(filename, "tileHeight"));
  }
  if (!json.HasMember("tileWidth") || !json["tileWidth"].IsInt()) {
    errors.addError(buildJSONErrorString(filename, "tileWidth"));
  }
  if (!json.HasMember("tiles") || !json["tiles"].IsArray()) {
    errors.addError(buildJSONErrorString(filename, "tiles"));
  }

  return errors;
}

std::string Tileset::buildJSONErrorString(std::string filename, std::string property) const {
  return "Tileset file '" + filename + "' does not contain a valid '" + property + "' property.";
}

void Tileset::unload() {
  imageFilename = "";
  tiles.clear();
}

const Tile* Tileset::getTile(std::string tileTag) const {
  auto tile = getTileOpt(tileTag);
  if (!tile) {
    LOG(FATAL) << "Cannot find tile with tag: '" << tileTag << "'";
    return nullptr;
  } else {
    return tile.get();
  }
}

boost::optional<const Tile*> Tileset::getTileOpt(std::string tileTag) const {
  boost::optional<const Tile*> option;
  auto t = std::find_if(tiles.begin(), tiles.end(), [tileTag](Tile tile)->bool{return tile.getTag() == tileTag;});

  if (t != tiles.end()) {
    option = {&(*t)};
  }
  return option;
}

boost::optional<const Image*> Tileset::getImage() const {
  boost::optional<const Image*> option;
  if (tileImage) {
    option = &tileImage.get();
  }
  return option;
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Tile.h"
#include "blackguard/interface/Image.h"

namespace BG {

Tile::Tile(std::string tag, int x, int y, int w, int h, Image* tileImage)
: Entity(tag, tag),
  tileImage(tileImage) {
  clip.x = x;
  clip.y = y;
  clip.w = w;
  clip.h = h;
}

Tile::~Tile() {}

Entity* Tile::getCopy() const {
  auto copy = new Tile(tag, clip.x, clip.y, clip.w, clip.h, tileImage);
  copy->id = id;
  return copy;
}

bool operator==(const Tile& lhs, const Tile& rhs) {
  return (lhs.getId() == rhs.getId() &&
          lhs.getClip().x == rhs.getClip().x &&
          lhs.getClip().y == rhs.getClip().y &&
          lhs.getClip().w == rhs.getClip().w &&
          lhs.getClip().h == rhs.getClip().h);
}

} // BG

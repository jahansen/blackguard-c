// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Entity.h"
#include "blackguard/logic/AreaModel.h"

namespace BG {

Entity::Entity() {}

Entity::Entity(std::string tag, std::string name)
: id(-1),
  tag(tag),
  name(name) {}

Entity::~Entity() {}

} // BG

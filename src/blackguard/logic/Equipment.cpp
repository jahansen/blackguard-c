// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Equipment.h"

namespace BG {

Equipment::Equipment(std::string tag, std::string name, int equipmentType, boost::optional<const AreaModel*> area, boost::optional<Position> position, const Tile* itemTile, const Tile* avatarTile)
: Item(tag, name, area, position, itemTile),
  equipmentType(equipmentType),
  avatarTile(avatarTile) {}

Equipment::~Equipment() {}

BG::Entity* Equipment::getCopy() const {
  auto copy = new Equipment(tag, name, equipmentType, area, position, tile, avatarTile);
  copy->id = id;
  return copy;
}

} // BG

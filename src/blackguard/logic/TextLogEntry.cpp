// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "TextLogEntry.h"

namespace BG {

TextLogEntry::TextLogEntry(std::string text, Colour colour)
: text(text),
  colour(colour) {}

TextLogEntry::~TextLogEntry() {
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PATHNODE_H
#define PATHNODE_H

#include <boost/optional.hpp>

namespace BG {

class CellModel;

class PathNode {
  public:
    PathNode(const CellModel* cell, int distFromStart, int distToEnd);
    PathNode(const CellModel* cell, const PathNode* parent, int distFromStart, int distToEnd);
    virtual ~PathNode();

    inline boost::optional<const PathNode*> getParent() const {return parent;}
    inline void setParent(const PathNode* newParent) {parent = {newParent};}
    inline const CellModel* getCell() const {return cell;}
    inline int getDistanceFromStart() const {return distFromStart;}
    inline void setDistanceFromStart(int distance) {distFromStart = distance;}
    inline int getDistanceToGoal() const {return distToGoal;}
    inline void setDistanceToGoal(int distance) {distToGoal = distance;}
    inline int getTotalDistance() const {return distFromStart + distToGoal;}

  private:
    boost::optional<const PathNode*> parent;
    const CellModel* cell;
    int distFromStart;
    int distToGoal;
};

} // BG

#endif // PATHNODE_H

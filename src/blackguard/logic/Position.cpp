// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Position.h"

namespace BG {

Position::Position(int x, int y, int z)
: AbstractEntity(),
  x(x),
  y(y),
  z(z) {}

Position::~Position() {
}

Position* Position::getCopy() const {
  auto copy = new Position(x, y, z);
  return copy;
}

bool Position::isBetweenInclusive(Position p1, Position p2) const {
  return (p1.getX() <= x && x <= p2.getX() &&
          p1.getY() <= y && y <= p2.getY());
}

bool Position::isBetweenExclusive(Position p1, Position p2) const {
  return (p1.getX() < x && x < p2.getX() &&
          p1.getY() < y && y < p2.getY());
}

bool operator==(const Position& lhs, const Position& rhs) {
  return (lhs.getX() == rhs.getX() &&
          lhs.getY() == rhs.getY() &&
          lhs.getZ() == rhs.getZ());
}

bool operator!=(const Position& lhs, const Position& rhs) {
  return (lhs.getX() != rhs.getX() ||
          lhs.getY() != rhs.getY() ||
          lhs.getZ() != rhs.getZ());
}

} // BG

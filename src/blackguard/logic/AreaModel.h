// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef AREAMODEL_H
#define AREAMODEL_H

#include <string>
#include <vector>
#include <boost/optional.hpp>
#include "rapidjson/document.h"
#include "blackguard/logic/CellModel.h"
#include "blackguard/logic/Direction.h"
#include "blackguard/logic/Position.h"
#include "blackguard/logic/Entity.h"

namespace BG {

class CellModel;
class ErrorCollection;
class Actor;
class RandomGenerator;

typedef std::vector<CellModel> CellRow;
typedef std::vector<CellRow> CellMatrix;

class AreaModel : public Entity {
  public:
    AreaModel(std::string tag, std::string name);
    virtual ~AreaModel();

    virtual std::string getType() const override {return "area";}

    // Basic Inspectors
    inline int getRowCount() const {return matrix.size();}
    inline int getColCount() const {return (matrix.size() > 0 ? matrix[0].size() : 0);}
    int getRowOffset(Direction direction) const;
    int getColOffset(Direction direction) const;
    inline const CellMatrix* getMatrix() const {return &matrix;}

    // Complex inspectors
    boost::optional<const CellModel*> getCell(int row, int col) const;
    boost::optional<const CellModel*> getCell(Position position) const;
    boost::optional<const CellModel*> getAdjacentCell(const CellModel* cell, Direction direction) const;
    boost::optional<const CellModel*> getAdjacentCell(Position position, Direction direction) const;
    const CellModel* getRandomCell(RandomGenerator* generator) const;
    const CellModel* getRandomWalkableCell(RandomGenerator* randomGenerator) const;
    Direction getDirection(Position sourcePosition, Position targetPosition) const;
    bool cellExists(Position position) const;
    bool cellsAreAdjacent(const CellModel* cell1, const CellModel* cell2) const;
    bool lineOfSightExists(Position sourcePosition, Position targetPosition) const;

    // Mutators
    void modifyCellProperty(BG::Position position, std::string key, std::string value);
    void loadAreaFromFile(std::string filename);
    bool loadAreaFromFileWithErrors(std::string filename, ErrorCollection& errors);
    void unload();

    // Virtual functions
    virtual std::vector<const CellModel*> getPassableNeighbours(const CellModel* cell, const BG::CellModel* goalCell, const std::vector<Actor*>& actorsInArea) const =0;
    virtual bool cellBlocksLineOfSight(const CellModel* cell) const =0;
    virtual bool cellIsWalkable(const CellModel* cell) const = 0;
    virtual bool cellIsPassable(const CellModel* cell, const BG::CellModel* goalCell, const std::vector<Actor*>& actorsInArea) const =0;
    virtual int getMovementCost(const CellModel* cell, const CellModel* neighbourCell) const =0;
    virtual int estimateDistanceBetweenCells(const CellModel* startCell, const CellModel* goalCell) const =0;

  protected:
    CellMatrix matrix;

    bool unobstructedLineExistsBetween(const CellModel* sourceCell, const CellModel* targetCell) const;
    ErrorCollection validateAreaJSON(rapidjson::Document& json, std::string filename) const;
    static std::string buildJSONErrorString(std::string filename, std::string property);
};

} // BG

#endif // AREAMODEL_H

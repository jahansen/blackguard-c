// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Coordinates.h"

namespace BG {

Coordinates::Coordinates(int x, int y)
: x(x),
  y(y) {}

Coordinates::~Coordinates() {}

Coordinates operator+(const Coordinates& lhs, const Coordinates& rhs) {
  return Coordinates(lhs.getX() + rhs.getX(), lhs.getY() + rhs.getY());
}

} // BG

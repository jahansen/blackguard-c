// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "CellularAutomataAreaModel.h"
#include "blackguard/core/RandomGenerator.h"

namespace BG {

CellularAutomataAreaModel::CellularAutomataAreaModel(std::string tag, std::string name)
: AreaModel(tag, name) {}

CellularAutomataAreaModel::~CellularAutomataAreaModel() {}

void CellularAutomataAreaModel::build(RandomGenerator* generator, int rows, int columns, int initialWallFrequency, int passageSize, int smoothness) {
  auto cellMatrix = IntMatrix(rows, std::vector<int>(columns, 0));

  // Randomly fill area with walls based on supplied frequency.
  initialiseSimulation(generator, cellMatrix, initialWallFrequency);

  // Running the simulation multiple times has the effect of smoothing out corridors.
  for (auto i = 0; i < smoothness; ++i) {
    runSimulation(cellMatrix, passageSize);
  }

  // The algorithm may leave unaccessible non-contiguous areas, so fill them in.
  fillDisjointAreas(cellMatrix);

  buildFinalArea(cellMatrix);
}

void CellularAutomataAreaModel::initialiseSimulation(RandomGenerator* generator, IntMatrix& cellMatrix, int initialAliveProbability) {
  for (int r = 0; r < (int)cellMatrix.size(); ++r) {
    for (int c = 0; c < (int)cellMatrix[0].size(); ++c) {
      if (cellIsOnBorder(r, c, cellMatrix) || generator->percentChance(initialAliveProbability)) {
        cellMatrix[r][c] = 1;
      } else {
        cellMatrix[r][c] = 0;
      }
    }
  }
}

void CellularAutomataAreaModel::runSimulation(IntMatrix& cellMatrix, int deathLimit) {
  auto areaCopy = cellMatrix;
  for (int r = 1; r < (int)cellMatrix.size()-1; ++r) {
    for (int c = 1; c < (int)cellMatrix[0].size()-1; ++c) {
      int adjacentWalls = countAdjacentAlive(r, c, areaCopy);
      if (areaCopy[r][c] == 1) {
        cellMatrix[r][c] = (adjacentWalls < deathLimit ? 0 : 1);
      } else {
        cellMatrix[r][c] = (adjacentWalls > BIRTH_LIMIT ? 1 : 0);
      }
    }
  }
}

int CellularAutomataAreaModel::countAdjacentAlive(int row, int col, const IntMatrix& cellMatrix) const {
  int wallCount = 0;

  // Loop through the block of 9 cells around the target cell.
  for (int r = row-1; r <= row+1; ++r) {
    for (int c = col-1; c <= col+1; ++c) {
      if (cellMatrix[r][c]) ++wallCount;
    }
  }

  // If the target cell is a wall, don't count it as a neighbour.
  if (cellMatrix[row][col]) --wallCount;

  return wallCount;
}

bool CellularAutomataAreaModel::cellIsOnBorder(int r, int c, const IntMatrix& cellMatrix) const {
  return (r == 0 || r == (int)cellMatrix.size() - 1 ||
          c == 0 || c == (int)cellMatrix[0].size() - 1);
}

void CellularAutomataAreaModel::fillDisjointAreas(IntMatrix& cellMatrix) {

  // Value for marking filled in areas. We need to be able to determine the largest contiguous area,
  // which will be the 'playable' area. Other areas will be filled in.
  int nextValue = 2;

  // Flood fill each non-connected, contiguous area with a unique number.
  for (int r = 0; r < (int)cellMatrix.size(); ++r) {
    for (int c = 0; c < (int)cellMatrix[0].size(); ++c) {
      if (cellMatrix[r][c] == 0) {
        floodFill(cellMatrix, r, c, nextValue);
        ++nextValue;
      }
    }
  }

  // Determine the size of each contiguous area.
  int maxCount = 0;
  int maxArea = 2;
  int currentCount = 0;
  for (int i = 2; i <= nextValue; ++i) {
    for (int r = 0; r < (int)cellMatrix.size(); ++r) {
      for (int c = 0; c < (int)cellMatrix[0].size(); ++c) {
        if (cellMatrix[r][c] == i) {
          ++currentCount;
        }
      }
    }
    if (currentCount > maxCount) {
      maxCount = currentCount;
      maxArea = i;
    }
    currentCount = 0;
  }

  // Mark all but the largest contiguous area as a wall.
  for (int r = 0; r < (int)cellMatrix.size(); ++r) {
    for (int c = 0; c < (int)cellMatrix[0].size(); ++c) {
      cellMatrix[r][c] = (cellMatrix[r][c] == maxArea ? 0 : 1);
    }
  }
}

// Recursively fills a contiguous area with the given value.
void CellularAutomataAreaModel::floodFill(IntMatrix& cellMatrix, int row, int col, int value) {
  if (cellMatrix[row][col] == 0) {
    cellMatrix[row][col] = value;
    floodFill(cellMatrix, row-1, col, value);
    floodFill(cellMatrix, row-1, col+1, value);
    floodFill(cellMatrix, row, col+1, value);
    floodFill(cellMatrix, row+1, col+1, value);
    floodFill(cellMatrix, row+1, col, value);
    floodFill(cellMatrix, row+1, col-1, value);
    floodFill(cellMatrix, row, col-1, value);
    floodFill(cellMatrix, row-1, col-1, value);
  }
}

} // BG

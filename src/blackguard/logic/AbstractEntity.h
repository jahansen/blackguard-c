// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ABSTRACTENTITY_H
#define ABSTRACTENTITY_H

#include <string>

namespace BG {

class AbstractEntity
{
  public:
    AbstractEntity();
    virtual ~AbstractEntity();

    virtual std::string getType() const =0;
    virtual AbstractEntity* getCopy() const =0;
};

} // BG

#endif // ABSTRACTENTITY_H

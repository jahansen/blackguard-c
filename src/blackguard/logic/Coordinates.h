// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef COORDINATES_H
#define COORDINATES_H

namespace BG {

class Coordinates {
  public:
    Coordinates(int x, int y);
    virtual ~Coordinates();

    inline int getX() const {return x;}
    inline int getY() const {return y;}

    inline void setX(int x) {this->x = x;}
    inline void setY(int y) {this->y = y;}

  private:
    int x;
    int y;
};

Coordinates operator+(const Coordinates& lhs, const Coordinates& rhs);

} // BG

#endif // COORDINATES_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef FACTION_H
#define FACTION_H

#include <unordered_map>
#include "blackguard/logic/Entity.h"

namespace BG {

class Faction : public Entity {
  public:
    Faction(std::string tag, std::string name);
    virtual ~Faction();

    virtual BG::Entity* getCopy() const override;
    virtual std::string getType() const override {return "faction";}

    virtual bool isFriendlyTo(Faction* faction) const;
    virtual bool isNeutralTo(Faction* faction) const;
    virtual bool isHostileTo(Faction* faction) const;
    virtual bool isNotFriendlyTo(Faction* faction) const;
    virtual bool isNotNeutralTo(Faction* faction) const;
    virtual bool isNotHostileTo(Faction* faction) const;
    int getRelationshipTo(Faction* faction) const;
    bool hasRelationshipTo(Faction* faction) const;

    void addRelationship(const Faction* faction, int value);

    static const int FRIENDLY = 100;
    static const int NEUTRAL = 0;
    static const int HOSTILE = -100;

  private:
    std::unordered_map<int, int> relationships;
};

} // BG

#endif // FACTION_H

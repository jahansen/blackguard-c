// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ENTITY_H
#define ENTITY_H

#include "blackguard/logic/AbstractEntity.h"

namespace BG {

class Entity : public AbstractEntity {
  public:
    Entity();
    Entity(std::string tag, std::string name);
    virtual ~Entity();

    inline int getId() const {return id;}
    inline std::string getTag() const {return tag;}
    inline std::string getName() const {return name;}

    inline void setId(int id) {this->id = id;}

  protected:
    int id;
    std::string tag;
    std::string name;
};

} // BG

#endif // ENTITY_H

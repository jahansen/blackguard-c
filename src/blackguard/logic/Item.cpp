// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Item.h"

namespace BG {

Item::Item(std::string tag, std::string name, boost::optional<const AreaModel*> area, boost::optional<Position> position, const Tile* tile)
: DrawableEntity(tag, name, position, area, tile) {}

Item::~Item() {
}

BG::Entity* Item::getCopy() const {
  auto copy = new Item(tag, name, area, position, tile);
  copy->id = id;
  return copy;
}

} // BG

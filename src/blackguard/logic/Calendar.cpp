// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Calendar.h"

namespace BG {

Calendar::Calendar()
: time(0) {}

// Getters
int Calendar::getMillisecond() const {
  return time % MS;
}

int Calendar::getSecond() const {
  return timeAsSeconds() % secondsPerMinute();
}

int Calendar::getMinute() const {
  return (timeAsSeconds() / secondsPerMinute()) % minutesPerHour();
}

int Calendar::getHour() const {
  return (timeAsSeconds() / getSecondsPerHour()) % hoursPerDay();
}

int Calendar::getDay() const {
  long secondsFromYearStart = timeAsSeconds() - ((getYear() - 1) * getSecondsPerYear());
  long secondsFromMonthStart = secondsFromYearStart - ((getMonth() - 1) * getSecondsPerMonth());
  return (secondsFromMonthStart / getSecondsPerDay()) + 1;
}

int Calendar::getMonth() const {
  long secondsFromYearStart = timeAsSeconds() - ((getYear() - 1) * getSecondsPerYear());
  return (secondsFromYearStart / getSecondsPerMonth()) + 1;
}

int Calendar::getYear() const {
  return (timeAsSeconds() / getSecondsPerYear()) + 1;
}

long Calendar::getTimeMillisecondsFromNow(int ms) const {
  return time + ms;
}

long Calendar::getTimeSecondsFromNow(int seconds) const {
  return time + (seconds * MS);
}


// Individual setters
void Calendar::setMillisecond(int millisecond) {
  setFullTime(getHour(), getMinute(), getSecond(), millisecond);
}

void Calendar::setSecond(int second) {
  setFullTime(getHour(), getMinute(), second, getMillisecond());
}

void Calendar::setMinute(int minute) {
  setFullTime(getHour(), minute, getSecond(), getMillisecond());
}

void Calendar::setHour(int hour) {
  setFullTime(hour, getMinute(), getSecond(), getMillisecond());
}

void Calendar::setDay(int day) {
  setDate(day, getMonth(), getYear());
}

void Calendar::setMonth(int month) {
  setDate(getDay(), month, getYear());
}

void Calendar::setYear(int year) {
  setDate(getDay(), getMonth(), year);
}


// Compound setters
void Calendar::setTime(int hour, int minute, int second) {
  setFullDateTime(hour, minute, second, getMillisecond(), getDay(), getMonth(), getYear());
}

void Calendar::setFullTime(int hour, int minute, int second, int millisecond) {
  setFullDateTime(hour, minute, second, millisecond, getDay(), getMonth(), getYear());
}

void Calendar::setDate(int day, int month, int year) {
  setFullDateTime(getHour(), getMinute(), getSecond(), getMillisecond(), day, month, year);
}

void Calendar::setDateTime(int hour, int minute, int second, int day, int month, int year) {
  setFullDateTime(hour, minute, second, getMillisecond(), day, month, year);
}

void Calendar::setFullDateTime(int hour, int minute, int second, int millisecond, int day, int month, int year) {
  time = ((getSecondsForYear(year) + getSecondsForMonth(month) + getSecondsForDay(day) +
           getSecondsForHour(hour) + getSecondsForMinute(minute) + second) * MS) + millisecond;
}


// Adders
void Calendar::addMilliseconds(int milliseconds) {
  time += milliseconds;
}

void Calendar::addSeconds(int seconds) {
  time += seconds * MS;
}

void Calendar::addMinutes(int minutes) {
  time += (minutes * secondsPerMinute() * MS);
}

void Calendar::addHours(int hours) {
  time += (hours * getSecondsPerHour() * MS);
}

void Calendar::addDays(int days) {
  time += (days * getSecondsPerDay() * MS);
}

void Calendar::addMonths(int months) {
  time += (months * getSecondsPerMonth() * MS);
}

void Calendar::addYears(int years) {
  time += (years * getSecondsPerYear() * MS);
}


// Calendar settings
int Calendar::secondsPerMinute() const {
  return 60;
}

int Calendar::minutesPerHour() const {
  return 60;
}

int Calendar::hoursPerDay() const {
  return 24;
}

int Calendar::daysPerYear() const {
  return 365;
}


// Helper functions
long Calendar::timeAsSeconds() const {
  return time / MS;
}

long Calendar::getSecondsPerHour() const {
  return secondsPerMinute() * minutesPerHour();
}

long Calendar::getSecondsPerDay() const {
  return getSecondsPerHour() * hoursPerDay();
}

long Calendar::getSecondsPerMonth() const {
  return getSecondsPerDay() * 30;
}

long Calendar::getSecondsPerYear() const {
  return getSecondsPerDay() * daysPerYear();
}

long Calendar::getSecondsForMinute(int minute) const {
  return minute * secondsPerMinute();
}

long Calendar::getSecondsForHour(int hour) const {
  return hour * getSecondsPerHour();
}

long Calendar::getSecondsForDay(int day) const {
  return (day - 1) * getSecondsPerDay();
}

long Calendar::getSecondsForMonth(int month) const {
  return (month - 1) * getSecondsPerMonth();
}

long Calendar::getSecondsForYear(int year) const {
  return (year - 1) * getSecondsPerYear();
}


} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ActionQueue.h"
#include "blackguard/logic/Actor.h"

namespace BG {

bool CompareSchedule::operator()(ScheduledAction action1, ScheduledAction action2) {
  if (action1.getTime() < action2.getTime()) return true;
  else return false;
}

ActionQueue::ActionQueue() {}

ScheduledAction ActionQueue::next() const {
  return *actionQueue.begin();
}

void ActionQueue::push(Actor* actor, long time) {
  actionQueue.insert(ScheduledAction(actor, time));
}

void ActionQueue::pop() {
  if (!actionQueue.empty()) {
    actionQueue.erase(actionQueue.begin());
  }
}

void ActionQueue::cancelActionsForEntity(Entity* entity) {
  for (auto i = actionQueue.begin(); i != actionQueue.end(); ++i) {
    if (i->getActor()->getTag() == entity->getTag()) {
      actionQueue.erase(i);
    }
  }
}



} // BG

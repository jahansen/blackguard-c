// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "CellModel.h"

namespace BG {

CellModel::CellModel(Position position)
: position(position) {}

CellModel::~CellModel() {
}

void CellModel::addProperty(std::string key, std::string value) {
  cellProperties.insert({{key, value}});
}

void CellModel::setProperty(std::string key, std::string value) {
  cellProperties[key] = value;
}

std::string CellModel::getProperty(std::string key) const {
  auto value = cellProperties.find(key);
  if (value == cellProperties.cend()) {
    return "";
  } else {
    return value->second;
  }
}

bool operator==(const CellModel& lhs, const CellModel& rhs) {
  return (lhs.getPosition() == rhs.getPosition() &&
          lhs.getProperties() == rhs.getProperties());
}

} // BG

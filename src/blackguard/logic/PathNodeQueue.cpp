// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php
#include "PathNodeQueue.h"

namespace BG {

PathNodeQueue::PathNodeQueue() {
}

PathNodeQueue::~PathNodeQueue() {
}

const PathNode* PathNodeQueue::push(PathNodePtr pathNode) {
  nodeQueue.push_back(std::move(pathNode));
  return nodeQueue.back().get();
}

PathNodePtr PathNodeQueue::pop() {
  if (nodeQueue.empty()) return nullptr;

  int smallestIndex = 0;
  int smallestDistance = nodeQueue.at(smallestIndex)->getTotalDistance();
  for (auto i = 0; i < (int)nodeQueue.size(); ++i) {
    if (nodeQueue.at(i)->getTotalDistance() < smallestDistance) {
      smallestIndex = i;
      smallestDistance = nodeQueue.at(i)->getTotalDistance();
    }
  }
  auto nextNode = std::move(nodeQueue.at(smallestIndex));
  nodeQueue.erase(nodeQueue.begin() + smallestIndex);
  return nextNode;
}

boost::optional<PathNode*> PathNodeQueue::find(const CellModel* cell) const {
  boost::optional<PathNode*> option;
  for (auto i = 0; i < (int)nodeQueue.size(); ++i) {
    if (nodeQueue.at(i)->getCell() == cell) {
      option = {nodeQueue.at(i).get()};
      break;
    }
  }
  return option;
}

} // BG

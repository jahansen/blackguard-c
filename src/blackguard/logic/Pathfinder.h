// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PATHFINDER_H
#define PATHFINDER_H

#include <vector>
#include <queue>
#include <boost/optional.hpp>

namespace BG {

class AreaModel;
class CellModel;
class PathNode;
class Actor;

class Pathfinder {
  public:
    Pathfinder();
    virtual ~Pathfinder();

    std::vector<const CellModel*> getShortestPath(const AreaModel* area, const CellModel* start, const CellModel* goal, const std::vector<Actor*>& actorsInArea = std::vector<Actor*>()) const;
    boost::optional<const CellModel*> getNextStepTowardsGoal(const AreaModel* area, const CellModel* start, const CellModel* goal, const std::vector<Actor*>& actorsInArea = std::vector<Actor*>()) const;

  private:
    std::vector<const CellModel*> constructFinalPath(const PathNode* goalNode) const;
    void updateNodeParent(PathNode* childNode, const PathNode* parentNode, int distanceFromStart, int distanceToGoal) const;
};

} // BG

#endif // PATHFINDER_H

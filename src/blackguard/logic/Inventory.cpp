// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Inventory.h"
#include "blackguard/logic/Item.h"
#include "../external/easylogging++.h"

namespace BG {

Inventory::Inventory() {
}

Inventory::~Inventory() {
}

void Inventory::addItem(Item* item) {
  item->removeFromWorld();
  items.push_back(item);
}

void Inventory::removeItem(Item* item) {
  for (int i = 0; i < (int)items.size(); ++i) {
    if (items.at(i)->getId() == item->getId()) {
      items.erase(items.begin()+i);
      return;
    }
  }
  LOG(ERROR) << "Inventory::removeItem(): Unable to locate item '" << item->getId() << ":" << item->getTag() << "'";
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Behaviour.h"

namespace BG {

Behaviour::Behaviour(std::string tag, std::string name, GameMaster* gm, Actor* actor)
: Entity(tag, name),
  gm(gm),
  actor(actor) {}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TILESET_H
#define TILESET_H

#include <string>
#include <vector>
#include <boost/optional.hpp>
#include "rapidjson/document.h"
#include "blackguard/logic/Tile.h"
#include "blackguard/interface/Image.h"
#include "blackguard/logic/Entity.h"

namespace BG {

class ErrorCollection;

class Tileset : public Entity {
  public:
    Tileset(std::string filename);
    virtual ~Tileset();

    inline virtual std::string getType() const {return "tileset";}
    virtual Tileset* getCopy() const;

    bool loadWithErrors(SDL_Renderer* renderer, ErrorCollection& errors);
    void load(SDL_Renderer* renderer);
    void unload();
    inline std::string getImageFilename() const {return imageFilename;}
    inline std::vector<Tile> getTiles() const {return tiles;}

    const Tile* getTile(std::string tileTag) const;
    boost::optional<const Tile*> getTileOpt(std::string tileId) const;
    boost::optional<const Image*> getImage() const;

  protected:
    std::string filename;
    std::string imageFilename;
    std::vector<Tile> tiles;
    boost::optional<Image> tileImage;

    ErrorCollection validateTilesetJSON(rapidjson::Document& json, std::string filename) const;
    std::string buildJSONErrorString(std::string filename, std::string property) const;

};

} // BG

#endif // TILESET_H

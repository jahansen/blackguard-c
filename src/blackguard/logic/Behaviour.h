// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef JOB_H
#define JOB_H

#include "blackguard/logic/Entity.h"

namespace BG {

class GameMaster;
class Actor;

class Behaviour : public Entity {
  public:
    Behaviour(std::string tag, std::string name, GameMaster* gm, Actor* actor);

    virtual std::string getType() const override {return "job";}

    virtual void execute() =0;

  protected:
    GameMaster* gm;
    Actor* actor;
};

} // BG

#endif // JOB_H

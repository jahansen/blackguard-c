// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "DrawableEntity.h"

namespace BG {

DrawableEntity::DrawableEntity(std::string tag, std::string name, boost::optional<Position> position, boost::optional<const AreaModel*> area, const Tile* tile)
: PhysicalEntity(tag, name, position, area),
  tile(tile) {}

DrawableEntity::~DrawableEntity() {
}

} // BG

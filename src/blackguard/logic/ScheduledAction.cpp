// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ScheduledAction.h"
#include "blackguard/logic/Actor.h"

namespace BG {

ScheduledAction::ScheduledAction(Actor* actor, long time)
: time(time),
  actor(actor) {}

} // BG

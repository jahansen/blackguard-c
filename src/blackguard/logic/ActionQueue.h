// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ACTIONQUEUE_H
#define ACTIONQUEUE_H

#include "blackguard/logic/ScheduledAction.h"
#include <set>

namespace BG {

class Actor;
class Entity;

class CompareSchedule {
  public:
    bool operator()(ScheduledAction action1, ScheduledAction action2);
};

class ActionQueue
{
  public:
    ActionQueue();

    ScheduledAction next() const;
    bool empty() const {return size() == 0;}
    int size() const {return actionQueue.size();}

    void push(Actor* actor, long time);
    void pop();
    void cancelActionsForEntity(Entity* entity);

  private:
    std::multiset<ScheduledAction, CompareSchedule> actionQueue;
};

} // BG

#endif // ACTIONQUEUE_H

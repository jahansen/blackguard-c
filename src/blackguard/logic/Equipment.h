// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef EQUIPMENT_H
#define EQUIPMENT_H

#include "blackguard/logic/Item.h"

namespace BG {

class Equipment : public Item {
  public:
    Equipment(std::string tag, std::string name, int equipmentType, boost::optional<const AreaModel*> area, boost::optional<Position>, const Tile* itemTile, const Tile* avatarTile);
    virtual ~Equipment();

    virtual std::string getType() const override {return "equipment";}
    virtual BG::Entity* getCopy() const override;

    inline int getEquipmentType() const {return equipmentType;}
    inline const Tile* getAvatarTile() const {return avatarTile;}

  private:
    int equipmentType;
    const Tile* avatarTile;
};

} // BG

#endif // EQUIPMENT_H

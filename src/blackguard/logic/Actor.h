// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ACTOR_H
#define ACTOR_H

#include <string>
#include <memory>
#include <vector>
#include "blackguard/logic/DrawableEntity.h"
#include "blackguard/logic/Direction.h"
#include "blackguard/logic/ActorMemory.h"
#include "blackguard/logic/Inventory.h"
#include "blackguard/logic/EquipmentSlot.h"

namespace BG {

class Tile;
class AreaModel;
class Faction;
class Behaviour;

class Actor : public DrawableEntity {
  public:
    Actor(std::string tag, std::string name, bool alive, const AreaModel* area, Position position, const Faction* faction, const Tile* tile);
    virtual ~Actor();

    virtual std::string getType() const override {return "actor";}
    const Faction* getFaction() const {return faction;}

    inline bool isAlive() const {return alive;}
    inline void setAlive(bool alive) {this->alive = alive;}

    virtual void move(const BG::Direction& direction) =0;
    virtual void pickUp() =0;
    virtual void wait() =0;
    virtual void moveDone() =0;
    virtual void attackDone() =0;
    virtual void die();
    virtual void bumpActor(Actor* actor) =0;
    virtual void bumpWall() =0;

    virtual void onDeath() =0;

    virtual bool waitForInput() const =0;
    inline bool isWaitingForInput() const {return myTurn;}
    virtual void startTurn() =0;
    inline void endTurn() {myTurn = false;}

    void setBehaviour(std::string behaviourTag);

    ActorMemory* getMemory() {return &memory;}
    Inventory* getInventory() {return &inventory;}

    inline void addEquipmentSlot(EquipmentSlot slot) {equipmentSlots.push_back(slot);}

    std::vector<const Equipment*> getEquippedItems() const;
    bool hasItemEquipped(const BG::Equipment* item) const;

  protected:
    bool alive;
    bool myTurn;
    const Faction* faction;
    std::vector<std::unique_ptr<Behaviour>> behaviours;
    Behaviour* currentBehaviour;
    ActorMemory memory;
    Inventory inventory;
    std::vector<EquipmentSlot> equipmentSlots;
};

} // BG

#endif // ACTOR_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "PhysicalEntity.h"

namespace BG {

PhysicalEntity::PhysicalEntity(std::string tag, std::string name, boost::optional<Position> position, boost::optional<const AreaModel*> area)
: Entity(tag, name),
  position(position),
  area(area) {}

PhysicalEntity::PhysicalEntity(std::string tag, std::string name)
: Entity(tag, name) {}

PhysicalEntity::~PhysicalEntity() {
}


} // BG

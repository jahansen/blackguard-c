// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "EquipmentSlot.h"

namespace BG {

EquipmentSlot::EquipmentSlot(int slotType)
: slot(boost::none),
  slotType(slotType) {}

EquipmentSlot::~EquipmentSlot() {}

} // BG

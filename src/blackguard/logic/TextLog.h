// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TEXTLOG_H
#define TEXTLOG_H

#include <vector>
#include "blackguard/logic/TextLogEntry.h"

namespace BG {

class TextLog {
  public:
    TextLog();

    void addEntry(TextLogEntry newEntry) {entries.push_back(newEntry);}
    void addEntry(std::string text, Colour colour=Colour()) {entries.push_back(TextLogEntry(text, colour));}
    std::vector<TextLogEntry> getEntries() const {return entries;}
    std::vector<TextLogEntry> getLatest(int amount=1) const;
    inline int getEntryCount() const {return entries.size();}
    void scrollUp();
    void scrollDown();
    inline int getScrollPosition() const {return scrollPosition;}

  private:
    std::vector<TextLogEntry> entries;
    int scrollPosition;
};

} // BG

#endif // TEXTLOG_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AreaModel.h"
#include "blackguard/core/RandomGenerator.h"
#include "blackguard/core/File.h"
#include "blackguard/core/ErrorCollection.h"
#include "../external/easylogging++.h"

namespace BG {

AreaModel::AreaModel(std::string tag, std::string name)
: Entity(tag, name) {}

AreaModel::~AreaModel() {
  unload();
}

int AreaModel::getRowOffset(Direction direction) const {
  switch (direction) {
    case Direction::NORTH:
    case Direction::NORTH_EAST:
    case Direction::NORTH_WEST:
      return -1;

    case Direction::SOUTH:
    case Direction::SOUTH_EAST:
    case Direction::SOUTH_WEST:
      return 1;

    default:
      return 0;
  }
}

int AreaModel::getColOffset(Direction direction) const {
  switch (direction) {
    case Direction::EAST:
    case Direction::SOUTH_EAST:
    case Direction::NORTH_EAST:
      return 1;

    case Direction::WEST:
    case Direction::SOUTH_WEST:
    case Direction::NORTH_WEST:
      return -1;

    default:
      return 0;
  }
}

boost::optional<const CellModel*> AreaModel::getCell(int row, int col) const {
  return getCell(Position(col, row));
}

boost::optional<const CellModel*> AreaModel::getCell(Position position) const {
  boost::optional<const CellModel*> option;
  if (cellExists(position)) {
    option = &matrix[position.getRow()][position.getCol()];
  }
  return option;
}

boost::optional<const CellModel*> AreaModel::getAdjacentCell(const CellModel* cell, Direction direction) const {
  return getCell(cell->getRowPos() + getRowOffset(direction), cell->getColPos() + getColOffset(direction));
}

boost::optional<const CellModel*> AreaModel::getAdjacentCell(Position position, Direction direction) const {
  return getCell(position.getRow() + getRowOffset(direction), position.getCol() + getColOffset(direction));
}

const CellModel* AreaModel::getRandomCell(BG::RandomGenerator* generator) const {
  return getCell(BG::Position(generator->range(0, getColCount()-1), generator->range(0, getRowCount()-1))).get();
}

const CellModel* AreaModel::getRandomWalkableCell(RandomGenerator* randomGenerator) const {

  std::vector<const CellModel*> walkableCells;
  for (int r = 0; r < getRowCount(); ++r) {
    for (int c = 0; c < getColCount(); ++c) {
      auto currentCell = getCell(Position(c, r)).get();
      if (cellIsWalkable(currentCell)) {
        walkableCells.push_back(currentCell);
      }
    }
  }

  if (walkableCells.empty()) {
    LOG(FATAL) << "Area '" + tag + "' does not contain any walkable cells.";
  }

  auto randomIndex = randomGenerator->range(0, walkableCells.size() - 1);
  return walkableCells.at(randomIndex);
}

Direction AreaModel::getDirection(Position sourcePosition, Position targetPosition) const {
  if (sourcePosition.getX() == targetPosition.getX()) {
    if (sourcePosition.getY() > targetPosition.getY()) return Direction::NORTH;
    else if (sourcePosition.getY() < targetPosition.getY()) return Direction::SOUTH;
    else return Direction::NEUTRAL;
  } else if (sourcePosition.getX() < targetPosition.getX()) {
    if (sourcePosition.getY() > targetPosition.getY()) return Direction::NORTH_EAST;
    else if (sourcePosition.getY() < targetPosition.getY()) return Direction::SOUTH_EAST;
    else return Direction::EAST;
  } else {
    if (sourcePosition.getY() > targetPosition.getY()) return Direction::NORTH_WEST;
    else if (sourcePosition.getY() < targetPosition.getY()) return Direction::SOUTH_WEST;
    else return Direction::WEST;
  }
}

bool AreaModel::cellExists(Position position) const {
  return (position.getRow() < getRowCount() && position.getCol() < getColCount() &&
          position.getRow() >= 0 && position.getCol() >= 0);
}

bool AreaModel::cellsAreAdjacent(const CellModel* cell1, const CellModel* cell2) const {
  return (abs(cell1->getColPos() - cell2->getColPos()) <= 1 &&
          abs(cell1->getRowPos() - cell2->getRowPos()) <= 1);
}

bool AreaModel::lineOfSightExists(Position sourcePosition, Position targetPosition) const {
  auto sourceCell = getCell(sourcePosition.getRow(), sourcePosition.getCol());
  auto targetCell = getCell(targetPosition.getRow(), targetPosition.getCol());

  if (!(sourceCell && targetCell)) {
    return false;
  } else if (sourceCell.get() == targetCell.get()) {
    return true;
  } else if (cellsAreAdjacent(sourceCell.get(), targetCell.get())) {
    return true;
  } else {
    return unobstructedLineExistsBetween(sourceCell.get(), targetCell.get());
  }
}

void AreaModel::modifyCellProperty(Position position, std::string key, std::string value) {
  if (cellExists(position)) {
    matrix[position.getRow()][position.getCol()].setProperty(key, value);
  }
}

void AreaModel::loadAreaFromFile(std::string filename) {
  ErrorCollection errors;
  if (!loadAreaFromFileWithErrors(filename, errors)) {
    errors.printToLog();
    LOG(FATAL) << "Failed to load area from file: '" << filename << "'";
  }
}

bool AreaModel::loadAreaFromFileWithErrors(std::string filename, ErrorCollection& errors) {
  unload();

  File areaFile(filename);
  if (!areaFile.open()) {
    errors.addError("Unable to open area file: '" + filename + "'");
    return false;
  }

  rapidjson::Document json;
  std::string fileData = areaFile.getAll();
  json.Parse<0>(fileData.data());
  areaFile.close();

  errors = validateAreaJSON(json, filename);
  if (!errors.isEmpty()) return false;

  const rapidjson::Value& cellRows = json["cells"];

  for (rapidjson::SizeType r = 0; r < cellRows.Size(); ++r) {
    const rapidjson::Value& cellRow = cellRows[r];
    if (!cellRow.IsArray()) {
      errors.addError(buildJSONErrorString(filename, "cells"));
      return false;
    }
    matrix.push_back(CellRow());
    for (rapidjson::SizeType c = 0; c < cellRow.Size(); ++c) {
      if (cellRow[c].IsObject()) {
        const rapidjson::Value& cell = cellRow[c];
        matrix[r].push_back(CellModel(Position(c, r)));
        for (auto property = cell.MemberBegin(); property != cell.MemberEnd(); ++property) {
          if (property->value.IsString()) {
            matrix[r].back().addProperty(property->name.GetString(), property->value.GetString());
          } else {
            errors.addError("Cell property '" + (std::string)property->name.GetString() + "' must be a string.");
            return false;
          }
        }
      }
    }
  }
  return true;
}

void AreaModel::unload() {
  for (int r = 0; r < (int)matrix.size(); ++r)
    while (!matrix[r].empty()) {
      matrix[r].clear();
    }
  matrix.clear();
}

bool AreaModel::unobstructedLineExistsBetween(const CellModel* sourceCell, const CellModel* targetCell) const {
  int deltaX = abs(sourceCell->getX() - targetCell->getX());
  int deltaY = abs(sourceCell->getY() - targetCell->getY());
  int signX = (sourceCell->getX() < targetCell->getX() ? 1 : -1);
  int signY = (sourceCell->getY() < targetCell->getY() ? 1 : -1);
  int error = deltaX - deltaY;

  const CellModel* curCell = sourceCell;
  int error2 = error;
  for (;;) {
    if (curCell == targetCell) return true;
    if (cellBlocksLineOfSight(curCell)) return false;

    error2 = error * 2;
    if (error2 > -deltaY) {
      error = error - deltaY;
      auto cellOption = getCell(Position(curCell->getX() + signX, curCell->getY()));
      if (cellOption) {
        curCell = cellOption.get();
      } else {
        LOG(ERROR) << "AreaModel::unobstructedLinhExistsBetween(): cannot find cell at ["
                   << curCell->getX() + signX << ", " << curCell->getY() << "]";
        return false;
      }
    }

    if (curCell == targetCell) return true;
    if (cellBlocksLineOfSight(curCell)) return false;

    if (error2 < deltaX) {
      error += deltaX;
      auto cellOption = getCell(Position(curCell->getX(), curCell->getY() + signY));
      if (cellOption) {
        curCell = cellOption.get();
      } else {
        LOG(ERROR) << "AreaModel::unobstructedLineExistsBetween(): cannot find cell at ["
                   << curCell->getX() << ", " << curCell->getY() + signY << "]";
        return false;
      }
    }
  }
}

ErrorCollection AreaModel::validateAreaJSON(rapidjson::Document& json, std::string filename) const {
  ErrorCollection errors;
  if (!json.IsObject()) {
    errors.addError("Area file '" + filename + "' does not have an object as root.");
    return errors;
  }
  if (!json.HasMember("cells") || !json["cells"].IsArray()) {
    errors.addError(buildJSONErrorString(filename, "cells"));
  }

  return errors;
}

std::string AreaModel::buildJSONErrorString(std::string filename, std::string property) {
  return "Area file '" + filename + "' does not contain a valid '" + property + "' property.";
}

} // BG

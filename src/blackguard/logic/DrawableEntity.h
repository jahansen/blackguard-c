// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef DRAWABLEENTITY_H
#define DRAWABLEENTITY_H

#include "blackguard/logic/PhysicalEntity.h"

namespace BG {

class Tile;

class DrawableEntity : public PhysicalEntity {
  public:
    DrawableEntity(std::string tag, std::string name, boost::optional<Position> position, boost::optional<const AreaModel*> area, const Tile* tile);
    virtual ~DrawableEntity();

    const Tile* getTile() const {return tile;}

  protected:
    const Tile* tile;
};

} // BG

#endif // DRAWABLEENTITY_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef SCHEDULEDACTION_H
#define SCHEDULEDACTION_H

namespace BG {

class Actor;

class ScheduledAction {
  public:
    ScheduledAction(Actor* actor, long time);

    long getTime() const {return time;}
    Actor* getActor() const {return actor;}

  private:
    long time;
    Actor* actor;
};

} // BG

#endif // SCHEDULEDACTION_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TILE_H
#define TILE_H

#include <SDL2/SDL.h>
#include <string>
#include "blackguard/logic/Entity.h"

namespace BG {

class Image;

class Tile : public Entity {
  public:
    Tile(std::string tag, int x, int y, int w, int h, Image* tileImage);
    virtual ~Tile();

    virtual Entity* getCopy() const override;
    virtual inline std::string getType() const override {return "tile";}

    inline SDL_Rect getClip() const {return clip;}
    inline Image* getImage() const {return tileImage;}

  private:
    SDL_Rect clip;
    Image* tileImage;
};

bool operator==(const Tile& lhs, const Tile& rhs);

} // BG

#endif // TILE_H

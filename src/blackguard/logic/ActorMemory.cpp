// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "ActorMemory.h"

namespace BG {

ActorMemory::ActorMemory() {
}

ActorMemory::~ActorMemory() {
}

void ActorMemory::memorise(std::string key, const AbstractEntity* entity) {
  memory[key] = std::unique_ptr<AbstractEntity>(entity->getCopy());
}

boost::optional<const AbstractEntity*> ActorMemory::recall(std::string key) const {
  boost::optional<const AbstractEntity*> option;
  auto value = memory.find(key);
  if (value != memory.cend()) {
    option = {value->second.get()};
  }
  return option;
}

void ActorMemory::forget(std::string key) {
  memory.erase(key);
}

} // BG

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PATHNODEQUEUE2_H
#define PATHNODEQUEUE2_H

#include <vector>
#include <memory>
#include <boost/optional.hpp>
#include "blackguard/logic/PathNode.h"

namespace BG {

typedef std::unique_ptr<PathNode> PathNodePtr;

class PathNodeQueue {
  public:
    PathNodeQueue();
    virtual ~PathNodeQueue();

    const PathNode* push(PathNodePtr pathNode);
    PathNodePtr pop();

    boost::optional<PathNode*> find(const CellModel* cell) const;
    inline bool empty() const {return nodeQueue.empty();}

  private:
    std::vector<PathNodePtr> nodeQueue;
};

} // BG

#endif // PATHNODEQUEUE2_H

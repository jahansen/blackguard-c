// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef WORLD_H
#define WORLD_H

#include <memory>
#include <boost/optional.hpp>
#include "blackguard/core/AbstractManager.h"
#include "blackguard/logic/AreaModel.h"

namespace BG {

class ErrorCollection;

class AreaManager : public AbstractManager {
  public:
    AreaManager();
    virtual ~AreaManager();

    void addArea(std::unique_ptr<AreaModel> area);

    const AreaModel* getArea(std::string tag) const;
    boost::optional<const AreaModel*> getAreaOpt(std::string tag) const;
};

} // BG

#endif // WORLD_H

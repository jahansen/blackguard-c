#ifndef FACTIONMANAGER_H
#define FACTIONMANAGER_H

#include <boost/optional.hpp>
#include "blackguard/core/AbstractManager.h"

namespace BG {

class Faction;

class FactionManager : public AbstractManager {
  public:
    FactionManager();
    virtual ~FactionManager();

    void addFaction(std::unique_ptr<Faction> faction);
    void addOneWayRelationship(Faction* faction1, const Faction* faction2, int value);
    void addTwoWayRelationship(Faction* faction1, Faction* faction2, int value);

    Faction* getFaction(std::string tag) const;
    boost::optional<Faction*> getFactionOpt(std::string tag) const;
};

} // BG

#endif // FACTIONMANAGER_H

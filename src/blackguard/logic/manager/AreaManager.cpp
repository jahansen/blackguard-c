// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AreaManager.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/core/ErrorCollection.h"
#include "../external/easylogging++.h"

namespace BG {

AreaManager::AreaManager()
: AbstractManager() {}

AreaManager::~AreaManager() {
}

void AreaManager::addArea(std::unique_ptr<AreaModel> area) {
  add(std::move(area));
}

const AreaModel* AreaManager::getArea(std::string tag) const {
  auto areaModel = getAreaOpt(tag);
  if (!areaModel) {
    LOG(FATAL) << "Cannot find area with tag: '" << tag << "'";
    return nullptr;
  } else {
    return areaModel.get();
  }
}

boost::optional<const AreaModel*> AreaManager::getAreaOpt(std::string tag) const {
  boost::optional<const AreaModel*> option;
  if (auto areaModel = dynamic_cast<const AreaModel*>(get(tag))) {
    option = {areaModel};
  }
  return option;
}

} // BG

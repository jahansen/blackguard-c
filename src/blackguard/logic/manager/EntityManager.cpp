// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "EntityManager.h"
#include "blackguard/logic/AreaModel.h"
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/Item.h"
#include "../external/easylogging++.h"

namespace BG {

EntityManager::EntityManager()
: AbstractManager() {}

void EntityManager::addEntity(std::unique_ptr<PhysicalEntity> entity) {
  add(std::move(entity));
}

PhysicalEntity* EntityManager::getEntity(std::string tag) const {
  auto entity = getEntityOpt(tag);
  if (!entity) {
    LOG(FATAL) << "Cannot find entity with tag: '" << tag << "'";
    return nullptr;
  } else {
    return entity.get();
  }
}

boost::optional<PhysicalEntity*> EntityManager::getEntityOpt(std::string tag) const {
  boost::optional<PhysicalEntity*> option;
  if (auto entity = dynamic_cast<PhysicalEntity*>(get(tag))) {
    option = {entity};
  }
  return option;
}

std::vector<PhysicalEntity*> EntityManager::getEntities() const {
  std::vector<PhysicalEntity*> allEntities;
  for (int i = 0; i < (int)entities.size(); ++i) {
    auto entity = entities.at(i).get();
    if (dynamic_cast<PhysicalEntity*>(entity)) {
      allEntities.push_back(dynamic_cast<PhysicalEntity*>(entity));
    } else {
      LOG(ERROR) << "EntityManager::getEntities(): Unable to cast entity '" << entity->getTag() << "' to PhysicalEntity";
    }
  }
  return allEntities;
}

std::vector<PhysicalEntity*> EntityManager::getEntitiesInCell(const CellModel* cell, const AreaModel* area) const {
  std::vector<BG::PhysicalEntity*> entities = getEntitiesInArea(area);
  std::vector<BG::PhysicalEntity*> entitiesInCell(entities.size());
  auto iter = std::copy_if(entities.begin(), entities.end(), entitiesInCell.begin(),
                           [cell](BG::PhysicalEntity* entity){return entity->getPosition()->getRow() == cell->getRowPos() &&
                                                             entity->getPosition()->getCol() == cell->getColPos();});
  entitiesInCell.resize(std::distance(entitiesInCell.begin(), iter));
  return entitiesInCell;
}

std::vector<PhysicalEntity*> EntityManager::getEntitiesInArea(const AreaModel* area) const {
  std::vector<PhysicalEntity*> entitiesInArea;
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (auto entity = dynamic_cast<PhysicalEntity*>(entities.at(i).get())) {
      if (entity->getArea() && entity->getArea().get()->getTag() == area->getTag()) {
        entitiesInArea.push_back(entity);
      }
    } else {
      LOG(ERROR) << "EntityManager::getEntitiesInArea(): Unable to cast entity '" << entity->getTag() << "' to PhysicalEntity";
    }
  }
  return entitiesInArea;
}

std::vector<Actor*> EntityManager::getActors() const {
  std::vector<Actor*> allActors;
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (auto actorEntity = dynamic_cast<Actor*>(entities.at(i).get())) {
      allActors.push_back(actorEntity);
    }
  }
  return allActors;
}

std::vector<Actor*> EntityManager::getActorsInArea(const AreaModel* area) const {
  std::vector<Actor*> actorsInArea;
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (auto actorEntity = dynamic_cast<Actor*>(entities.at(i).get())) {
      if (actorEntity->getArea() && actorEntity->getArea().get()->getId() == area->getId()) {
        actorsInArea.push_back(actorEntity);
      }
    }
  }
  return actorsInArea;
}

std::vector<Item*> EntityManager::getItemsAtPosition(Position position, const AreaModel* area) const {
  auto items = getItemsInArea(area);
  std::vector<BG::Item*> itemsAtPosition(items.size());
  auto iter = std::copy_if(items.begin(), items.end(), itemsAtPosition.begin(),
                           [position](BG::Item* i){return *i->getPosition() == position;});
  itemsAtPosition.resize(std::distance(itemsAtPosition.begin(), iter));
  return itemsAtPosition;
}

std::vector<Item*> EntityManager::getItemsInArea(const AreaModel* area) const {
  std::vector<Item*> itemsInArea;
  for (int i = 0; i < (int)entities.size(); ++i) {
    if (auto itemEntity = dynamic_cast<Item*>(entities.at(i).get())) {
      auto areaOpt = itemEntity->getArea();
      if (areaOpt && itemEntity->getArea().get()->getId() == area->getId()) {
        itemsInArea.push_back(itemEntity);
      }
    }
  }
  return itemsInArea;
}

void EntityManager::deleteEntity(PhysicalEntity* entity) {
  erase(entity);
}

} // BG

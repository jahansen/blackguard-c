// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "FactionManager.h"
#include "blackguard/logic/Faction.h"
#include "../external/easylogging++.h"

namespace BG {

FactionManager::FactionManager()
: AbstractManager() {}

FactionManager::~FactionManager() {
}

void FactionManager::addFaction(std::unique_ptr<Faction> faction) {
  add(std::move(faction));
}

Faction* FactionManager::getFaction(std::string tag) const {
  auto factionOpt = getFactionOpt(tag);
  if (!factionOpt) {
    LOG(FATAL) << "Cannot find faction with tag '" << tag << "'";
    return nullptr;
  } else {
    return factionOpt.get();
  }
}

boost::optional<Faction*> FactionManager::getFactionOpt(std::string tag) const {
  boost::optional<Faction*> option;
  if (auto faction = dynamic_cast<Faction*>(get(tag))) {
    option = {faction};
  }
  return option;
}

void FactionManager::addOneWayRelationship(Faction* faction1, const Faction* faction2, int value) {
  faction1->addRelationship(faction2, value);
}

void FactionManager::addTwoWayRelationship(Faction* faction1, Faction* faction2, int value) {
  faction1->addRelationship(faction2, value);
  faction2->addRelationship(faction1, value);
}

} // BG

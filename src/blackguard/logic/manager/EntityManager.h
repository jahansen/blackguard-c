// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ENTITYPOOL_H
#define ENTITYPOOL_H

#include <boost/optional.hpp>
#include "blackguard/core/AbstractManager.h"
#include "blackguard/logic/PhysicalEntity.h"

namespace BG {

class AreaModel;
class Actor;
class Item;
class CellModel;

class EntityManager : public AbstractManager {
  public:
    EntityManager();

    void addEntity(std::unique_ptr<PhysicalEntity> entity);

    PhysicalEntity* getEntity(std::string tag) const;
    boost::optional<PhysicalEntity*> getEntityOpt(std::string tag) const;

    std::vector<PhysicalEntity*> getEntities() const;
    std::vector<PhysicalEntity*> getEntitiesInCell(const CellModel* cell, const AreaModel* area) const;
    std::vector<PhysicalEntity*> getEntitiesInArea(const AreaModel* area) const;
    std::vector<Actor*> getActors() const;
    std::vector<Actor*> getActorsInArea(const AreaModel* area) const;
    std::vector<Item*> getItemsAtPosition(Position position, const AreaModel* area) const;
    std::vector<Item*> getItemsInArea(const AreaModel* area) const;

    void deleteEntity(PhysicalEntity* entity);
};

} // BG

#endif // ENTITYPOOL_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "AbstractEntity.h"

namespace BG {

AbstractEntity::AbstractEntity() {
}

AbstractEntity::~AbstractEntity() {
}

} // BG

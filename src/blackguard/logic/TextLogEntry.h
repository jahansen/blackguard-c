// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef TEXTLOGENTRY_H
#define TEXTLOGENTRY_H

#include "blackguard/interface/Colour.h"
#include <string>

namespace BG {

class TextLogEntry {
  public:
    TextLogEntry(std::string text, Colour colour=Colour());
    virtual ~TextLogEntry();

    inline std::string getText() const {return text;}
    inline Colour getColour() const {return colour;}

  private:
    std::string text;
    Colour colour;
};

} // BG

#endif // TEXTLOGENTRY_H

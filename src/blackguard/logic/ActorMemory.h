// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ACTORMEMORY_H
#define ACTORMEMORY_H

#include <unordered_map>
#include <memory>
#include <boost/optional.hpp>
#include "blackguard/logic/AbstractEntity.h"

namespace BG {

class ActorMemory {
  public:
    ActorMemory();
    virtual ~ActorMemory();

    void memorise(std::string key, const AbstractEntity* entity);
    boost::optional<const AbstractEntity*> recall(std::string key) const;
    void forget(std::string key);

  private:
    std::unordered_map<std::string, std::unique_ptr<AbstractEntity>> memory;
};

} // BG

#endif // ACTORMEMORY_H

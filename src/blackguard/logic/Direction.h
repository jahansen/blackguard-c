// Copyright (c) 2012-2013 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef DIRECTION_H_INCLUDED
#define DIRECTION_H_INCLUDED

namespace BG {

  enum Direction {NORTH, EAST, SOUTH, WEST, NORTH_EAST, SOUTH_EAST, SOUTH_WEST, NORTH_WEST, UP, DOWN, NEUTRAL};

} // BG

#endif // DIRECTION_H_INCLUDED

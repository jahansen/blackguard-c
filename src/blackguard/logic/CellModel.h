// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef CELLMODEL_H
#define CELLMODEL_H

#include <unordered_map>
#include "blackguard/logic/Position.h"

namespace BG {

class CellModel {
  public:
    CellModel();
    CellModel(Position position);
    virtual ~CellModel();

    inline int getRowPos() const {return position.getRow();}
    inline int getColPos() const {return position.getCol();}
    inline int getX() const {return position.getX();}
    inline int getY() const {return position.getY();}
    inline Position getPosition() const {return position;}

    void addProperty(std::string key, std::string value);
    void setProperty(std::string key, std::string value);
    std::string getProperty(std::string key) const;
    inline std::unordered_map<std::string, std::string> getProperties() const {return cellProperties;}

  private:
    Position position;
    std::unordered_map<std::string, std::string> cellProperties;
};

bool operator==(const CellModel& lhs, const CellModel& rhs);

} // BG

#endif // CELLMODEL_H

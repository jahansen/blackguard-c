// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>

namespace BG {

class Item;

class Inventory {
  public:
    Inventory();
    virtual ~Inventory();

    void addItem(Item* item);
    void removeItem(Item* item);

    inline std::vector<Item*> getItems() const {return items;}

  private:
    std::vector<Item*> items;
};

} // BG

#endif // INVENTORY_H

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "Faction.h"

namespace BG {

Faction::Faction(std::string tag, std::string name)
: Entity(tag, name) {}

Faction::~Faction() {
}

BG::Entity* Faction::getCopy() const {
  auto faction = new Faction(tag, name);
  faction->id = id;
  faction->relationships = relationships;
  return faction;
}

int Faction::getRelationshipTo(Faction* faction) const {
  auto value = relationships.find(faction->getId());
  if (value == relationships.cend()) {
    return 0;
  } else {
    return value->second;
  }
}

bool Faction::hasRelationshipTo(Faction* faction) const {
  auto value = relationships.find(faction->getId());
  return !(value == relationships.cend());
}

void Faction::addRelationship(const Faction* faction, int value) {
  relationships.insert({{faction->getId(), value}});
}

bool Faction::isFriendlyTo(Faction* faction) const {
  return getRelationshipTo(faction) > 0;
}

bool Faction::isNeutralTo(Faction* faction) const {
  return getRelationshipTo(faction) == 0;
}

bool Faction::isHostileTo(Faction* faction) const {
  return getRelationshipTo(faction) < 0;
}

bool Faction::isNotFriendlyTo(Faction* faction) const {
  return getRelationshipTo(faction) <= 0;
}

bool Faction::isNotNeutralTo(Faction* faction) const {
  return getRelationshipTo(faction) != 0;
}

bool Faction::isNotHostileTo(Faction* faction) const {
  return getRelationshipTo(faction) >= 0;
}

} // BG

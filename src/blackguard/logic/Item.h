// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef ITEM_H
#define ITEM_H

#include "blackguard/logic/DrawableEntity.h"

namespace BG {

class Item : public DrawableEntity {
  public:
    Item(std::string tag, std::string name, boost::optional<const AreaModel*> area, boost::optional<Position> position, const Tile* tile);
    virtual ~Item();

    virtual std::string getType() const override {return "item";}
    virtual BG::Entity* getCopy() const override;

  private:
};

} // BG

#endif // ITEM_H

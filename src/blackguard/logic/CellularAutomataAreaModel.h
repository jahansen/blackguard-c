// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef CELLULARAUTOMATAAREAMODEL_H
#define CELLULARAUTOMATAAREAMODEL_H

#include <vector>
#include "blackguard/logic/AreaModel.h"

class RandomGenerator;

namespace BG {

class CellularAutomataAreaModel : public AreaModel {
  public:
    CellularAutomataAreaModel(std::string tag, std::string name);
    virtual ~CellularAutomataAreaModel();

  protected:
    typedef std::vector<std::vector<int>> IntMatrix;
    void build(RandomGenerator* generator, int rows, int columns, int initialWallFrequency=40, int passageSize=3, int smoothness=5);
    virtual void buildFinalArea(const IntMatrix& cellMatrix) =0;

  private:
    static const int BIRTH_LIMIT = 4;
    void initialiseSimulation(RandomGenerator* generator, IntMatrix& cellMatrix, int initialAliveProbability);
    void runSimulation(IntMatrix& cellMatrix, int deathLimit);
    int countAdjacentAlive(int r, int c, const IntMatrix& cellMatrix) const;
    bool cellIsOnBorder(int r, int c, const IntMatrix& cellMatrix) const;
    void fillDisjointAreas(IntMatrix& cellMatrix);
    void floodFill(IntMatrix& cellMatrix, int startRow, int startCol, int value);
};

} // BG

#endif // CELLULARAUTOMATAAREAMODEL_H

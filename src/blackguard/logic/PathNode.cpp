// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "PathNode.h"

namespace BG {

PathNode::PathNode(const CellModel* cell, int distFromStart, int distToGoal)
: cell(cell),
  distFromStart(distFromStart),
  distToGoal(distToGoal) {}

PathNode::PathNode(const CellModel* cell, const PathNode* parent, int distFromStart, int distToGoal)
: parent({parent}),
  cell(cell),
  distFromStart(distFromStart),
  distToGoal(distToGoal) {}

PathNode::~PathNode() {
}

} // BG

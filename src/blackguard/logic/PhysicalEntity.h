// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PHYSICALENTITY_H
#define PHYSICALENTITY_H

#include "blackguard/logic/Entity.h"
#include "blackguard/logic/Position.h"
#include <boost/optional.hpp>

namespace BG {

class AreaModel;

class PhysicalEntity : public Entity {
  public:
    PhysicalEntity(std::string tag, std::string name, boost::optional<Position> position, boost::optional<const AreaModel*> area);
    PhysicalEntity(std::string tag, std::string name);
    virtual ~PhysicalEntity();

    inline boost::optional<Position> getPosition() const {return position;}
    boost::optional<const AreaModel*> getArea() const {return area;}

    inline void setPosition(Position position) {this->position = position;}
    inline void setArea(const AreaModel* area) {this->area = {area};}
    inline void removeFromWorld() {position = boost::none; area = boost::none;}

  protected:
    boost::optional<Position> position;
    boost::optional<const AreaModel*> area;

};

} // BG

#endif // PHYSICALENTITY_H

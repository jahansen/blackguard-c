// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "TextLog.h"
#include "../external/easylogging++.h"

namespace BG {

TextLog::TextLog()
: scrollPosition(1) {}

std::vector<TextLogEntry> TextLog::getLatest(int amount) const {
  if (amount > 0) {
    return std::vector<TextLogEntry>(entries.end() - std::min((int)entries.size(), amount), entries.end());
  } else {
    LOG(WARNING) << "TextLog.getLatest() received invalid negative argument";
    return std::vector<TextLogEntry>();
  }
}

void TextLog::scrollUp() {
  if (getEntryCount() <= scrollPosition) {
    scrollPosition = getEntryCount();
  } else {
    ++scrollPosition;
  }
}

void TextLog::scrollDown() {
  scrollPosition <= 1 ? scrollPosition = 1 : --scrollPosition;
}

} // BG

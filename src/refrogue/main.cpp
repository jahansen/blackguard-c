// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "blackguard/core/Window.h"
#include "refrogue/RefRogue.h"
#include "../external/easylogging++.h"
#include <memory>

_INITIALIZE_EASYLOGGINGPP

void initRefRogue(int argc, char** argv) {
  auto refRogue = std::unique_ptr<RefRogue>(new RefRogue(argc, argv));
  if (!refRogue->initialise(BG::Application::DEFAULT_WIDTH, BG::Application::DEFAULT_HEIGHT, "RefRogue")) {
    LOG(ERROR) << "Failed to successfully initialise RefRogue.";
    exit(EXIT_FAILURE);
  }

  refRogue->run();
}

int main(int argc, char** argv) {
  initRefRogue(argc, argv);

  return EXIT_SUCCESS;
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef REFROGUEAREAPANEL_H
#define REFROGUEAREAPANEL_H

#include "blackguard/interface/SubPanel.h"
#include "blackguard/logic/Position.h"
#include <functional>

namespace BG {
  class AreaModel;
  class FontManager;
  class TilesetManager;
  class EntityManager;
  class Tile;
}

class PC;

class RefRogueAreaPanel : public BG::SubPanel {
  public:
    RefRogueAreaPanel(std::string tag, Panel* parent, BG::FontManager* fontManager,
                      BG::TilesetManager* tilesetManager, BG::EntityManager* entityManager, const PC* player);

    virtual void drawSelf() override;

  private:
    static const int VIEWPORT_COLS = 25;
    static const int VIEWPORT_ROWS = 21;

    static const int TILE_WIDTH = 32;
    static const int TILE_HEIGHT = 32;
    static const int PANEL_WIDTH = VIEWPORT_COLS * TILE_WIDTH;
    static const int PANEL_HEIGHT = VIEWPORT_ROWS * TILE_HEIGHT;
    static const int OFFSET_X = 0;
    static const int OFFSET_Y = 0;

    BG::FontManager* fontManager;
    BG::TilesetManager* tilesetManager;
    BG::EntityManager* entityManager;

    const PC* player;

    void drawTile(const BG::Tile* tile, const BG::Position position, int rowOffset, int colOffset);
};

#endif // REFROGUEAREAPANEL_H

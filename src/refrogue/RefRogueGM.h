// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef REFROGUEGM_H
#define REFROGUEGM_H

#include "blackguard/core/GameMaster.h"

class RefRogueGM : public BG::GameMaster {
  public:
    RefRogueGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window);
    virtual ~RefRogueGM();

    bool cellIsPassable(const BG::CellModel* cell) const override;
    inline int getCurrentDepth() const {return currentDepth;}
    bool shouldQuit() const;

    const BG::AreaModel* generateNewArea();
    void transitionActorToNewArea(BG::Actor* actor);

  private:
    static const int MAX_DEPTH = 3;

    int currentDepth;

    bool maxDepthReached() const {return currentDepth >= MAX_DEPTH;}
    bool maxDepthExceeded() const {return currentDepth >= MAX_DEPTH + 1;}
    std::string getCurrentLevelTag() const;
    std::string getCurrentLevelName() const;
};

#endif // REFROGUEGM_H

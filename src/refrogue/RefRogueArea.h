// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef REFROGUEAREA_H
#define REFROGUEAREA_H

#include "blackguard/logic/CellularAutomataAreaModel.h"

namespace BG {
  class RandomGenerator;
}

class RefRogueArea : public BG::CellularAutomataAreaModel {
  public:
    RefRogueArea(std::string tag, std::string name);
    virtual ~RefRogueArea();

    virtual Entity* getCopy() const override;

    static RefRogueArea* create(std::string tag, std::string name, BG::RandomGenerator* generator);

    virtual std::vector<const BG::CellModel*> getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const override;
    virtual bool cellBlocksLineOfSight(const BG::CellModel* cell) const override;
    virtual bool cellIsWalkable(const BG::CellModel* cell) const override;
    virtual bool cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const override;
    virtual int getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const override;
    virtual int estimateDistanceBetweenCells(const BG::CellModel* startCell, const BG::CellModel* goalCell) const override;

  private:
    static const int ROWS = 25;
    static const int COLS = 25;

    void buildFinalArea(const IntMatrix& cellMatrix) override;

    void generateExit(BG::RandomGenerator* generator);
};

#endif // REFROGUEAREA_H

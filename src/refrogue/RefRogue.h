// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef REFROGUE_H
#define REFROGUE_H

#include "blackguard/core/Application.h"
#include "blackguard/logic/TextLog.h"

namespace BG {
  class Tile;
}

class RefRogueAreaPanel;

class RefRogue : public BG::Application {
  public:
    RefRogue(int argc, char** arg);
    virtual ~RefRogue();

  private:
    static const int RefRogueFPS = 30;

    RefRogueAreaPanel* areaPanel;
    BG::TextLog mainLog;

    void loadAreas() override;
    void buildGame() override;
    void buildScreen() override;
};

#endif // REFROGUE_H

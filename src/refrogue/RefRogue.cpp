// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "RefRogue.h"
#include "RefRogueGM.h"
#include "RefRogueArea.h"
#include "RefRogueAreaPanel.h"
#include "PC.h"
#include "FieldContext.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/logic/Faction.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/core/Window.h"
#include "../external/easylogging++.h"

RefRogue::RefRogue(int argc, char** arg)
: Application(RefRogueFPS, argc, argv) {}

RefRogue::~RefRogue() {}

void RefRogue::loadAreas() {
//  auto area = std::unique_ptr<RefRogueArea>(new RefRogueArea("level1", "Level 1"));
//  area->loadAreaFromFile(getAreaDirectory() + "/testArea.json");
//  world.addArea(std::move(area));
}

void RefRogue::buildGame() {
  auto actorTileset = tilesetManager.getTileset("actors");

  gameMaster = std::unique_ptr<RefRogueGM>(new RefRogueGM(&entityManager, &world, &fontManager, &mainLog, window.get()));
  auto refRogueGM = dynamic_cast<RefRogueGM*>(gameMaster.get());

  factionManager.addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("player", "Player")));
  auto playerFaction = factionManager.getFaction("player");

  auto playerTile = actorTileset->getTile("player");

  auto initialArea = refRogueGM->generateNewArea();

  entityManager.addEntity(std::unique_ptr<PC>(new PC(initialArea, initialArea->getRandomWalkableCell(refRogueGM->getGenerator())->getPosition(), playerFaction, playerTile, refRogueGM)));

  refRogueGM->scheduleInitialTurns();

  auto player = dynamic_cast<BaseActor*>(entityManager.getEntity("player"));
  refRogueGM->addContext(std::unique_ptr<FieldContext>(new FieldContext(refRogueGM, player)));
  refRogueGM->setContext("field");
}

void RefRogue::buildScreen() {
  auto player = dynamic_cast<PC*>(entityManager.getEntity("player"));
  areaPanel = new RefRogueAreaPanel("area-main", window->getMasterPanel(), &fontManager, &tilesetManager, &entityManager, player);
  window->getMasterPanel()->addChild(areaPanel);
}

// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "PC.h"
#include "RefRogueGM.h"
#include "blackguard/logic/AreaModel.h"
#include "../external/easylogging++.h"

PC::PC(const BG::AreaModel* area, BG::Position position, const BG::Faction* faction, const BG::Tile* tile, RefRogueGM* gm)
: BaseActor("player", "Player", true, area, position, faction, tile, gm) {}

PC::~PC() {}

BG::Entity* PC::getCopy() const {
  auto copy = new PC(area.get(), position.get(), faction, tile, gm);
  copy->id = id;
  return copy;
}

void PC::move(const BG::Direction& direction) {
  gm->moveActor(this, area.get(), direction);
}

void PC::pickUp() {

}

void PC::wait() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeSecondsFromNow(5));
}


void PC::moveDone() {
  gm->scheduleNextTurn(this, gm->getWorldCalendar()->getTimeMillisecondsFromNow(500));
}

void PC::attackDone() {

}

void PC::bumpActor(BG::Actor* actor) {

}

void PC::bumpWall() {

}

void PC::use() {
  auto currentCell = area.get()->getCell(position.get()).get();

  // If the player uses the stairs, generate a new level and transition the player to it
  if (currentCell->getProperty("tile") == "stairs") {
    gm->transitionActorToNewArea(this);
  }
}

void PC::onDeath() {

}

void PC::startTurn() {
  myTurn = true;
  gm->waitForInput();
}

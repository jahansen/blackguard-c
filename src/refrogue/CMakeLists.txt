# RefRogue

add_subdirectory(data)

set(refrogue_SRCS
  main.cpp
  RefRogue.cpp
  RefRogueAreaPanel.cpp
  RefRogueGM.cpp
  RefRogueArea.cpp
  PC.cpp
  BaseActor.cpp
  FieldContext.cpp
  BaseContext.cpp
)

include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_SOURCE_DIR}/external)

add_executable(refrogue ${refrogue_SRCS})
target_link_libraries(refrogue blackguard)

install(TARGETS refrogue DESTINATION ${CMAKE_INSTALL_PREFIX})

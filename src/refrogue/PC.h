// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#ifndef PC_H
#define PC_H

#include "BaseActor.h"

class RefRogueGM;

class PC : public BaseActor {
  public:
    PC(const BG::AreaModel* area, BG::Position postiion, const BG::Faction* faction, const BG::Tile* tile, RefRogueGM* gm);
    virtual ~PC();

    virtual Entity* getCopy() const override;

    virtual inline bool waitForInput() const override {return true;}

    virtual void move(const BG::Direction &direction) override;
    virtual void pickUp() override;
    void wait() override;
    virtual void moveDone() override;
    virtual void attackDone() override;
    virtual void bumpActor(BG::Actor* actor) override;
    virtual void bumpWall() override;
    virtual void use() override;

    virtual void onDeath() override;

    virtual void startTurn() override;
};

#endif // PC_H

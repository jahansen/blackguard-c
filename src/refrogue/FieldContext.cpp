// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "FieldContext.h"
#include "PC.h"
#include "RefRogueGM.h"

FieldContext::FieldContext(RefRogueGM* gm, BaseActor* player)
: BaseContext("field", gm, player) {}

FieldContext::~FieldContext() {}

void FieldContext::handleInput(const SDL_Event &event, bool &quit) {
  if (event.type == SDL_KEYDOWN && !event.key.repeat) {
    switch (event.key.keysym.sym) {
      case SDLK_KP_8:
        player->move(BG::Direction::NORTH);
        break;

      case SDLK_KP_6:
        player->move(BG::Direction::EAST);
        break;

      case SDLK_KP_2:
        player->move(BG::Direction::SOUTH);
        break;

      case SDLK_KP_4:
        player->move(BG::Direction::WEST);
        break;

      case SDLK_KP_9:
        player->move(BG::Direction::NORTH_EAST);
        break;

      case SDLK_KP_3:
        player->move(BG::Direction::SOUTH_EAST);
        break;

      case SDLK_KP_1:
        player->move(BG::Direction::SOUTH_WEST);
        break;

      case SDLK_KP_7:
        player->move(BG::Direction::NORTH_WEST);
        break;

      case SDLK_KP_5:
        player->wait();
        break;

      case SDLK_z:
        player->use();
        quit = gm->shouldQuit();
        break;

      case SDLK_PAGEUP:
//        gm->scrollLogUp();
        break;

      case SDLK_PAGEDOWN:
//        gm->scrollLogDown();
        break;

      case SDLK_g:
//        player->pickUp();
        break;

      case SDLK_d:
//        player->dropItem();
        break;

      case SDLK_i:
//        gm->openInventory(player);
        break;

      case SDLK_q:
        quit = true;
        break;

      default:
        break;
    }
  }
}


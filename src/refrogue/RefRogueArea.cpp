// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "RefRogueArea.h"
#include "blackguard/logic/Actor.h"

RefRogueArea::RefRogueArea(std::string tag, std::string name)
: BG::CellularAutomataAreaModel(tag, name) {}

RefRogueArea::~RefRogueArea() {}

BG::Entity* RefRogueArea::getCopy() const {
  auto area = new RefRogueArea(tag, name);
  area->id = id;
  area->matrix = matrix;
  return area;
}

RefRogueArea* RefRogueArea::create(std::string tag, std::string name, BG::RandomGenerator* generator) {
  auto area = new RefRogueArea(tag, name);
  area->build(generator, ROWS, COLS);

  area->generateExit(generator);

  return area;
}

std::vector<const BG::CellModel*> RefRogueArea::getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  auto northCell = getAdjacentCell(cell, BG::Direction::NORTH);
  auto eastCell = getAdjacentCell(cell, BG::Direction::EAST);
  auto southCell = getAdjacentCell(cell, BG::Direction::SOUTH);
  auto westCell = getAdjacentCell(cell, BG::Direction::WEST);
  auto neCell = getAdjacentCell(cell, BG::Direction::NORTH_EAST);
  auto seCell = getAdjacentCell(cell, BG::Direction::SOUTH_EAST);
  auto swCell = getAdjacentCell(cell, BG::Direction::SOUTH_WEST);
  auto nwCell = getAdjacentCell(cell, BG::Direction::NORTH_WEST);

  std::vector<const BG::CellModel*> passableCells;
  if (northCell && cellIsPassable(northCell.get(), goalCell, actorsInArea)) passableCells.push_back(northCell.get());
  if (eastCell && cellIsPassable(eastCell.get(), goalCell, actorsInArea)) passableCells.push_back(eastCell.get());
  if (southCell && cellIsPassable(southCell.get(), goalCell, actorsInArea)) passableCells.push_back(southCell.get());
  if (westCell && cellIsPassable(westCell.get(), goalCell, actorsInArea)) passableCells.push_back(westCell.get());
  if (neCell && cellIsPassable(neCell.get(), goalCell, actorsInArea)) passableCells.push_back(neCell.get());
  if (seCell && cellIsPassable(seCell.get(), goalCell, actorsInArea)) passableCells.push_back(seCell.get());
  if (swCell && cellIsPassable(swCell.get(), goalCell, actorsInArea)) passableCells.push_back(swCell.get());
  if (nwCell && cellIsPassable(nwCell.get(), goalCell, actorsInArea)) passableCells.push_back(nwCell.get());

  return passableCells;
}

bool RefRogueArea::cellBlocksLineOfSight(const BG::CellModel* cell) const {
  return cell->getProperty("passable") == "false";
}

bool RefRogueArea::cellIsWalkable(const BG::CellModel* cell) const {
  return (cell->getProperty("passable") == "true");
}

bool RefRogueArea::cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  if (cell == goalCell) return true; // The goal cell is always considered passable (otherwise a path to it will never be found)
  if (cell->getProperty("passable") == "false") return false;

  for (auto actor : actorsInArea) {
    if (*actor->getPosition() == cell->getPosition()) return false;
  }
  return true;
}

int RefRogueArea::getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const {
  auto travelDirection = getDirection(cell->getPosition(), neighbourCell->getPosition());

  switch (travelDirection) {
    case BG::Direction::NORTH:
    case BG::Direction::EAST:
    case BG::Direction::SOUTH:
    case BG::Direction::WEST:
      return 10;

    case BG::Direction::NORTH_EAST:
    case BG::Direction::SOUTH_EAST:
    case BG::Direction::SOUTH_WEST:
    case BG::Direction::NORTH_WEST:
      return 14;

    default:
      return 10;
  }
}

int RefRogueArea::estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const {
  // Chebyshev distance with straight movement cost of 10, diagonal of 14
  const int DS = 10;
  const int DD = 14;
  int dx = abs(start->getX() - goal->getX());
  int dy = abs(start->getY() - goal->getY());
  return DS * (dx + dy) + (DD - 2 * DS) * std::min(dx, dy);
}

void RefRogueArea::buildFinalArea(const IntMatrix& cellMatrix) {
  for (int r = 0; r < (int)cellMatrix.size(); ++r) {
    matrix.push_back(BG::CellRow());
    for (int c = 0; c < (int)cellMatrix[0].size(); ++c) {
      matrix[r].push_back(BG::CellModel(BG::Position(c, r)));

      if (cellMatrix[r][c]) {
        matrix[r].back().setProperty("tile", "wall");
        matrix[r].back().setProperty("passable", "false");
      } else {
        matrix[r].back().setProperty("tile", "floor");
        matrix[r].back().setProperty("passable", "true");
      }
    }
  }
}

void RefRogueArea::generateExit(BG::RandomGenerator* generator) {
  auto exitPosition = getRandomWalkableCell(generator)->getPosition();
  modifyCellProperty(exitPosition, "tile", "stairs");
}

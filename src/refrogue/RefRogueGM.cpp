// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "RefRogueGM.h"
#include "BaseActor.h"
#include "RefRogueArea.h"
#include "blackguard/logic/manager/AreaManager.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "../external/easylogging++.h"
#include <boost/format.hpp>

RefRogueGM::RefRogueGM(BG::EntityManager* entityManager, BG::AreaManager* world, BG::FontManager* fontManager, BG::TextLog* log, BG::Window* window)
: BG::GameMaster(entityManager, world, fontManager, log, window),
  currentDepth(0) {}

RefRogueGM::~RefRogueGM() {}

bool RefRogueGM::cellIsPassable(const BG::CellModel* cell) const {
  return cell->getProperty("passable") == "true";
}

bool RefRogueGM::shouldQuit() const {
  return maxDepthExceeded();
}

const BG::AreaModel* RefRogueGM::generateNewArea() {

  ++currentDepth;

  auto levelTag= getCurrentLevelTag();
  auto area = std::unique_ptr<RefRogueArea>(RefRogueArea::create(levelTag, getCurrentLevelName(), &generator));
  world->addArea(std::move(area));

  return world->getArea(levelTag);
}

void RefRogueGM::transitionActorToNewArea(BG::Actor* actor) {
  if (!maxDepthReached()) {
    auto newArea = generateNewArea();
    moveActorToArea(actor, newArea, newArea->getRandomWalkableCell(&generator));
  } else {
    ++currentDepth;
  }
}

std::string RefRogueGM::getCurrentLevelTag() const {
  return boost::str(boost::format("level%1%") % currentDepth);
}

std::string RefRogueGM::getCurrentLevelName() const {
  return boost::str(boost::format("Level %1%") % currentDepth);
}

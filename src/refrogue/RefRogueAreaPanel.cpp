// Copyright (c) 2013-2014 Joshua A. Hansen
// Distributed under the MIT software license.
// See the accompanying file LICENSE.txt or http://www.opensource.org/licenses/mit-license.php

#include "RefRogueAreaPanel.h"
#include "blackguard/core/ErrorCollection.h"
#include "blackguard/interface/manager/TilesetManager.h"
#include "blackguard/interface/Viewport.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/logic/manager/EntityManager.h"
#include "blackguard/logic/Actor.h"
#include "blackguard/logic/AreaModel.h"
#include "PC.h"
#include "../external/easylogging++.h"

RefRogueAreaPanel::RefRogueAreaPanel(std::string tag, Panel* parent, BG::FontManager* fontManager,
                                     BG::TilesetManager* tilesetManager, BG::EntityManager* entityManager, const PC* player)
  : BG::SubPanel(tag, BG::Coordinates(OFFSET_X, OFFSET_Y), PANEL_WIDTH, PANEL_HEIGHT, parent),
    fontManager(fontManager),
    tilesetManager(tilesetManager),
    entityManager(entityManager),
    player(player) {}

void RefRogueAreaPanel::drawSelf() {
  auto worldTileset = tilesetManager->getTileset("world");

  auto currentArea = player->getArea().get();
  auto viewport = BG::Viewport(currentArea, player->getPosition().get(), VIEWPORT_COLS, VIEWPORT_ROWS);

  viewport.forEachVisibleCell([this, currentArea, worldTileset](int r, int c, BG::Position vpOffset) {
    auto curCell = currentArea->getCell(r, c).get();
    auto curTile = worldTileset->getTile(curCell->getProperty("tile"));
    drawTile(curTile, curCell->getPosition(), -vpOffset.getRow(), -vpOffset.getCol());
  });

  auto actorsInArea = entityManager->getActorsInArea(currentArea);
  for (auto actor : actorsInArea) {
    if (viewport.positionIsWithinViewport(actor->getPosition().get())) {
      auto entity = dynamic_cast<BG::DrawableEntity*>(actor);
      drawTile(entity->getTile(), entity->getPosition().get(), -viewport.getOffset().getRow(), -viewport.getOffset().getCol());
    }
  }
}

void RefRogueAreaPanel::drawTile(const BG::Tile* tile, const BG::Position position, int rowOffset, int colOffset) {
  SDL_Rect renderTarget = {(short int)((position.getCol() + colOffset) * TILE_WIDTH), (short int)((position.getRow() + rowOffset) * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
  SDL_Rect tileClip = tile->getClip();
  SDL_RenderCopy(parent->getRenderer(), tile->getImage()->getTexture(), &tileClip, &renderTarget);
}





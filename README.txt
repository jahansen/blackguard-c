Blackguard is a library for creating roguelike-style 2D role-playing games, written in C++.

Development Environment Setup
-----------------------------
Blackguard is currently being developed on Linux and is untested on other platforms. It uses features from C++11, and must be compiled with the -std=c++11 flag.

The following dependencies are required:
- SDL2 (http://www.libsdl.org/download-2.0.php)
- SD2-image (http://www.libsdl.org/projects/SDL_image/)
- SDL2-mixer (http://www.libsdl.org/projects/SDL_mixer/)
- SDL2-ttf (http://www.libsdl.org/projects/SDL_ttf/)
- googletest (https://code.google.com/p/googletest/)
- Boost (http://www.boost.org)

Acknowledgements
----------------
Placeholder graphics used in this program are from the 'Crawl Stone Soup' public domain tileset: https://code.google.com/p/crawl-tiles/

#include "gtest/gtest.h"
#include "blackguard/logic/Position.h"

class PositionTest : public testing::Test {
  protected:
    virtual void SetUp() {
    }
};

TEST_F(PositionTest, testIsBetweenInclusive) {
  BG::Position p1(0, 0);
  BG::Position p2(10, 10);

  ASSERT_TRUE(BG::Position(0, 0).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(10, 10).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(1, 0).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(0, 1).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(9, 10).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(10, 9).isBetweenInclusive(p1, p2));
  ASSERT_TRUE(BG::Position(5, 5).isBetweenInclusive(p1, p2));

  ASSERT_FALSE(BG::Position(-1, -1).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(-1, 0).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(0, -1).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(11, 11).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(10, 11).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(11, 10).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(5, 20).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(20, 5).isBetweenInclusive(p1, p2));
  ASSERT_FALSE(BG::Position(20, 20).isBetweenInclusive(p1, p2));
}

TEST_F(PositionTest, testIsBetweenExclusive) {
  BG::Position p1(0, 0);
  BG::Position p2(10, 10);

  ASSERT_TRUE(BG::Position(1, 1).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(1, 2).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(2, 1).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(9, 9).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(8, 9).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(9, 8).isBetweenExclusive(p1, p2));
  ASSERT_TRUE(BG::Position(5, 5).isBetweenExclusive(p1, p2));

  ASSERT_FALSE(BG::Position(0, 0).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(1, 0).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(0, 1).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(10, 10).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(10, 9).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(9, 10).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(5, 20).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(20, 5).isBetweenExclusive(p1, p2));
  ASSERT_FALSE(BG::Position(20, 20).isBetweenExclusive(p1, p2));
}

#include "gtest/gtest.h"
#include "blackguard/core/File.h"
#include <memory>

class FileTest: public testing::Test {
  protected:
    virtual void SetUp() {
      intFile = std::unique_ptr<BG::File>(new BG::File("data/intTestData.txt"));
      stringFile = std::unique_ptr<BG::File>(new BG::File("data/stringTestData.txt"));
    }

    virtual void TearDown() {

    }

    std::unique_ptr<BG::File> intFile;
    std::unique_ptr<BG::File> stringFile;
};

TEST_F(FileTest, testOpenFile) {
  intFile->open();
  ASSERT_TRUE(intFile->isOpen());
}

TEST_F(FileTest, testCloseFile) {
  intFile->open();
  intFile->close();
  ASSERT_FALSE(intFile->isOpen());
}

TEST_F(FileTest, testGetNextInt) {
  intFile->open();
  for (int i = -1; i <= 10; ++i) {
    EXPECT_EQ(intFile->getNextInt(), i);
  }
}

TEST_F(FileTest, testGetNextIntThrowsExceptionOnFail) {
  stringFile->open();
  ASSERT_THROW(stringFile->getNextInt(), std::exception);
}

TEST_F(FileTest, testGetAll) {
  stringFile->open();
  EXPECT_EQ(stringFile->getAll(), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget nulla ullamcorper, ullamcorper nisi eget, congue ipsum. Fusce vulputate ullamcorper lobortis.\n"
"Maecenas vulputate egestas massa vel facilisis. Donec nec eleifend nibh. Sed vulputate posuere dui et adipiscing. Nulla mauris enim, blandit quis consectetur at, adipiscing at nisi.\n"
"Pellentesque tincidunt metus at adipiscing eleifend. Donec convallis eleifend ante. Nunc sapien arcu, commodo rutrum diam a, auctor viverra metus. Ut vel auctor leo. Curabitur consequat vulputate pulvinar. Integer sit amet sapien lacus.\n");
}

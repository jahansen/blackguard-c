#include "gtest/gtest.h"
#include "stubs/AreaStub.h"
#include "stubs/Area8ConnectedStub.h"
#include "stubs/AreaTerrainStub.h"
#include "blackguard/logic/Pathfinder.h"
#include "blackguard/core/ErrorCollection.h"

class PathfinderTest : public testing::Test {
  protected:
    virtual void SetUp() {
    }
};

TEST_F(PathfinderTest, testFindsShortestPathOn4ConnectedGrid) {
  AreaStub area;
  BG::ErrorCollection errors;
  area.loadAreaFromFileWithErrors("data/pathfinderTestData.bga", errors);
  ASSERT_TRUE(errors.isEmpty());

  auto pathfinder = BG::Pathfinder();
  auto startCell = area.getCell(3, 1).get();
  auto goalCell = area.getCell(5, 9).get();
  auto path = pathfinder.getShortestPath(&area, startCell, goalCell);
  EXPECT_EQ(path.size(), 15);
  EXPECT_EQ(path.at(0), startCell);
}

TEST_F(PathfinderTest, testFindsShortestPathOn8ConnectedGridDiagonalEquals10) {
  Area8ConnectedStub area;
  BG::ErrorCollection errors;
  area.loadAreaFromFileWithErrors("data/pathfinderTestData.bga", errors);
  ASSERT_TRUE(errors.isEmpty());

  auto pathfinder = BG::Pathfinder();
  auto startCell = area.getCell(3, 1).get();
  auto goalCell = area.getCell(5, 9).get();
  auto path = pathfinder.getShortestPath(&area, startCell, goalCell);

  // Allowing diagonal movement shortens the path
  EXPECT_EQ(path.size(), 9);
  EXPECT_EQ(path.at(0), startCell);
}

TEST_F(PathfinderTest, testFindsShortestPathOnVariableTerrain) {
  AreaTerrainStub area;
  BG::ErrorCollection errors;
  area.loadAreaFromFileWithErrors("data/pathfinderTestData.bga", errors);
  ASSERT_TRUE(errors.isEmpty());

  auto pathfinder = BG::Pathfinder();
  auto startCell = area.getCell(3, 1).get();
  auto goalCell = area.getCell(5, 9).get();
  auto path = pathfinder.getShortestPath(&area, startCell, goalCell);

  // The shortest distance path now contains difficult terrain, so it is quicker to go around
  EXPECT_EQ(path.size(), 19);
  EXPECT_EQ(path.at(0), startCell);
}

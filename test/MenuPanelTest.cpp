#include "gtest/gtest.h"
#include "blackguard/interface/MenuPanel.h"
#include "blackguard/interface/MasterPanel.h"
#include "stubs/ListPanelItemStub.h"

class MenuPanelTest : public testing::Test {
  protected:
    virtual void SetUp() {
      masterPanel = std::unique_ptr<BG::MasterPanel>(new BG::MasterPanel("test-master", 1000, 2000, nullptr, nullptr));
      menuPanel = std::unique_ptr<BG::MenuPanel>(new BG::MenuPanel("test-menu-panel", BG::Coordinates(100, 200), 300, 400, masterPanel.get()));

      menuPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(menuPanel.get())));
      menuPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(menuPanel.get())));
      menuPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(menuPanel.get())));
    }

    std::unique_ptr<BG::MenuPanel> menuPanel;
    std::unique_ptr<BG::MasterPanel> masterPanel;
};

TEST_F(MenuPanelTest, testSelectNextInitiallySelectsFirst) {
  menuPanel->selectNext();
  EXPECT_EQ(menuPanel->getSelectedIndex(), 0);
}

TEST_F(MenuPanelTest, testSelectNextLoopsToFirst) {
  menuPanel->selectNext();
  menuPanel->selectNext();
  menuPanel->selectNext();
  menuPanel->selectNext();
  EXPECT_EQ(menuPanel->getSelectedIndex(), 0);
}

TEST_F(MenuPanelTest, testSelectPreviousInitiallySelectsLast) {
  menuPanel->selectPrevious();
  EXPECT_EQ(menuPanel->getSelectedIndex(), 2);
}

TEST_F(MenuPanelTest, testSelectPreviousLoopsToLast) {
  menuPanel->selectPrevious();
  menuPanel->selectPrevious();
  menuPanel->selectPrevious();
  menuPanel->selectPrevious();
  EXPECT_EQ(menuPanel->getSelectedIndex(), 2);
}

#include "gtest/gtest.h"
#include "blackguard/logic/manager/FactionManager.h"
#include "blackguard/logic/Faction.h"
#include <memory>

class FactionManagerTest : public testing::Test {
  protected:
    virtual void SetUp() {
      factionManager = new BG::FactionManager();
      factionManager->addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("f1", "Faction 1")));
      factionManager->addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("f2", "Faction 2")));
    }

    virtual void TearDown() {
      delete factionManager;
    }

    BG::FactionManager* factionManager;
};

TEST_F(FactionManagerTest, testAddFaction) {
  auto newFaction = factionManager->getFactionOpt("f1").get();
  EXPECT_EQ(newFaction->getId(), 0);
  EXPECT_EQ(newFaction->getTag(), "f1");
  EXPECT_EQ(newFaction->getName(), "Faction 1");
  EXPECT_EQ(factionManager->getCount(), 2);
}

TEST_F(FactionManagerTest, testSetOneWayRelationships) {
  factionManager->addOneWayRelationship(factionManager->getFaction("f1"), factionManager->getFaction("f2"), 100);
  factionManager->addOneWayRelationship(factionManager->getFaction("f2"), factionManager->getFaction("f1"), 50);

  auto f1 = factionManager->getFaction("f1");
  auto f2 = factionManager->getFaction("f2");
  EXPECT_EQ(f1->getRelationshipTo(f2), 100);
  EXPECT_EQ(f2->getRelationshipTo(f1), 50);
}

TEST_F(FactionManagerTest, testSetTwoWayRelationships) {
  factionManager->addTwoWayRelationship(factionManager->getFaction("f1"), factionManager->getFaction("f2"), 75);

  auto f1 = factionManager->getFaction("f1");
  auto f2 = factionManager->getFaction("f2");
  EXPECT_EQ(f1->getRelationshipTo(f2), 75);
  EXPECT_EQ(f2->getRelationshipTo(f1), 75);
}

TEST_F(FactionManagerTest, testAddingDuplicateTagsFails) {
  factionManager->addFaction(std::unique_ptr<BG::Faction>(new BG::Faction("f1", "Faction3")));
  EXPECT_EQ(factionManager->getCount(), 2);
}

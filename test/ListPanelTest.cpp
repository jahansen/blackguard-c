#include "gtest/gtest.h"
#include "blackguard/interface/ListPanel.h"
#include "blackguard/interface/MasterPanel.h"
#include "stubs/ListPanelItemStub.h"

class ListPanelTest : public testing::Test {
  protected:
    virtual void SetUp() {
      masterPanel = std::unique_ptr<BG::MasterPanel>(new BG::MasterPanel("test-master", 1000, 2000, nullptr, nullptr));
      listPanel = std::unique_ptr<BG::ListPanel>(new BG::ListPanel("test-list-panel", BG::Coordinates(100, 200), 300, 400, masterPanel.get()));
    }

    std::unique_ptr<BG::ListPanel> listPanel;
    std::unique_ptr<BG::MasterPanel> masterPanel;
};

TEST_F(ListPanelTest, testItemsArePositionedCorrectly) {
  listPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(listPanel.get())));
  listPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(listPanel.get())));
  listPanel->addItem(std::unique_ptr<ListPanelItemStub>(new ListPanelItemStub(listPanel.get())));

// TODO: Obsolete - write a better test
//  auto itemPanels = listPanel->getItemPanels();
//  for (int i = 0; i < (int)itemPanels.size(); ++i) {
//    auto itemPanel = itemPanels.at(i);
//    auto itemPanelOffset = itemPanel->getAbsoluteOffset();
//    auto listPanelOffset = listPanel->getAbsoluteOffset();
//    EXPECT_EQ(itemPanelOffset.getX(), listPanelOffset.getX());
//    EXPECT_EQ(itemPanelOffset.getY(), listPanelOffset.getY() + (i*ListPanelItemStub::HEIGHT));
//  }
}

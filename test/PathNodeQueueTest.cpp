#include "gtest/gtest.h"
#include "blackguard/logic/PathNodeQueue.h"
#include "stubs/AreaStub.h"
#include "blackguard/core/ErrorCollection.h"

class PathNodeQueueTest : public testing::Test {
  protected:
    virtual void SetUp() {
      BG::ErrorCollection errors;
      area.loadAreaFromFileWithErrors("data/pathfinderTestData.bga", errors);
      ASSERT_TRUE(errors.isEmpty());

      cell1 = area.getCell(1, 1).get();
      cell2 = area.getCell(2, 2).get();
      cell3 = area.getCell(3, 3).get();

      nodeQueue.push(BG::PathNodePtr(new BG::PathNode(cell1, 0, 20)));
      nodeQueue.push(BG::PathNodePtr(new BG::PathNode(cell2, 0, 10)));
      nodeQueue.push(BG::PathNodePtr(new BG::PathNode(cell3, 0, 30)));
    }

    BG::PathNodeQueue nodeQueue;
    AreaStub area;
    const BG::CellModel* cell1;
    const BG::CellModel* cell2;
    const BG::CellModel* cell3;
};

TEST_F(PathNodeQueueTest, testPushAndPop) {
  auto firstNode = nodeQueue.pop();
  EXPECT_EQ(firstNode->getCell(), cell2);

  auto secondNode = nodeQueue.pop();
  EXPECT_EQ(secondNode->getCell(), cell1);

  nodeQueue.pop();
  auto shouldBeNull = nodeQueue.pop();
  EXPECT_EQ(shouldBeNull, nullptr);
  ASSERT_TRUE(nodeQueue.empty());
}

TEST_F(PathNodeQueueTest, testFindNode) {
  auto foundNode = nodeQueue.find(cell2).get();
  EXPECT_EQ(foundNode->getCell(), cell2);
}

TEST_F(PathNodeQueueTest, testUpdateNode) {
  auto parentNode = nodeQueue.find(cell1).get();
  auto childNode = nodeQueue.find(cell2).get();
  childNode->setParent(parentNode);
  childNode->setDistanceFromStart(1);
  childNode->setDistanceToGoal(2);

  auto updatedNode = nodeQueue.find(cell2).get();
  EXPECT_EQ(updatedNode->getParent().get()->getCell(), cell1);
}

TEST_F(PathNodeQueueTest, testUpdateAndMoveNodes) {
  BG::PathNodeQueue anotherQueue;

  auto parentNode = nodeQueue.find(cell2).get();
  auto childNode = nodeQueue.find(cell3).get();
  childNode->setParent(parentNode);

  // Move the parent node to the other queue
  auto nodeToMove = nodeQueue.pop();
  anotherQueue.push(std::move(nodeToMove));

  // Check that the child node's parent reference is still intact
  EXPECT_EQ(childNode->getParent().get()->getCell(), cell2);
}

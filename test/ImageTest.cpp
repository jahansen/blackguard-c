#include "gtest/gtest.h"
#include "blackguard/interface/Image.h"
#include "blackguard/core/Window.h"
#include <memory>

class ImageTest: public testing::Test {
  protected:
    virtual void SetUp() {
      SDL_Init(SDL_INIT_VIDEO);
      window = std::unique_ptr<BG::Window>(new BG::Window());
      window->initialise(640, 480, "");
      imageFile = std::unique_ptr<BG::Image>(new BG::Image("data/tree.png"));
    }

    std::unique_ptr<BG::Window> window;
    std::unique_ptr<BG::Image> imageFile;
};

TEST_F(ImageTest, testLoadImageFromFile) {
  imageFile->load(window->getRenderer());
  ASSERT_TRUE(imageFile->isLoaded());
}

#include "gtest/gtest.h"
#include "blackguard/core/Directory.h"
#include "../external/easylogging++.h"
#include <regex>

class DirectoryTest : public testing::Test {
  protected:
    virtual void SetUp() {
    }
};

TEST_F(DirectoryTest, getFileNamesListsFiles) {
  auto fileNames = BG::Directory::getFileNames("data/directory-test");

  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test.json") != fileNames.end());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test2.json") != fileNames.end());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test.png") != fileNames.end());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test2.png") != fileNames.end());
}

TEST_F(DirectoryTest, getFileNamesDoesNotListDirectories) {
  auto fileNames = BG::Directory::getFileNames("data/directory-test");

  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test-dir") == fileNames.end());
}

TEST_F(DirectoryTest, getFileNamesWithInvalidDirectoryReturnsEmptyList) {
  auto fileNames = BG::Directory::getFileNames("data/invalid-directory");

  ASSERT_TRUE(fileNames.empty());
}

TEST_F(DirectoryTest, getFileNamesWithEndingRegexReturnsMatchingFileNames) {
  auto fileNames = BG::Directory::getFileNames("data/directory-test", std::regex("(.*)[.](json)"));

  EXPECT_EQ(2, fileNames.size());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test.json") != fileNames.end());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test2.json") != fileNames.end());
}

TEST_F(DirectoryTest, getFileNamesWithStartingRegexReturnsMatchingFileNames) {
  auto fileNames = BG::Directory::getFileNames("data/directory-test", std::regex("(test2)(.*)"));

  EXPECT_EQ(2, fileNames.size());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test2.json") != fileNames.end());
  ASSERT_TRUE(std::find(fileNames.begin(), fileNames.end(), "test2.png") != fileNames.end());
}

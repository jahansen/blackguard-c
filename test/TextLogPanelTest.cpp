#include "gtest/gtest.h"
#include "blackguard/interface/TextLogPanel.h"
#include "blackguard/interface/Font.h"
#include "blackguard/core/ErrorCollection.h"
#include <memory>
#include "../external/easylogging++.h"

class TextLogPanelTest : public testing::Test {
  protected:
    virtual void SetUp() {
      TTF_Init();
      BG::ErrorCollection errors;
      font = std::unique_ptr<BG::Font>(new BG::Font("data/luximr16.json"));
      font->load(errors);
      if (!errors.isEmpty()) errors.printToLog();

      textLogPanel = std::unique_ptr<BG::TextLogPanel>(new BG::TextLogPanel("test-log-panel", font.get(), BG::Coordinates(0, 700), 1024, 68, nullptr, &textLog));
      textLogPanel->addEntry(BG::TextLogEntry("Entry 1"));
      textLogPanel->addEntry(BG::TextLogEntry("Entry 2"));
    }

    BG::TextLog textLog;
    std::unique_ptr<BG::TextLogPanel> textLogPanel;
    std::unique_ptr<BG::Font> font;
};

TEST_F(TextLogPanelTest, testScrollUp) {
  EXPECT_EQ(textLogPanel->getScrollPosition(), 1);
  textLogPanel->scrollUp();
  EXPECT_EQ(textLogPanel->getScrollPosition(), 2);
  textLogPanel->scrollUp();
  EXPECT_EQ(textLogPanel->getScrollPosition(), 2);
}

TEST_F(TextLogPanelTest, testScrollDown) {
  textLogPanel->scrollDown();
  EXPECT_EQ(textLogPanel->getScrollPosition(), 1);
  textLogPanel->scrollUp();
  textLogPanel->scrollDown();
  EXPECT_EQ(textLogPanel->getScrollPosition(), 1);
}

#include "gtest/gtest.h"
#include "blackguard/core/RandomGenerator.h"
#include "../external/easylogging++.h"

class RandomGeneratorTest : public testing::Test {
  protected:
    BG::RandomGenerator generator;

    static const int REPEATS = 10000;
};

TEST_F(RandomGeneratorTest, testRange) {
  for (int i = 0; i < REPEATS; ++i) {
    int result = generator.range(5, 500);
    EXPECT_TRUE(5 <= result && result <= 500) << "Unexpected value was " << result;
  }
}

TEST_F(RandomGeneratorTest, testRollXd) {
  for (int i = 0; i < REPEATS; ++i) {
    int result = generator.rolldX(20);
    EXPECT_TRUE(1 <= result && result <= 20) << "Unexpected value was " << result;
  }
}

TEST_F(RandomGeneratorTest, testRollXdY) {
  for (int i = 0; i < REPEATS; ++i) {
    int result = generator.rollXdY(3, 6);
    EXPECT_TRUE(3 <= result && result <= 18) << "Unexpected value was " << result;
  }
}

TEST_F(RandomGeneratorTest, testRollXdYplusZ) {
  for (int i = 0; i < REPEATS; ++i) {
    int result = generator.rollXdYplusZ(4, 5, 3);
    EXPECT_TRUE(7 <= result && result <= 23) << "Unexpected value was " << result;
  }
}

#include "gtest/gtest.h"
#include "blackguard/core/Timer.h"
#include <chrono>
#include <thread>

class TimerTest : public testing::Test {
  protected:
    void sleepMs(int ms) {
      std::this_thread::sleep_for(std::chrono::milliseconds(ms));
    }

    BG::Timer timer;
};

TEST_F(TimerTest, testGetTicksReturnsZeroWhenNotStarted) {
  EXPECT_EQ(timer.getTicks(), 0);
}

TEST_F(TimerTest, testGetTicksReturnsPositiveWhenStarted) {
  timer.start();
  sleepMs(1);
  EXPECT_GT(timer.getTicks(), 0);
}

TEST_F(TimerTest, testStoppedTimerStopsCounting) {
  timer.start();
  sleepMs(1);
  timer.stop();
  int firstTime = timer.getTicks();
  sleepMs(1);
  EXPECT_EQ(timer.getTicks(), firstTime);
}

TEST_F(TimerTest, testUnpausedTimerResumesCounting) {
  timer.start();
  sleepMs(1);
  timer.stop();
  int pausedTime = timer.getTicks();
  timer.start();
  sleepMs(1);
  EXPECT_GT(timer.getTicks(), pausedTime);
}

#include "gtest/gtest.h"
#include "blackguard/core/Window.h"
#include <string>

class WindowTest: public testing::Test {
  protected:
    virtual void SetUp() {
      window.initialise(WINDOW_WIDTH, WINDOW_HEIGHT, "Title");
    }

    BG::Window window;
    static const int WINDOW_WIDTH = 640;
    static const int WINDOW_HEIGHT = 480;
};

TEST_F(WindowTest, testInit) {
  EXPECT_EQ(SDL_WasInit(SDL_INIT_TIMER|SDL_INIT_AUDIO|SDL_INIT_VIDEO),
            SDL_INIT_TIMER|SDL_INIT_AUDIO|SDL_INIT_VIDEO);
  ASSERT_TRUE(TTF_WasInit());
  ASSERT_TRUE(window.getWidth() == WindowTest::WINDOW_WIDTH);
  ASSERT_TRUE(window.getHeight() == WindowTest::WINDOW_HEIGHT);
  ASSERT_TRUE(window.getScreen() != nullptr);
  EXPECT_EQ(window.getCaption(), "Title");
}




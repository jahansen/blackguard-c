#include "gtest/gtest.h"
#include "stubs/AreaStub.h"
#include "blackguard/interface/Viewport.h"
#include "../external/easylogging++.h"

class ViewportTest : public testing::Test {
  protected:
    virtual void SetUp() {
      area.loadAreaFromFile("data/areaTestData.bga");
    }

    AreaStub area;
};

TEST_F(ViewportTest, testPositionIsWithinViewport) {
  auto viewport = BG::Viewport(&area, BG::Position(5, 5), 7, 7);

  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(2, 2)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(2, 5)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(2, 8)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(5, 2)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(5, 5)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(5, 8)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(8, 2)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(8, 5)));
  ASSERT_TRUE(viewport.positionIsWithinViewport(BG::Position(8, 8)));

  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(1, 1)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(2, 1)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(1, 2)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(1, 9)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(1, 8)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(2, 9)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(8, 1)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(9, 1)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(9, 2)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(9, 9)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(9, 8)));
  ASSERT_FALSE(viewport.positionIsWithinViewport(BG::Position(8, 9)));
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondUpperLeft) {
  auto viewport = BG::Viewport(&area, BG::Position(1, 1), 7, 7);

  EXPECT_EQ(BG::Position(0, 0), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondTop) {
  auto viewport = BG::Viewport(&area, BG::Position(6, 1), 7, 7);

  EXPECT_EQ(BG::Position(3, 0), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondUpperRight) {
  auto viewport = BG::Viewport(&area, BG::Position(11, 1), 7, 7);

  EXPECT_EQ(BG::Position(6, 0), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondRight) {
  auto viewport = BG::Viewport(&area, BG::Position(11, 6), 7, 7);

  EXPECT_EQ(BG::Position(6, 3), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondLowerRight) {
  auto viewport = BG::Viewport(&area, BG::Position(11, 11), 7, 7);

  EXPECT_EQ(BG::Position(6, 6), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondBottom) {
  auto viewport = BG::Viewport(&area, BG::Position(6, 11), 7, 7);

  EXPECT_EQ(BG::Position(3, 6), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondLowerLeft) {
  auto viewport = BG::Viewport(&area, BG::Position(1, 11), 7, 7);

  EXPECT_EQ(BG::Position(0, 6), viewport.getOffset());
}

TEST_F(ViewportTest, originIsShiftedForViewportBeyondLeft) {
  auto viewport = BG::Viewport(&area, BG::Position(1, 6), 7, 7);

  EXPECT_EQ(BG::Position(0, 3), viewport.getOffset());
}


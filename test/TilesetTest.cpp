#include "gtest/gtest.h"
#include "blackguard/logic/Tileset.h"
#include "blackguard/logic/Tile.h"
#include "blackguard/core/Window.h"
#include "blackguard/core/ErrorCollection.h"

#include <SDL2/SDL.h>
#include <vector>
#include <memory>

class TilesetTest: public testing::Test {
  protected:
    virtual void SetUp() {
      tileset = std::unique_ptr<BG::Tileset>(new BG::Tileset("testTileset", "Test Tiles", "data/tilesetTestData.bgt"));
      window = std::unique_ptr<BG::Window>(new BG::Window());
      window->initialise(640, 480, "");
    }

    virtual void TearDown() {

    }

    std::unique_ptr<BG::Tileset> tileset;
    std::unique_ptr<BG::Window> window;
};

TEST_F(TilesetTest, testLoadTileset) {
  BG::ErrorCollection errors;
  tileset->load(window->getRenderer(), errors);
  std::vector<BG::Tile> tiles = tileset->getTiles();
  ASSERT_TRUE(errors.isEmpty());
  ASSERT_FALSE(tileset->getImage() == nullptr);
  EXPECT_EQ(tileset->getImageFilename(), "data/tilesetTestData.png");
  EXPECT_EQ(tiles[0], BG::Tile("tile0", 0, 0, 32, 32, nullptr));
  EXPECT_EQ(tiles[1], BG::Tile("tile1", 96, 0, 32, 32, nullptr));
  EXPECT_EQ(tiles[2], BG::Tile("tile2", 0, 32, 32, 32, nullptr));
  EXPECT_EQ(tiles[3], BG::Tile("tile3", 32, 96, 32, 32, nullptr));
}

TEST_F(TilesetTest, testUnload) {
  BG::ErrorCollection errors;
  tileset->load(window->getRenderer(), errors);
  ASSERT_TRUE(errors.isEmpty());
  tileset->unload();
  EXPECT_EQ(tileset->getImageFilename(), "");
  ASSERT_TRUE(tileset->getTiles().empty());
}

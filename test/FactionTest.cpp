#include "gtest/gtest.h"
#include "blackguard/logic/Faction.h"

class FactionTest : public testing::Test {
  protected:
    virtual void SetUp() {
      humanFaction.addRelationship(&elfFaction, BG::Faction::FRIENDLY);
      humanFaction.addRelationship(&orcFaction, BG::Faction::HOSTILE);
      elfFaction.addRelationship(&humanFaction, BG::Faction::NEUTRAL);
      elfFaction.addRelationship(&orcFaction, BG::Faction::HOSTILE);
      orcFaction.addRelationship(&humanFaction, BG::Faction::HOSTILE);
      orcFaction.addRelationship(&elfFaction, BG::Faction::HOSTILE);
    }

    BG::Faction humanFaction = BG::Faction("humans", "Humans"); // Friendly to elves, hostile to orcs
    BG::Faction elfFaction = BG::Faction("elves", "Elves");   // Neutral to humans, hostile to orcs
    BG::Faction orcFaction = BG::Faction("orcs", "Orcs");   // Hostile to everyone
    BG::Faction hermitFaction = BG::Faction("hermit", "Hermit"); // No relationships
};

TEST_F(FactionTest, testHumansFriendlyToElves) {
  EXPECT_TRUE(humanFaction.isFriendlyTo(&elfFaction));
  EXPECT_FALSE(humanFaction.isNeutralTo(&elfFaction));
  EXPECT_FALSE(humanFaction.isHostileTo(&elfFaction));
  EXPECT_TRUE(humanFaction.isNotHostileTo(&elfFaction));
  EXPECT_FALSE(humanFaction.isNotFriendlyTo(&elfFaction));
  EXPECT_TRUE(humanFaction.isNotNeutralTo(&elfFaction));
}

TEST_F(FactionTest, testElvesNeutralToHumans) {
  EXPECT_FALSE(elfFaction.isFriendlyTo(&humanFaction));
  EXPECT_TRUE(elfFaction.isNeutralTo(&humanFaction));
  EXPECT_FALSE(elfFaction.isHostileTo(&humanFaction));
  EXPECT_TRUE(elfFaction.isNotFriendlyTo(&humanFaction));
  EXPECT_FALSE(elfFaction.isNotNeutralTo(&humanFaction));
  EXPECT_TRUE(elfFaction.isNotHostileTo(&humanFaction));
}

TEST_F(FactionTest, testOrcsHostileToElves) {
  EXPECT_FALSE(orcFaction.isFriendlyTo(&elfFaction));
  EXPECT_FALSE(orcFaction.isNeutralTo(&elfFaction));
  EXPECT_TRUE(orcFaction.isHostileTo(&elfFaction));
  EXPECT_TRUE(orcFaction.isNotFriendlyTo(&elfFaction));
  EXPECT_TRUE(orcFaction.isNotNeutralTo(&elfFaction));
  EXPECT_FALSE(orcFaction.isNotHostileTo(&elfFaction));
}

TEST_F(FactionTest, testHermitNeutralToEveryone) {
  EXPECT_TRUE(hermitFaction.isNeutralTo(&humanFaction));
  EXPECT_FALSE(hermitFaction.isFriendlyTo(&elfFaction));
  EXPECT_FALSE(hermitFaction.isHostileTo(&orcFaction));
}



#include "gtest/gtest.h"
#include "blackguard/logic/ActionQueue.h"
#include "stubs/ActorStub.h"

class ActionQueueTest : public testing::Test {
  protected:
    virtual void SetUp() {
      actor1 = new ActorStub("alpha", nullptr, nullptr, nullptr);
      actor2 = new ActorStub("beta", nullptr, nullptr, nullptr);
      actor3 = new ActorStub("gamma", nullptr, nullptr, nullptr);
    }

    virtual void TearDown() {
      delete actor1;
      delete actor2;
      delete actor3;
    }

    BG::ActionQueue actionQueue;
    ActorStub* actor1;
    ActorStub* actor2;
    ActorStub* actor3;
};

TEST_F(ActionQueueTest, testActionsAreAddedInTemporalOrder) {
  actionQueue.push(actor1, 30);
  actionQueue.push(actor2, 10);
  actionQueue.push(actor3, 20);

  EXPECT_EQ(actionQueue.next().getActor()->getTag(), "beta");
  actionQueue.pop();
  EXPECT_EQ(actionQueue.next().getActor()->getTag(), "gamma");
  actionQueue.pop();
  EXPECT_EQ(actionQueue.next().getActor()->getTag(), "alpha");
}

TEST_F(ActionQueueTest, testCancelActionsCancelsAllActionsForEntity) {
  actionQueue.push(actor1, 1);
  actionQueue.push(actor1, 2);
  actionQueue.push(actor2, 3);
  actionQueue.push(actor1, 4);

  actionQueue.cancelActionsForEntity(actor1);
  EXPECT_EQ(actionQueue.next().getActor()->getTag(), "beta");
  EXPECT_EQ(actionQueue.size(), 1);
}

#include "gtest/gtest.h"
#include "stubs/AreaStub.h"
#include "blackguard/logic/CellModel.h"
#include "blackguard/logic/Direction.h"
#include "blackguard/core/ErrorCollection.h"

class AreaTest : public testing::Test {
  protected:
    virtual void SetUp() {
      BG::ErrorCollection errors;
      area.loadAreaFromFileWithErrors("data/areaTestData.bga", errors);
      ASSERT_TRUE(errors.isEmpty());
    }

    AreaStub area;
    const std::string testAreaData[13][13] = {
      {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
      {"0", "0", "0", "0", "1", "1", "1", "1", "1", "0", "1", "0", "0"},
      {"0", "0", "0", "0", "1", "1", "1", "1", "1", "1", "0", "0", "0"},
      {"0", "0", "1", "1", "1", "1", "1", "1", "1", "1", "1", "0", "0"},
      {"0", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "0"},
      {"0", "1", "", "1", "1", "1", "1", "1", "1", "1", "1", "1", "0"},
      {"0", "1", "1", "1", "1", "1", "1", "1", "2", "2", "1", "1", "0"},
      {"0", "1", "1", "1", "1", "1", "1", "1", "2", "2", "1", "1", "0"},
      {"0", "1", "1", "0", "1", "1", "1", "1", "1", "1", "1", "1", "0"},
      {"0", "0", "1", "1", "1", "1", "1", "1", "1", "1", "1", "0", "0"},
      {"0", "0", "0", "1", "1", "1", "1", "1", "1", "1", "0", "0", "0"},
      {"0", "0", "0", "0", "1", "1", "1", "1", "1", "0", "0", "0", "0"},
      {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
    };
};

TEST_F(AreaTest, testLoadAreaFromFile) {
  auto rows = area.getMatrix();
  for (auto row : *rows) {
    for (auto cell : row) {
      EXPECT_EQ(cell.getProperty("type"), testAreaData[cell.getRowPos()][cell.getColPos()]);
    }
  }
}

TEST_F(AreaTest, testUnloadFile) {
  area.unload();
  EXPECT_EQ(area.getRowCount(), 0);
}

TEST_F(AreaTest, testCellExists) {
  ASSERT_TRUE(area.cellExists(BG::Position(0, 0)));
  ASSERT_TRUE(area.cellExists(BG::Position(12, 12)));
  ASSERT_FALSE(area.cellExists(BG::Position(999, -1)));
  ASSERT_FALSE(area.cellExists(BG::Position(13, 13)));
}

TEST_F(AreaTest, testGetAdjacentCell) {
  const BG::CellModel* centralCell = area.getCell(10, 10).get();
  const BG::CellModel* northCell = area.getCell(9, 10).get();
  const BG::CellModel* eastCell = area.getCell(10, 11).get();
  const BG::CellModel* southCell = area.getCell(11, 10).get();
  const BG::CellModel* westCell = area.getCell(10, 9).get();
  const BG::CellModel* neCell = area.getCell(9, 11).get();
  const BG::CellModel* seCell = area.getCell(11, 11).get();
  const BG::CellModel* swCell = area.getCell(11, 9).get();
  const BG::CellModel* nwCell = area.getCell(9, 9).get();

  EXPECT_EQ(northCell, area.getAdjacentCell(centralCell, BG::Direction::NORTH));
  EXPECT_EQ(eastCell, area.getAdjacentCell(centralCell, BG::Direction::EAST));
  EXPECT_EQ(southCell, area.getAdjacentCell(centralCell, BG::Direction::SOUTH));
  EXPECT_EQ(westCell, area.getAdjacentCell(centralCell, BG::Direction::WEST));
  EXPECT_EQ(neCell, area.getAdjacentCell(centralCell, BG::Direction::NORTH_EAST));
  EXPECT_EQ(seCell, area.getAdjacentCell(centralCell, BG::Direction::SOUTH_EAST));
  EXPECT_EQ(swCell, area.getAdjacentCell(centralCell, BG::Direction::SOUTH_WEST));
  EXPECT_EQ(nwCell, area.getAdjacentCell(centralCell, BG::Direction::NORTH_WEST));
}

TEST_F(AreaTest, testLoSSourceEqualsTarget) {
  auto sourcePos = BG::Position(3, 5);
  auto targetPos = BG::Position(3, 5);
  EXPECT_TRUE(area.lineOfSightExists(sourcePos, targetPos));
}

TEST_F(AreaTest, testLoSSourceAdjacentToTarget) {
  auto sourcePos = BG::Position(3, 5);
  auto targetPos = BG::Position(3, 6);
  EXPECT_TRUE(area.lineOfSightExists(sourcePos, targetPos));
}

TEST_F(AreaTest, testLoSExists) {
  auto sourcePos = BG::Position(3, 5);
  auto targetPos = BG::Position(7, 8);
  EXPECT_TRUE(area.lineOfSightExists(sourcePos, targetPos));
}

TEST_F(AreaTest, testLoSBlocked) {
  auto sourcePos = BG::Position(3, 5);
  auto targetPos = BG::Position(3, 10);
  EXPECT_FALSE(area.lineOfSightExists(sourcePos, targetPos));
}

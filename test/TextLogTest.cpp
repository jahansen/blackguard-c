#include "gtest/gtest.h"
#include "blackguard/logic/TextLog.h"
#include "blackguard/interface/Colour.h"
#include <vector>

class TextLogTest : public testing::Test {
  protected:
    virtual void SetUp() {
      textLog.addEntry(entry1, entry1Colour);
      textLog.addEntry(entry2, entry2Colour);
      textLog.addEntry(entry3, entry3Colour);
      textLog.addEntry(entry4, entry4Colour);
      textLog.addEntry(entry5, entry5Colour);
    }

    BG::TextLog textLog;
    const std::string entry1 = "Lorem";
    const std::string entry2 = "ipsum";
    const std::string entry3 = "dolor";
    const std::string entry4 = "sit";
    const std::string entry5 = "amet";
    const BG::Colour entry1Colour = BG::Colour(BG::ColourId::WHITE);
    const BG::Colour entry2Colour = BG::Colour(BG::ColourId::GREEN);
    const BG::Colour entry3Colour = BG::Colour(BG::ColourId::BLUE);
    const BG::Colour entry4Colour = BG::Colour(BG::ColourId::YELLOW);
    const BG::Colour entry5Colour = BG::Colour(BG::ColourId::RED);
};

TEST_F(TextLogTest, testAddEntries) {
  std::vector<BG::TextLogEntry> entries = textLog.getEntries();
  EXPECT_EQ(entries.at(0).getText(), entry1);
  EXPECT_EQ(entries.at(0).getColour(), entry1Colour);
  EXPECT_EQ(entries.at(1).getText(), entry2);
  EXPECT_EQ(entries.at(1).getColour(), entry2Colour);
}

TEST_F(TextLogTest, testGetLatest) {
  std::vector<BG::TextLogEntry> entries = textLog.getLatest(3);
  EXPECT_EQ(entries.at(0).getText(), entry3);
  EXPECT_EQ(entries.at(1).getText(), entry4);
  EXPECT_EQ(entries.at(2).getText(), entry5);
}

TEST_F(TextLogTest, testGetLatestMoreThanExists) {
  std::vector<BG::TextLogEntry> entries = textLog.getLatest(10);
  EXPECT_EQ(entries.at(4).getText(), entry5);
}

TEST_F(TextLogTest, testGetLatestNegative) {
  std::vector<BG::TextLogEntry> entries = textLog.getLatest(-1);
  EXPECT_EQ(entries.size(), 0);
}

#include "gtest/gtest.h"
#include "blackguard/logic/Calendar.h"

class CalendarTest : public testing::Test {
  protected:
    virtual void SetUp() {
      calendar.setFullDateTime(23, 59, 59, 999, 30, 10, 1000);
    }

    BG::Calendar calendar;
};

TEST_F(CalendarTest, testSetDate) {
  const int DAY = 1;
  const int MONTH = 2;
  const int YEAR = 3000;

  calendar.setDate(DAY, MONTH, YEAR);

  EXPECT_EQ(calendar.getDay(), DAY);
  EXPECT_EQ(calendar.getMonth(), MONTH);
  EXPECT_EQ(calendar.getYear(), YEAR);
}

TEST_F(CalendarTest, testSetTime) {
  const int SECOND = 10;
  const int MINUTE = 20;
  const int HOUR = 13;

  calendar.setTime(HOUR, MINUTE, SECOND);

  EXPECT_EQ(calendar.getSecond(), SECOND);
  EXPECT_EQ(calendar.getMinute(), MINUTE);
  EXPECT_EQ(calendar.getHour(), HOUR);
}

TEST_F(CalendarTest, testSetDateTime) {
  const int MILLISECOND = 999;
  const int SECOND = 1;
  const int MINUTE = 2;
  const int HOUR = 3;
  const int DAY = 4;
  const int MONTH = 5;
  const int YEAR = 600;

  calendar.setFullDateTime(HOUR, MINUTE, SECOND, MILLISECOND, DAY, MONTH, YEAR);

  EXPECT_EQ(calendar.getMillisecond(), MILLISECOND);
  EXPECT_EQ(calendar.getSecond(), SECOND);
  EXPECT_EQ(calendar.getMinute(), MINUTE);
  EXPECT_EQ(calendar.getHour(), HOUR);
  EXPECT_EQ(calendar.getDay(), DAY);
  EXPECT_EQ(calendar.getMonth(), MONTH);
  EXPECT_EQ(calendar.getYear(), YEAR);
}

TEST_F(CalendarTest, testAddMilliseconds) {
  calendar.addMilliseconds(2);

  EXPECT_EQ(calendar.getMillisecond(), 1);
  EXPECT_EQ(calendar.getSecond(), 0);
  EXPECT_EQ(calendar.getMinute(), 0);
  EXPECT_EQ(calendar.getHour(), 0);
  EXPECT_EQ(calendar.getDay(), 1);
  EXPECT_EQ(calendar.getMonth(), 11);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddSeconds) {
  calendar.addSeconds(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 1);
  EXPECT_EQ(calendar.getMinute(), 0);
  EXPECT_EQ(calendar.getHour(), 0);
  EXPECT_EQ(calendar.getDay(), 1);
  EXPECT_EQ(calendar.getMonth(), 11);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddMinutes) {
  calendar.addMinutes(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 59);
  EXPECT_EQ(calendar.getMinute(), 1);
  EXPECT_EQ(calendar.getHour(), 0);
  EXPECT_EQ(calendar.getDay(), 1);
  EXPECT_EQ(calendar.getMonth(), 11);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddHours) {
  calendar.addHours(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 59);
  EXPECT_EQ(calendar.getMinute(), 59);
  EXPECT_EQ(calendar.getHour(), 1);
  EXPECT_EQ(calendar.getDay(), 1);
  EXPECT_EQ(calendar.getMonth(), 11);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddDays) {
  calendar.addDays(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 59);
  EXPECT_EQ(calendar.getMinute(), 59);
  EXPECT_EQ(calendar.getHour(), 23);
  EXPECT_EQ(calendar.getDay(), 2);
  EXPECT_EQ(calendar.getMonth(), 11);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddMonths) {
  calendar.addMonths(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 59);
  EXPECT_EQ(calendar.getMinute(), 59);
  EXPECT_EQ(calendar.getHour(), 23);
  EXPECT_EQ(calendar.getDay(), 30);
  EXPECT_EQ(calendar.getMonth(), 12);
  EXPECT_EQ(calendar.getYear(), 1000);
}

TEST_F(CalendarTest, testAddYears) {
  calendar.addYears(2);

  EXPECT_EQ(calendar.getMillisecond(), 999);
  EXPECT_EQ(calendar.getSecond(), 59);
  EXPECT_EQ(calendar.getMinute(), 59);
  EXPECT_EQ(calendar.getHour(), 23);
  EXPECT_EQ(calendar.getDay(), 30);
  EXPECT_EQ(calendar.getMonth(), 10);
  EXPECT_EQ(calendar.getYear(), 1002);
}


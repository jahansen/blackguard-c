#ifndef AREA8CONNECTEDSTUB_H
#define AREA8CONNECTEDSTUB_H

#include "AreaStub.h"

class Area8ConnectedStub : public AreaStub {
  public:
    Area8ConnectedStub();
    Area8ConnectedStub(std::string tag, std::string name);
    virtual ~Area8ConnectedStub();

    virtual std::vector<const BG::CellModel*> getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea = std::vector<BG::Actor*>()) const override;
    virtual int getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const override;
    virtual int estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const override;
};

#endif // AREA8CONNECTEDSTUB_H

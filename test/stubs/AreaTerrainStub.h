#ifndef AREA8CONNECTED14DIAGSTUB_H
#define AREA8CONNECTED14DIAGSTUB_H

#include "Area8ConnectedStub.h"

class AreaTerrainStub : public Area8ConnectedStub {
  public:
    AreaTerrainStub();
    AreaTerrainStub(std::string tag, std::string name);
    virtual ~AreaTerrainStub();

    virtual int getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const override;
};

#endif // AREA8CONNECTED14DIAGSTUB_H

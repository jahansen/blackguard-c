#include "ActorStub.h"

ActorStub::ActorStub(std::string tag, const BG::AreaModel* area, const BG::Faction* faction, const BG::Tile* tile)
: Actor(tag, "FakeActor", true, area, BG::Position(), faction, tile) {}

BG::Entity* ActorStub::getCopy() const {
  auto copy = new ActorStub(tag, area.get(), faction, tile);
  copy->id = id;
  return copy;
}

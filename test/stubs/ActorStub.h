#ifndef ACTORSTUB_H
#define ACTORSTUB_H

#include "blackguard/logic/Actor.h"

class ActorStub : public BG::Actor {
  public:
    ActorStub(std::string tag, const BG::AreaModel* area, const BG::Faction* faction, const BG::Tile* tile);

    virtual Entity* getCopy() const override;

    inline virtual void move(const BG::Direction& direction) override {}
    inline virtual void bumpActor(BG::Actor* actor) override {}
    inline virtual void bumpWall() override {}
    inline virtual void onDeath() override {}
    inline virtual bool waitForInput() const override {return false;}
    inline virtual void startTurn() override {}
    inline virtual void pickUp() override {}
    inline virtual void wait() override {}
    inline virtual void moveDone() override {}
    inline virtual void attackDone() override {}

  private:
};

#endif // ACTORSTUB_H

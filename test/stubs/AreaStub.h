#ifndef AREASTUB_H
#define AREASTUB_H

#include "blackguard/logic/AreaModel.h"

class AreaStub : public BG::AreaModel {
  public:
    AreaStub();
    AreaStub(std::string tag, std::string name);
    virtual ~AreaStub();

    BG::Entity* getCopy() const override;

    std::vector<const BG::CellModel*> getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea = std::vector<BG::Actor*>()) const override;
    bool cellBlocksLineOfSight(const BG::CellModel* cell) const override;
    bool cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea = std::vector<BG::Actor*>()) const override;
    bool cellIsWalkable(const BG::CellModel* cell) const;
    int getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const override;
    int estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const override;
};

#endif // AREASTUB_H

#include "Area8ConnectedStub.h"
#include <cmath>

Area8ConnectedStub::Area8ConnectedStub()
: AreaStub() {}

Area8ConnectedStub::Area8ConnectedStub(std::string tag, std::string name)
: AreaStub(tag, name) {}

Area8ConnectedStub::~Area8ConnectedStub() {
}

std::vector<const BG::CellModel*> Area8ConnectedStub::getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  auto northCell = getAdjacentCell(cell, BG::Direction::NORTH);
  auto eastCell = getAdjacentCell(cell, BG::Direction::EAST);
  auto southCell = getAdjacentCell(cell, BG::Direction::SOUTH);
  auto westCell = getAdjacentCell(cell, BG::Direction::WEST);
  auto neCell = getAdjacentCell(cell, BG::Direction::NORTH_EAST);
  auto seCell = getAdjacentCell(cell, BG::Direction::SOUTH_EAST);
  auto swCell = getAdjacentCell(cell, BG::Direction::SOUTH_WEST);
  auto nwCell = getAdjacentCell(cell, BG::Direction::NORTH_WEST);

  std::vector<const BG::CellModel*> passableCells;
  if (northCell && cellIsPassable(northCell.get(), goalCell)) passableCells.push_back(northCell.get());
  if (eastCell && cellIsPassable(eastCell.get(), goalCell)) passableCells.push_back(eastCell.get());
  if (southCell && cellIsPassable(southCell.get(), goalCell)) passableCells.push_back(southCell.get());
  if (westCell && cellIsPassable(westCell.get(), goalCell)) passableCells.push_back(westCell.get());
  if (neCell && cellIsPassable(neCell.get(), goalCell)) passableCells.push_back(neCell.get());
  if (seCell && cellIsPassable(seCell.get(), goalCell)) passableCells.push_back(seCell.get());
  if (swCell && cellIsPassable(swCell.get(), goalCell)) passableCells.push_back(swCell.get());
  if (nwCell && cellIsPassable(nwCell.get(), goalCell)) passableCells.push_back(nwCell.get());

  return passableCells;
}

int Area8ConnectedStub::getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const {
  auto travelDirection = getDirection(cell->getPosition(), neighbourCell->getPosition());

  switch (travelDirection) {
    case BG::Direction::NORTH:
    case BG::Direction::EAST:
    case BG::Direction::SOUTH:
    case BG::Direction::WEST:
      return 10;

    case BG::Direction::NORTH_EAST:
    case BG::Direction::SOUTH_EAST:
    case BG::Direction::SOUTH_WEST:
    case BG::Direction::NORTH_WEST:
      return 14;

    default:
      return 10;
  }
}

int Area8ConnectedStub::estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const {
  // Chebyshev distance with straight movement cost of 10, diagonal of 14
  const int DS = 10;
  const int DD = 14;
  int dx = abs(start->getX() - goal->getX());
  int dy = abs(start->getY() - goal->getY());
  return DS * (dx + dy) + (DD - 2 * DS) * std::min(dx, dy);
}



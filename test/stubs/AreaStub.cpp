#include "AreaStub.h"

AreaStub::AreaStub()
: BG::AreaModel("", "") {}

AreaStub::AreaStub(std::string tag, std::string name)
: BG::AreaModel(tag, name) {}

AreaStub::~AreaStub() {
}

BG::Entity* AreaStub::getCopy() const {
  auto copy = new AreaStub(tag, name);
  copy->id = id;
  copy->matrix = matrix;
  return copy;
}

std::vector<const BG::CellModel*> AreaStub::getPassableNeighbours(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  auto northCell = getAdjacentCell(cell, BG::Direction::NORTH);
  auto eastCell = getAdjacentCell(cell, BG::Direction::EAST);
  auto southCell = getAdjacentCell(cell, BG::Direction::SOUTH);
  auto westCell = getAdjacentCell(cell, BG::Direction::WEST);

  std::vector<const BG::CellModel*> passableCells;
  if (northCell && cellIsPassable(northCell.get(), goalCell)) passableCells.push_back(northCell.get());
  if (eastCell && cellIsPassable(eastCell.get(), goalCell)) passableCells.push_back(eastCell.get());
  if (southCell && cellIsPassable(southCell.get(), goalCell)) passableCells.push_back(southCell.get());
  if (westCell && cellIsPassable(westCell.get(), goalCell)) passableCells.push_back(westCell.get());

  return passableCells;
}

bool AreaStub::cellBlocksLineOfSight(const BG::CellModel* cell) const {
  return cell->getProperty("type") == "0";
}

bool AreaStub::cellIsPassable(const BG::CellModel* cell, const BG::CellModel* goalCell, const std::vector<BG::Actor*>& actorsInArea) const {
  return cell->getProperty("type") != "0" || cell == goalCell;
}

bool AreaStub::cellIsWalkable(const BG::CellModel* cell) const {
  return cell->getProperty("type") != "0";
}

int AreaStub::getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const {
  return 10;
}

int AreaStub::estimateDistanceBetweenCells(const BG::CellModel* start, const BG::CellModel* goal) const {
  // Manhattan distance with minimum movement cost of 10
  const int D = 10;
  int dx = abs(start->getX() - goal->getX());
  int dy = abs(start->getY() - goal->getY());
  return D * (dx + dy);
}




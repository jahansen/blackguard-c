#include "ListPanelItemStub.h"

ListPanelItemStub::ListPanelItemStub(Panel* parent)
: ListPanelItem(WIDTH, HEIGHT, parent) {}

ListPanelItemStub::~ListPanelItemStub() {}

void ListPanelItemStub::drawSelf() {}

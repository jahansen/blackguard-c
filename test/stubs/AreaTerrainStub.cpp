#include "AreaTerrainStub.h"

AreaTerrainStub::AreaTerrainStub()
: Area8ConnectedStub() {}

AreaTerrainStub::AreaTerrainStub(std::string tag, std::string name)
: Area8ConnectedStub(tag, name) {}

AreaTerrainStub::~AreaTerrainStub() {}

int AreaTerrainStub::getMovementCost(const BG::CellModel* cell, const BG::CellModel* neighbourCell) const {
  auto travelDirection = getDirection(cell->getPosition(), neighbourCell->getPosition());

  int cellWeight;
  if (neighbourCell->getProperty("type") == "1") cellWeight = 1;
  else cellWeight = 3;

  switch (travelDirection) {
    case BG::Direction::NORTH:
    case BG::Direction::EAST:
    case BG::Direction::SOUTH:
    case BG::Direction::WEST:
      return 10 * cellWeight;

    case BG::Direction::NORTH_EAST:
    case BG::Direction::SOUTH_EAST:
    case BG::Direction::SOUTH_WEST:
    case BG::Direction::NORTH_WEST:
      return 14 * cellWeight;

    default:
      return 10 * cellWeight;
  }
}

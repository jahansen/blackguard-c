#ifndef LISTPANELITEMSTUB_H
#define LISTPANELITEMSTUB_H

#include "blackguard/interface/ListPanelItem.h"

class ListPanelItemStub : public BG::ListPanelItem {
  public:
    ListPanelItemStub(Panel* parent);
    virtual ~ListPanelItemStub();

    virtual void drawSelf() override;

    static const int WIDTH = 300;
    static const int HEIGHT = 100;
};

#endif // LISTPANELITEMSTUB_H

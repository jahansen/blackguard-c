#!/bin/bash

function nohome() {
  echo "Error: This script requires the BG_HOME environment variable to be set to the "
  echo "base directory for the project."
  exit 0
}

if [ -z $BG_HOME ]; then
  nohome
fi

cd $BG_HOME
mkdir build
cd build
mkdir debug
mkdir release
mkdir test
cd debug
cmake -DCMAKE_BUILD_TYPE=Debug ../..
cd ../test
cmake -DCMAKE_BUILD_TYPE=Debug ../..
cd ../release
cmake -DCMAKE_BUILD_TYPE=Release ../..



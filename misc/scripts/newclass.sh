#!/bin/bash

function usage() {
  echo "Usage: $0 ClassName class/path [namespace]"
  echo "Creates a new class in the Blackguard project using a basic template."
  exit 0
}

function nohome() {
  echo "Error: This script requires the BG_HOME environment variable to be set to the "
  echo "base directory for the project."
  exit 0
}

function nodir() {
  echo "Error: The specified directory $SRC_PATH does not exist."
  exit 0
}

function duplicate() {
  echo "Error: A class with the name $CLASS_NAME appears to already exist."
  exit 0
}

function noboilerplate() {
  echo "Error: Cannot find file $BG_HOME/misc/boilerplate.txt."
  exit 0
}

function writeHeader() {
  $(cat $BG_HOME/misc/boilerplate.txt >> $H_FILE)
  $(echo >> $H_FILE)
  $(echo "#ifndef ${CLASS_UPPER}_H" >> $H_FILE)
  $(echo "#define ${CLASS_UPPER}_H" >> $H_FILE)
  $(echo >> $H_FILE)

  if [ ! -z $NAMESPACE ]; then
    $(echo "namespace ${NAMESPACE} {" >> $H_FILE)
    $(echo >> $H_FILE)
  fi

  $(echo "class $CLASS_NAME {" >> $H_FILE)
  $(echo "  public:" >> $H_FILE)
  $(echo "    $CLASS_NAME();" >> $H_FILE)
  $(echo "    virtual ~$CLASS_NAME();" >> $H_FILE)
  $(echo >> $H_FILE)
  $(echo "};" >> $H_FILE)
  $(echo >> $H_FILE)

  if [ ! -z $NAMESPACE ]; then
    $(echo "} // ${NAMESPACE}" >> $H_FILE)
    $(echo >> $H_FILE)
  fi

  $(echo "#endif // ${CLASS_UPPER}_H" >> $H_FILE)
}

function writeCpp() {
  $(cat $BG_HOME/misc/boilerplate.txt >> $CPP_FILE)
  $(echo >> $CPP_FILE)
  $(echo "#include \"${CLASS_NAME}.h\"" >> $CPP_FILE)
  $(echo >> $CPP_FILE)

  if [ ! -z $NAMESPACE ]; then
    $(echo "namespace ${NAMESPACE} {" >> $CPP_FILE)
    $(echo >> $CPP_FILE)
  fi

  $(echo "${CLASS_NAME}::${CLASS_NAME}() {}" >> $CPP_FILE)
  $(echo >> $CPP_FILE)
  $(echo "${CLASS_NAME}::~${CLASS_NAME}() {}" >> $CPP_FILE) 

  if [ ! -z $NAMESPACE ]; then
    $(echo >> $CPP_FILE)
    $(echo "} // ${NAMESPACE}" >> $CPP_FILE)
  fi
}

if [ $# -lt 2 ]; then
  usage
fi

if [ -z $BG_HOME ]; then
  nohome
fi

SRC_PATH="${BG_HOME}/src/${2%/}"
CLASS_NAME=$1
CLASS_UPPER=${CLASS_NAME^^}
H_FILE="$SRC_PATH/$CLASS_NAME.h"
CPP_FILE="$SRC_PATH/$CLASS_NAME.cpp"

NAMESPACE=$3

if [ ! -d $SRC_PATH ]; then
  nodir
fi

if [ -e $H_FILE -o -e $CPP_FILE ]; then
  duplicate
fi

if [ ! -e $BG_HOME/misc/boilerplate.txt ]; then
  noboilerplate
fi

writeHeader
writeCpp

echo "Files created:"
echo $H_FILE
echo $CPP_FILE
echo "Don't forget to add these to CMakeLists.txt!"







#!/bin/bash

function nohome() {
  echo "Error: This script requires the BG_HOME environment variable to be set to the "
  echo "base directory for the project."
  exit 0
}

if [ -z $BG_HOME ]; then
  nohome
fi

cd $BG_HOME
./setupbuild.sh
cd build/release
make install

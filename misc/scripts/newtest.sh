#!/bin/bash

function usage() {
  echo "Usage: $0 ClassName class/path"
  echo "Creates a new class unit test in the Blackguard project using a basic template."
  exit 0
}

function nohome() {
  echo "Error: This script requires the BG_HOME environment variable to be set to the "
  echo "base directory for the project."
  exit 0
}

function notestdir() {
  echo "Error: The directory $TEST_PATH does not exist."
  exit 0
}

function nosrcdir() {
  echo "Error: The directory $SRC_PATH does not exist."
  exit 0
}

function duplicate() {
  echo "Error: A file with the name $TEST_FILE appears to already exist."
  exit 0
}

function writeTest() {
  $(echo "#include \"gtest/gtest.h\"" >> $TEST_FILE)
  $(echo "#include \"${SRC_DIR}/${CLASS_NAME}.h\"" >> $TEST_FILE)
  $(echo >> $TEST_FILE)
  $(echo "class ${CLASS_NAME}Test : public testing::Test {" >> $TEST_FILE)
  $(echo "  protected:" >> $TEST_FILE)
  $(echo "    virtual void SetUp() {" >> $TEST_FILE)
  $(echo "    }" >> $TEST_FILE)
  $(echo "};" >> $TEST_FILE)
  $(echo >> $TEST_FILE)
  $(echo "TEST_F(${CLASS_NAME}Test, test${CLASS_NAME}) {" >> $TEST_FILE)
  $(echo "}" >> $TEST_FILE)
}

if [ $# -lt 2 ]; then
  usage
fi

if [ -z $BG_HOME ]; then
  nohome
fi

TEST_PATH="${BG_HOME}/test"
SRC_DIR=${2%/}
SRC_PATH="${BG_HOME}/src/${2%/}"
CLASS_NAME=$1
TEST_FILE="$TEST_PATH/${CLASS_NAME}Test.cpp"

if [ ! -d $TEST_PATH ]; then
  notestdir
fi

if [ ! -d $SRC_PATH ]; then
  nosrcdir
fi

if [ -e $TEST_FILE ]; then
  duplicate
fi

writeTest

echo "File created: ${TEST_FILE}"
echo "Don't forget to add this to CMakeLists.txt!"
